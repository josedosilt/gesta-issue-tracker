package es.udc.fic.gesta.model.entities.privileges.project;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import es.udc.fic.gesta.model.entities.privileges.BasePrivilege;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.utils.PrivilegeLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class ProjectPrivileges extends BasePrivilege {

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    @Override
    @Transient
    protected PrivilegeLevel getPrivilegeLevel() {
        return PrivilegeLevel.PROJECT;
    }
}
