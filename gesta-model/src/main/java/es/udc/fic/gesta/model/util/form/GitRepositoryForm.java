package es.udc.fic.gesta.model.util.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class GitRepositoryForm {
    @NotNull
    @NotEmpty
    private String url;
    @NotNull
    @NotEmpty
    private String repositoryId;

    private String name;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
