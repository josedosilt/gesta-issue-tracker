package es.udc.fic.gesta.model.service.user;

import es.udc.fic.gesta.model.service.utils.EntityAdapters;
import es.udc.fic.gesta.model.service.utils.SearchExclusions;
import es.udc.fic.gesta.model.entities.privileges.project.ProjectPrivileges;
import es.udc.fic.gesta.model.entities.privileges.project.ProjectPrivilegesRepository;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.project.ProjectRepository;
import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.entities.user.UserRepository;
import es.udc.fic.gesta.model.entities.user.User_;
import es.udc.fic.gesta.model.entities.utils.Privileges;
import es.udc.fic.gesta.model.service.auth.AccessControlFacade;
import es.udc.fic.gesta.model.service.exceptions.EmailAlreadyInUseException;
import es.udc.fic.gesta.model.service.exceptions.UserNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.project.exception.ProjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.*;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AccessControlFacade accessControlFacade;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private ProjectPrivilegesRepository projectPrivilegesRepository;

    @PostConstruct
    protected void initialize() throws EmailAlreadyInUseException {
        if (userRepository.findByEmail("admin") == null) {
            User user= new User("admin", "admin", "ROLE_ADMIN");
            user.setFullName("Paco Manuel Martinez");
            save(user);
        }
        if (userRepository.findByEmail("user") == null) {
            User user = new User("user", "user", "ROLE_USER");
            user.setFullName("Francisco Martinez Fernandez");
            save(user);
        }
    }

    @Transactional
    public User save(User user){
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return user;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(email);
        }

        return createUser(user);
    }

    public void signin(User user) {
        SecurityContextHolder.getContext().setAuthentication(authenticate(user));
    }

    private Authentication authenticate(User user) {
        return new UsernamePasswordAuthenticationToken(createUser(user), null,
                Collections.singleton(createAuthority(user)));
    }

    private UserPrincipal createUser(User user) {
        return new UserPrincipal(user, Collections.singletonList(createAuthority(user)));
    }


    private GrantedAuthority createAuthority(User user) {
        return new SimpleGrantedAuthority(user.getRole());
    }

    @Transactional
    public UserProfileDTO getUserProfile(Long id) {
        return EntityAdapters.createUserProfile(userRepository.getOne(id), true);
    }

    @Transactional
    public UserProfileDTO getUserProfile(String email) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(email);
        }
        return EntityAdapters.createUserProfile(user, true);
    }

    /**
     * Check if the password is valid for the user
     *
     * @param userDetails
     * @param password
     * @return
     */
    public boolean checkPassword(UserDetails userDetails, String password) {
        User user = userRepository.findByEmail(userDetails.getUsername());
        return user != null && password != null && passwordEncoder.matches(password, user.getPassword());
    }

    /**
     * Updates the password of an user
     *
     * @param userDetails
     * @param newPassword
     */
    @Transactional
    public void updatePassword(UserDetails userDetails, String newPassword) {
        User user = userRepository.findByEmail(userDetails.getUsername());
        user.setPassword(newPassword);
        this.save(user);
    }

    @Transactional
    public UserProfileDTO createUser(String email, String password, String fullName) throws EmailAlreadyInUseException {
        User user = new User(email, password, "ROLE_USER");
        user.setFullName(fullName);
        if (userRepository.findByEmail(user.getEmail()) != null) {
            throw new EmailAlreadyInUseException(user.getEmail());
        }
        return EntityAdapters.createUserProfile(this.save(user), false);
    }

    /**
     * Find users that meet the filters
     *
     * @param email
     * @param name
     * @param projectCode
     * @param searchExclusions
     * @param pageSize
     * @return
     */
    @Transactional
    public Page<UserProfileDTO> findUsers(String email, String name, String projectCode,
                                          SearchExclusions searchExclusions, int pageNumber, int pageSize,
                                          boolean isAnd) {
        Set<UserProfileDTO> userProfileDTOS = new HashSet<UserProfileDTO>();
        Specification<User> finalFilter = null;
        if (!(email == null || email.isEmpty()) || !(name == null || name.isEmpty())) {
            finalFilter = UserSpecifications.nameOrEmailsEqualsTo(email, name, isAnd);
        }
        if (projectCode != null && !projectCode.isEmpty()) {
            Specification<User> filterProject = UserSpecifications.withProjectAsigned(projectCode);
            if (finalFilter == null) {
                finalFilter = filterProject;
            } else {
                finalFilter = finalFilter.and(filterProject);
            }
        }
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(User_.EMAIL));
        Page<User> page;
        finalFilter = UserSpecifications.applyExclusions(finalFilter,searchExclusions);
        if (finalFilter == null) {
            page = userRepository.findAll(pageable);
        } else {
            page = userRepository.findAll(finalFilter, pageable);
        }
        List<UserProfileDTO> userProfiles = new ArrayList<UserProfileDTO>();
        UserProfileDTO userProfile = null;
        for (User user : page.getContent()) {
            userProfile = new UserProfileDTO(user.getId(), user.getEmail(), user.getFullName());
            userProfiles.add(userProfile);
        }
        return new PageImpl<UserProfileDTO>(userProfiles, page.getPageable(), page.getTotalPages());
    }

    @Transactional
    public List<UserProfileDTO> findUsersById(List<Long> userIds){
        List<UserProfileDTO> userProfileDTOS = new ArrayList<>();
        for(User user : userRepository.findAllById(userIds)){
            userProfileDTOS.add(EntityAdapters.createUserProfile(user, false));
        }
        return userProfileDTOS;
    }

    @Transactional
    public void assignPrivileges(Long userId, String projectId, List<Privileges> privileges) throws
            OperationForbiddenException, ProjectNotFoundException, UserNotFoundException {
        if(!accessControlFacade.isAdmin()){
            throw new OperationForbiddenException();
        }
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        Project project = projectRepository.findProjectByCode(projectId)
                .orElseThrow(() -> new ProjectNotFoundException(projectId));
        // If we try to assign privileges to a not assigned project, we assign the project first
        if(!user.getProjects().contains(project) && !privileges.isEmpty()){
            user.getProjects().add(project);
            project.getAssignedTo().add(user);
        } else if(user.getProjects().contains(project) && privileges.isEmpty()){
            // if user has the project and the privileges list is empty, we remove the project from the user ones.
            user.getProjects().remove(project);
            project.getAssignedTo().remove(user);
        }
        ProjectPrivileges projectPrivileges = projectPrivilegesRepository.findByUserAndProject(user,project).orElse(null);
        if (projectPrivileges == null) {
            projectPrivileges = new ProjectPrivileges();
            projectPrivileges.setProject(project);
            projectPrivileges.setUser(user);
        }
        projectPrivileges.setPrivileges(privileges);
        projectPrivilegesRepository.save(projectPrivileges);
        projectRepository.save(project);
        userRepository.save(user);
    }

}
