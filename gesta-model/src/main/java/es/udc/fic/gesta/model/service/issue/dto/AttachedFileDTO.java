package es.udc.fic.gesta.model.service.issue.dto;

public class AttachedFileDTO {

    private Long fileId;
    private String fileName;

    public AttachedFileDTO() {
    }

    public AttachedFileDTO(Long fileId, String fileName) {
        this.fileId = fileId;
        this.fileName = fileName;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
