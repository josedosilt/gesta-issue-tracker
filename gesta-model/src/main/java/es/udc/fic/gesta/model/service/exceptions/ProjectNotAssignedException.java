package es.udc.fic.gesta.model.service.exceptions;

public class ProjectNotAssignedException extends Exception {

    public ProjectNotAssignedException(String projectID, String email){
        super(String.format("User with email %s doesn't have the project %s assigned", email, projectID));
    }
}
