package es.udc.fic.gesta.model.entities.issue;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import es.udc.fic.gesta.model.entities.AuditableEntity;
import es.udc.fic.gesta.model.entities.attached.AttachedFile;
import es.udc.fic.gesta.model.entities.cycle.DevelopmentCycle;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.entities.utils.Priority;
import es.udc.fic.gesta.model.util.IssueStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Issue.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Issue extends AuditableEntity implements Serializable {

    private static final long serialVersionUID = -952860901896154127L;

    @Id
    @Column(name = "issue_id")
    private String issueID;

    @NotNull
    @NotBlank
    private String summary;

    private String description;

    @NotNull
    private IssueType type;

    @NotNull
    private Priority priority;

    @NotNull
    @ManyToOne
    private User createdBy;

    @NotNull
    @ManyToOne
    private User assignedTo;

    private Date startDate;

    private Date endDate;

    private Double estimatedHours;

    @NotNull
    @ManyToOne
    private Project project;

    @ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
    @JoinTable(name = "issue_watchers", joinColumns = { @JoinColumn(name = "issue_id") }, inverseJoinColumns = {
            @JoinColumn(name = "user_id") })
    private Set<User> watchers;

    @ManyToOne
    private DevelopmentCycle developmentCycle;

    @NotNull
    @Column(name = "issue_status")
    private IssueStatus issueStatus;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Issue> relatedIssues;

    @OneToMany(mappedBy = "issue", cascade = CascadeType.ALL)
    private List<AttachedFile> attachedFiles;
}
