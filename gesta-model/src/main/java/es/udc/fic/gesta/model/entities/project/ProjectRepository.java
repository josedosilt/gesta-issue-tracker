package es.udc.fic.gesta.model.entities.project;

import es.udc.fic.gesta.model.entities.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface ProjectRepository
        extends JpaRepository<Project, Long>, JpaSpecificationExecutor<Project>,
        PagingAndSortingRepository<Project, Long> {

    Project findByCode(String code);

    Optional<Project> findProjectByCode(String code);

    @Query("SELECT p FROM Issue i INNER JOIN i.project p WHERE i.issueID = ?1")
    Optional<Project> findByIssueId(String issueId);


    @Query("SELECT " +
            "CASE WHEN COUNT(p) > 0 THEN true ELSE false END " +
            "FROM Project p JOIN p.assignedTo a " +
            "WHERE p = :project AND a = :user")
    boolean hasProjectAssigned(@Param("project") Project project, @Param("user") User user);

    @Query("SELECT " +
            "CASE WHEN COUNT(p) > 0 THEN true ELSE false END " +
            "FROM Project p JOIN p.assignedTo a " +
            "WHERE p.code = :project AND a.email = :user")
    boolean hasProjectAssigned(@Param("project") String project, @Param("user") String user);

    Page<Project> findProjectsByAssignedToContains(User user, Pageable pageable);


}
