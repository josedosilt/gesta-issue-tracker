package es.udc.fic.gesta.model.service.exceptions.cycle;

public abstract class BadNameException extends Exception {

    protected BadNameException(String msg){
        super(msg);
    }
}
