package es.udc.fic.gesta.model.entities.project;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import es.udc.fic.gesta.model.entities.AuditableEntity;
import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.privileges.project.ProjectPrivileges;
import es.udc.fic.gesta.model.entities.repository.GitLabRepo;
import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.util.ProjectState;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Project extends AuditableEntity implements Serializable {

    private static final long serialVersionUID = -9179947485090605417L;

    @Id
    @GeneratedValue
    @Column(name = "project_id")
    private Long id;

    private String name;

    @Column(unique = true)
    private String code;

    @ManyToOne
    private User projectLeader;

    @ManyToMany
    @JoinTable(name = "user_project", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = {
            @JoinColumn(name = "project_id") })
    private List<User> assignedTo;

    private ProjectState projectState;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "project")
    private List<ProjectPrivileges> projectPrivileges;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "project")
    private List<Issue> projectIssues;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "project")
    private GitLabRepo gitLabRepo;

    public Project(String name, String code, User projectLeader) {
        this.name = name;
        this.code = code;
        this.projectLeader = projectLeader;
        this.projectState = ProjectState.OPEN;
        this.assignedTo = new ArrayList<>();
        this.assignedTo.add(projectLeader);
    }
}
