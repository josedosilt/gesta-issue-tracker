package es.udc.fic.gesta.model.service.git.dto;

public class GitLabRepositoryDTO {

    private Long repositoryId;

    private String repositoryName;

    private String repositoryUrl;

    public Long getRepositoryId() {
        return this.repositoryId;
    }

    public void setRepositoryId(Long repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getRepositoryName() {
        return repositoryName;
    }

    public void setRepositoryName(String repositoryName) {
        this.repositoryName = repositoryName;
    }

    public String getRepositoryUrl() {
        return repositoryUrl;
    }

    public void setRepositoryUrl(String repositoryUrl) {
        this.repositoryUrl = repositoryUrl;
    }
}
