package es.udc.fic.gesta.model.entities.privileges.project;

import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProjectPrivilegesRepository extends JpaRepository<ProjectPrivileges, Long>,
        JpaSpecificationExecutor<ProjectPrivileges>, PagingAndSortingRepository<ProjectPrivileges, Long> {

    Optional<ProjectPrivileges> findByUserAndProject(User user, Project project);

    void removeAllByProject(Project project);

}
