package es.udc.fic.gesta.model.service.exceptions.cycle;

public class EmptyOrNullNameException extends BadNameException{


    public EmptyOrNullNameException() {
        super("Name cannot be empty or null");
    }
}
