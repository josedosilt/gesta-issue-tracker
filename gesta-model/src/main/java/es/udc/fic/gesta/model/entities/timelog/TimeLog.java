package es.udc.fic.gesta.model.entities.timelog;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotNull;

import es.udc.fic.gesta.model.entities.AuditableEntity;
import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class TimeLog extends AuditableEntity implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "time_log_id")
    @NotNull
    private Long id;

    @ManyToOne
    @NotNull
    private Issue issue;

    @ManyToOne
    @NotNull
    private User user;

    @NotNull
    private BigDecimal hours;

    private String comment;

    @NotNull
    private Date date;

    @PrePersist
    public void prePersist() {
        date = Date.from(Instant.now());
    }
}
