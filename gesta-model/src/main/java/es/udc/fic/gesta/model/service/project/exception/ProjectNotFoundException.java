package es.udc.fic.gesta.model.service.project.exception;

import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.service.exceptions.EntityNotFoundException;

import java.util.Map;

public class ProjectNotFoundException extends EntityNotFoundException {

    public ProjectNotFoundException(Long id) {
        super(Project.class, id);
    }

    public ProjectNotFoundException(String projectID) {
        super(Project.class, projectID);
    }

    public ProjectNotFoundException(Map<String, Object> filters) {
        super(Project.class, filters);
    }
}
