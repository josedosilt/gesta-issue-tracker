package es.udc.fic.gesta.model.service.exceptions;

public class StorageException extends Exception {

    public StorageException(String msg){
        super(msg);
    }

    public StorageException(String msg, Exception cause){
        super(msg, cause);
    }
}
