package es.udc.fic.gesta.model.service.project.exception;

/**
 * The type Project already assigned exception.
 */
public class ProjectAlreadyAssignedException extends Exception {

    private final Long userID;

    private final String projectID;

    /**
     * Instantiates a new Project already assigned exception.
     *
     * @param userID    the user id
     * @param projectID the project id
     */
    public ProjectAlreadyAssignedException(Long userID, String projectID) {
        super(String.format("User with id %d already has the project %s assigned or is the project leader",
                userID.intValue(), projectID));
        this.userID = userID;
        this.projectID = projectID;
    }

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public Long getUserID() {
        return userID;
    }
    /**
     * Gets project id.
     *
     * @return the project id
     */
    public String getProjectID() {
        return projectID;
    }
}
