package es.udc.fic.gesta.model.service.project;

import es.udc.fic.gesta.model.service.exceptions.DevelopmentCycleNotFoundException;
import es.udc.fic.gesta.model.entities.cycle.DevelopmentCycle;
import es.udc.fic.gesta.model.entities.cycle.DevelopmentCycleRepository;
import es.udc.fic.gesta.model.entities.cycle.DevelopmentCycle_;
import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.issue.IssueRepository;
import es.udc.fic.gesta.model.entities.issue.Issue_;
import es.udc.fic.gesta.model.entities.privileges.project.ProjectPrivileges;
import es.udc.fic.gesta.model.entities.privileges.project.ProjectPrivilegesRepository;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.project.ProjectRepository;
import es.udc.fic.gesta.model.entities.project.Project_;
import es.udc.fic.gesta.model.entities.project.specifications.ProjectSpecifications;
import es.udc.fic.gesta.model.entities.repository.GitLabRepo;
import es.udc.fic.gesta.model.entities.repository.GitLabRepoRepository;
import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.entities.user.UserRepository;
import es.udc.fic.gesta.model.entities.utils.PrivilegeLevel;
import es.udc.fic.gesta.model.entities.utils.Privileges;
import es.udc.fic.gesta.model.entities.utils.SpecificationsUtils;
import es.udc.fic.gesta.model.service.auth.AccessControlFacade;
import es.udc.fic.gesta.model.service.dto.DevelopmentCycleDTO;
import es.udc.fic.gesta.model.service.exceptions.EntityNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.ProjectNotAssignedException;
import es.udc.fic.gesta.model.service.exceptions.UserNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.exceptions.cycle.BadNameException;
import es.udc.fic.gesta.model.service.exceptions.cycle.EmptyOrNullNameException;
import es.udc.fic.gesta.model.service.exceptions.cycle.NameAlreadyExistsForProjectException;
import es.udc.fic.gesta.model.service.exceptions.git.NoRepositoryAsignedException;
import es.udc.fic.gesta.model.service.issue.IssueDetails;
import es.udc.fic.gesta.model.service.project.exception.ProjectAlreadyAssignedException;
import es.udc.fic.gesta.model.service.project.exception.ProjectNotFoundException;
import es.udc.fic.gesta.model.service.utils.EntityAdapters;
import es.udc.fic.gesta.model.service.utils.SearchExclusions;
import es.udc.fic.gesta.model.util.form.GitRepositoryForm;
import es.udc.fic.gesta.model.util.form.ProjectForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.*;

@Service
@Validated
public class ProjectService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private IssueRepository issueRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private DevelopmentCycleRepository developmentCycleRepository;
    @Autowired
    private AccessControlFacade accessControlFacade;
    @Autowired
    private ProjectPrivilegesRepository projectPrivilegesRepository;
    @Autowired
    private GitLabRepoRepository gitLabRepoRepository;

    @Transactional(dontRollbackOn = EntityNotFoundException.class)
    public Project createProject(String projectCode, String projectName, Long projectLeaderID)
            throws EntityNotFoundException, OperationForbiddenException {
        if(!accessControlFacade.isAdmin()){
            throw new OperationForbiddenException();
        }
        Optional<User> projectLeader = userRepository.findById(projectLeaderID);

        if (!projectLeader.isPresent()) {
            throw new EntityNotFoundException(User.class, projectLeaderID);
        }
        Project project = saveProject(projectName, projectCode, projectLeader.get());
        // Set defaults privileges
        this.setDefaultProjectPrivileges(project, projectLeader.get());
        return project;
    }

    @Transactional
    public Project createProject(String projectCode, String projectName, String projectLeaderEmail)
            throws EntityNotFoundException, OperationForbiddenException {
        if(!accessControlFacade.isAdmin()){
            throw new OperationForbiddenException();
        }
        User projectLeader = userRepository.findByEmail(projectLeaderEmail);
        if (projectLeader == null) {
            throw new EntityNotFoundException(User.class, Collections.singletonMap("email", projectLeaderEmail));
        }
        Project project = saveProject(projectName, projectCode, projectLeader);
        // Set defaults privileges
        this.setDefaultProjectPrivileges(project, projectLeader);
        return project;
    }

    private Project saveProject(String projectName, String projectCode, User projectLeader) {
        return projectRepository.save(new Project(projectName, projectCode, projectLeader));
}

    public ProjectDetails getProjectDetails(String projectID) throws OperationForbiddenException,
            EntityNotFoundException {
        return this.getProjectDetails(projectID, false);
    }

    @Transactional
    public ProjectDetails getProjectDetails(String projectID, boolean recoverIssues) throws EntityNotFoundException,
            OperationForbiddenException {
        Project project = projectRepository.findByCode(projectID);
        if (project == null) {
            Map<String, Object> params = new HashMap<String, Object>() {{
                put(Project_.CODE, projectID);
            }};
            throw new EntityNotFoundException(Project.class, params);
        }
        if(!accessControlFacade.hasPrivilegesInProject(project, Privileges.READ_PRIVILEGE)){
            throw new OperationForbiddenException();
        }
        ProjectDetails projectDetails = EntityAdapters.createProjectDetails(project, true);
        if (recoverIssues) {
            Page<Issue> projectIssues =
                    issueRepository.findByProject(project,
                            PageRequest.of(0, 5, Sort.by(Sort.Direction.DESC, Issue_.CREATED_DATE)));
            if (projectIssues.getTotalElements() > 0) {
                List<IssueDetails> issueDetails = new ArrayList<>();
                for (Issue issue : projectIssues.getContent()) {
                    issueDetails.add(EntityAdapters.createIssueDetails(issue));
                }
                projectDetails.setIssueDetails(issueDetails);
            }
        }
        return projectDetails;

    }

    @Transactional
    public Page<ProjectDetails> findProjects(String projectParam, String leaderName, SearchExclusions searchExclusions,
                                             int page, int pageSize) {
        Specification<Project> specification = null;
        if (projectParam != null && !projectParam.isEmpty()) {
            specification = ProjectSpecifications.nameOrCodeLikeTo(projectParam);
        }
        if (leaderName != null && !leaderName.isEmpty()) {
            Specification<Project> projectSpecification = ProjectSpecifications.projectLeaderNameEqualsTo(leaderName);
            specification = (specification != null) ? specification.and(projectSpecification) : projectSpecification;
        }
        if(!accessControlFacade.isAdmin()){
            specification = SpecificationsUtils.addOrCreateFilterWithAnd(specification,
                    accessControlFacade.accessToProjectSpecification(Privileges.READ_PRIVILEGE));
        }
        final Page<Project> projectPage;
        Pageable pageable = PageRequest.of(page, pageSize, Sort.by(Project_.CODE));
        specification = ProjectSpecifications.applyProjectExclusions(specification, searchExclusions);
        if (specification == null) {
            projectPage = projectRepository.findAll(pageable);
        } else {
            projectPage = projectRepository.findAll(specification, pageable);
        }
        List<ProjectDetails> projectDetailsList = new ArrayList<>();
        projectPage.forEach(
                (project) -> projectDetailsList.add(EntityAdapters.createProjectDetails((Project) project, false)));

        return new PageImpl<ProjectDetails>(projectDetailsList, PageRequest.of(page, pageSize),
                projectPage.getTotalElements());
    }

    /**
     * Assign project to one or more users.
     *
     * @param projectID the project id
     * @param userIDs   the IDs of the users to assign the project
     * @throws ProjectNotFoundException        if no project with {@code projectID} is found
     * @throws UserNotFoundException           if at least one of the id of {@code userIDs} does not belong to any user
     * @throws ProjectAlreadyAssignedException if at least one user which id is in {@code userIDs} already has the
     *                                         project assigned or is the project leader
     */
    @Transactional(rollbackOn = Exception.class)
    public void assignProject(String projectID, List<Long> userIDs) throws ProjectNotFoundException,
            UserNotFoundException, ProjectAlreadyAssignedException, OperationForbiddenException {
        if(!accessControlFacade.isAdmin()){
            throw new OperationForbiddenException();
        }
        Project project = projectRepository.findByCode(projectID);
        if (project == null) {
            throw new ProjectNotFoundException(projectID);
        }

        List<User> users = userRepository.findAllById(userIDs);
        if (users == null || users.isEmpty()) {
            throw new UserNotFoundException(userIDs);
        }

        if (users.contains(project.getProjectLeader())) {
            throw new ProjectAlreadyAssignedException(project.getProjectLeader().getId(), projectID);
        }

        for (User assignee : project.getAssignedTo()) {
            if (users.contains(assignee)) {
                throw new ProjectAlreadyAssignedException(assignee.getId(), projectID);
            }
        }
        this.setDefaultProjectPrivileges(project, users.toArray(new User[0]));
        project.getAssignedTo().addAll(users);
        projectRepository.save(project);

    }

    @Transactional(dontRollbackOn = ProjectNotFoundException.class)
    public void removeProject(String projectId) throws ProjectNotFoundException, OperationForbiddenException {
        if(!accessControlFacade.isAdmin()){
            throw new OperationForbiddenException();
        }
        Project project = projectRepository.findByCode(projectId);
        if(project == null){
            throw new ProjectNotFoundException(projectId);
        }
        projectPrivilegesRepository.removeAllByProject(project);
        projectRepository.delete(project);
    }

    @Transactional(dontRollbackOn = {ProjectNotFoundException.class, UserNotFoundException.class})
    public void editProject(ProjectForm projectForm, String projectId) throws ProjectNotFoundException,
            UserNotFoundException, OperationForbiddenException {
        if(!accessControlFacade.isAdmin()){
            throw new OperationForbiddenException();
        }
        Project project = projectRepository.findByCode(projectId);
        if(project == null){
            throw new ProjectNotFoundException(projectId);
        }
        if(!project.getProjectLeader().getId().equals(projectForm.getProjectLeaderID())){
            User newLeader = userRepository.findById(projectForm.getProjectLeaderID())
                    .orElseThrow(() -> new UserNotFoundException(projectForm.getProjectLeaderID()));
            project.setProjectLeader(newLeader);
            if(!project.getAssignedTo().contains(newLeader)){
                project.getAssignedTo().add(newLeader);
            }
        }
        if(!project.getName().equals(projectForm.projectName)){
            project.setName(projectForm.projectName);
        }
        if(!project.getProjectState().equals(projectForm.getProjectState())){
            project.setProjectState(projectForm.getProjectState());
        }

        projectRepository.save(project);
    }

    @Transactional(
            dontRollbackOn = {UserNotFoundException.class, ProjectNotFoundException.class, BadNameException.class}
    )
    public DevelopmentCycleDTO createDevelopmentCycle(String email, String projectId, String cycleName) throws
            UserNotFoundException, ProjectNotFoundException, NameAlreadyExistsForProjectException,
            EmptyOrNullNameException, ProjectNotAssignedException, OperationForbiddenException {
        if (cycleName == null || cycleName.isEmpty()) {
            throw new EmptyOrNullNameException();
        }
        User user = userRepository.findUserByEmail(email).orElseThrow(() -> new UserNotFoundException(email));
        Project project = projectRepository.findProjectByCode(projectId)
                .orElseThrow(() -> new ProjectNotFoundException(projectId));
        if(!accessControlFacade.hasPrivilegesInProject(project, Privileges.CREATE_CYCLE_PRIVILEGE)){
            throw new OperationForbiddenException();
        }
        if (!(project.getProjectLeader().equals(user) || projectRepository.hasProjectAssigned(projectId, email))) {
            throw new ProjectNotAssignedException(projectId, email);
        }

        if (developmentCycleRepository.existsByNameAndProject(cycleName, project)) {
            throw new NameAlreadyExistsForProjectException(cycleName, projectId);
        }

        DevelopmentCycle developmentCycle = developmentCycleRepository.save(new DevelopmentCycle(project, cycleName));
        return EntityAdapters.createDevelopmentCycleDTO(developmentCycle, false);
    }

    @Transactional
    public boolean userHasProjectAssigned(String email, String projectId){
        return projectRepository.hasProjectAssigned(projectId, email);
    }

    @Transactional
    public Page<DevelopmentCycleDTO> searchDevelopmentCycles(@NotNull String projectId,
                                                             @Nullable String developmentCycleName, int page,
                                                             int pageSize) throws OperationForbiddenException {
        if (!accessControlFacade
                .hasPrivilegesInProject(projectRepository.findByCode(projectId), Privileges.READ_PRIVILEGE)) {
            throw new OperationForbiddenException();
        }
        List<DevelopmentCycleDTO> result = new ArrayList<>();
        Page<DevelopmentCycle> developmentCycles;
        if (developmentCycleName == null || developmentCycleName.isEmpty()) {
            developmentCycles = developmentCycleRepository
                    .findByProject_Code(projectId, PageRequest.of(page, pageSize, Sort.by(DevelopmentCycle_.NAME)));
        } else {
            developmentCycles = developmentCycleRepository
                    .findByProject_CodeAndNameContainsIgnoreCase(projectId, developmentCycleName,
                            PageRequest.of(page, pageSize, Sort.by(DevelopmentCycle_.NAME)));
        }
        for (DevelopmentCycle developmentCycle : developmentCycles.getContent()) {
            result.add(EntityAdapters.createDevelopmentCycleDTO(developmentCycle, false));
        }
        return new PageImpl<DevelopmentCycleDTO>(result, PageRequest.of(page, pageSize),
                developmentCycles.getTotalElements());
    }

    @Transactional
    public DevelopmentCycleDTO getDevelopmentCycle(String projectId, String cycleName) throws
            DevelopmentCycleNotFoundException,
            OperationForbiddenException {
        DevelopmentCycle cycle = developmentCycleRepository.findByProject_CodeAndName(projectId, cycleName)
                .orElseThrow(() -> new DevelopmentCycleNotFoundException(cycleName));
        if(!accessControlFacade.hasAccessToProject(cycle.getProject())){
            throw new OperationForbiddenException();
        }
        return EntityAdapters.createDevelopmentCycleDTO(cycle, true);
    }

    @Transactional (dontRollbackOn = {OperationForbiddenException.class, DevelopmentCycleNotFoundException.class})
    public DevelopmentCycleDTO getDevelopmentCycle(Long cycleId) throws DevelopmentCycleNotFoundException,
            OperationForbiddenException {
        DevelopmentCycle cycle = developmentCycleRepository.findById(cycleId)
                .orElseThrow(() -> new DevelopmentCycleNotFoundException(cycleId));
        if(!accessControlFacade.hasAccessToProject(cycle.getProject())){
            throw new OperationForbiddenException();
        }
        return EntityAdapters.createDevelopmentCycleDTO(cycle, true);
    }

    private void setDefaultProjectPrivileges(Project project, User... users){
        for(User user : users){
            ProjectPrivileges projectPrivileges = new ProjectPrivileges();
            projectPrivileges.setProject(project);
            projectPrivileges.setUser(user);
            projectPrivileges.setPrivileges(new ArrayList<>(Privileges.valuesByLevel(PrivilegeLevel.PROJECT)));
            projectPrivilegesRepository.save(projectPrivileges);
        }
    }

    @Transactional
    public void assignGitRepository(String projectId, @Valid GitRepositoryForm repositoryForm) throws
            OperationForbiddenException, ProjectNotFoundException {
        if(!accessControlFacade.isAdmin()){
            throw new OperationForbiddenException();
        }

        Project project = projectRepository.findProjectByCode(projectId)
                .orElseThrow(() -> new ProjectNotFoundException(projectId));
        if(project.getGitLabRepo() != null){
            gitLabRepoRepository.delete(project.getGitLabRepo());
        }
        GitLabRepo gitLabRepo =
                new GitLabRepo(repositoryForm.getRepositoryId(), repositoryForm.getName(), repositoryForm.getUrl(), project);
        project.setGitLabRepo(gitLabRepo);
        projectRepository.save(project);
    }

    @Transactional(dontRollbackOn = {DevelopmentCycleNotFoundException.class, OperationForbiddenException.class,
            NoRepositoryAsignedException.class})
    public void assignGitBranchToCycle(String projectId, String cycleId, String branchName) throws DevelopmentCycleNotFoundException,
            OperationForbiddenException, NoRepositoryAsignedException {
        DevelopmentCycle cycle = developmentCycleRepository.findByProject_CodeAndName(projectId, cycleId)
                .orElseThrow(() -> new DevelopmentCycleNotFoundException(cycleId));
        if(!accessControlFacade.hasPrivilegesInProject(cycle.getProject(), Privileges.WRITE_PRIVILEGE)){
            throw new OperationForbiddenException();
        }
        Project project = cycle.getProject();
        if(project.getGitLabRepo() == null){
            throw new NoRepositoryAsignedException(project.getCode());
        }

        cycle.setGitBranch(branchName);
        developmentCycleRepository.save(cycle);
    }

    @Transactional(dontRollbackOn = {OperationForbiddenException.class, UserNotFoundException.class})
    public Page<ProjectDetails> findAssignedProjects(int page, int pageSize) throws OperationForbiddenException,
            UserNotFoundException {
        Authentication auth = accessControlFacade.getAuthentication();
        if (auth == null || auth instanceof AnonymousAuthenticationToken) {
            throw new OperationForbiddenException();
        }
        User user = userRepository.findUserByEmail(auth.getName())
                .orElseThrow(() -> new UserNotFoundException(auth.getName()));

        Page<Project> projects =
                projectRepository.findProjectsByAssignedToContains(user, PageRequest.of(page, pageSize));
        List<ProjectDetails> resultContent = new ArrayList<>();
        for (Project project : projects) {
            resultContent.add(EntityAdapters.createProjectDetails(project, false));
        }

        return new PageImpl<>(resultContent, projects.getPageable(), projects.getTotalElements());
    }

    public boolean projectHasGitRepository(String projectId){
        Project project = projectRepository.findProjectByCode(projectId).orElse(null);
        return project != null && project.getGitLabRepo() != null;

    }
}
