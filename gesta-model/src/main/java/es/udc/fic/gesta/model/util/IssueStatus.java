package es.udc.fic.gesta.model.util;

public enum IssueStatus {
    OPEN,
    IN_PROGRESS,
    CLOSED;

}
