package es.udc.fic.gesta.model.service.exceptions;

import es.udc.fic.gesta.model.entities.user.User;

import java.util.List;
import java.util.Map;

public class UserNotFoundException extends EntityNotFoundException {


    public UserNotFoundException(Long id) {
        super(User.class, id);
    }

    public UserNotFoundException(Map<String, Object> filters) {
        super(User.class, filters);
    }

    public UserNotFoundException(List<Long> ids){
        super(String.format("No User with ids %s was found", ids));
    }

    public UserNotFoundException(String email){
        super("No user found with email: " + email);
    }
}
