package es.udc.fic.gesta.model.service.exceptions.git;

public class NoRepositoryAsignedException extends GitException {

    public NoRepositoryAsignedException(String projectId){
        super(String.format("Project with ID %s doesn't have a git repository assigned", projectId));
    }
}
