package es.udc.fic.gesta.model.service.exceptions;

public class GitLabRepositoryNotFoundException extends Exception{

    public GitLabRepositoryNotFoundException(String projectId){
        super(String.format("GitLab Repository not found. No Project with id %s with repository assigned"));
    }

}
