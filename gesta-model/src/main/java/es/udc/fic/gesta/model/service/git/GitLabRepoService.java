package es.udc.fic.gesta.model.service.git;

import es.udc.fic.gesta.model.service.exceptions.DevelopmentCycleNotFoundException;
import es.udc.fic.gesta.model.entities.cycle.DevelopmentCycle;
import es.udc.fic.gesta.model.entities.cycle.DevelopmentCycleRepository;
import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.issue.IssueRepository;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.project.ProjectRepository;
import es.udc.fic.gesta.model.entities.repository.GitLabRepo;
import es.udc.fic.gesta.model.entities.repository.GitLabRepoRepository;
import es.udc.fic.gesta.model.entities.utils.Privileges;
import es.udc.fic.gesta.model.service.auth.AccessControlFacade;
import es.udc.fic.gesta.model.service.dto.BranchDTO;
import es.udc.fic.gesta.model.service.exceptions.GitLabRepositoryNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.IssueNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.git.dto.GitCommitDTO;
import es.udc.fic.gesta.model.service.git.dto.GitLabRepositoryDTO;
import es.udc.fic.gesta.model.service.project.exception.ProjectNotFoundException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class GitLabRepoService {

    @Autowired
    private GitLabRepoRepository gitLabRepoRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private IssueRepository issueRepository;
    @Autowired
    private AccessControlFacade accessControlFacade;
    @Autowired
    private DevelopmentCycleRepository developmentCycleRepository;

    @Value("${gitlab.access-token:}")
    private String gitLabAccessToken;

    @Value("${gitlab.baseUrl:https://gitlab.com}")
    private String gitLabBaseUrl;

    @Transactional
    public List<BranchDTO> findProjectBranches(String projectId) throws GitLabRepositoryNotFoundException,
            ProjectNotFoundException, OperationForbiddenException {
        Project project = projectRepository.findProjectByCode(projectId)
                .orElseThrow(() -> new ProjectNotFoundException(projectId));
        if (!accessControlFacade.isAdmin() &&
                !accessControlFacade.hasPrivilegesInProject(project, Privileges.READ_PRIVILEGE)) {
            throw new OperationForbiddenException();
        }
        GitLabRepo repo = gitLabRepoRepository.findByProject(project)
                .orElseThrow(() -> new GitLabRepositoryNotFoundException(projectId));
        HttpURLConnection connection = null;
        List<BranchDTO> branchDTOS = new ArrayList<>();
        try {
            StringBuilder urlBuilder = new StringBuilder("https://");
            urlBuilder.append(new URL(repo.getURL()).getAuthority());
            urlBuilder.append("/api/v4/projects/").append(repo.getRepositoryId()).append("/repository/branches");
            urlBuilder.append("?").append(URLEncoder.encode("private_token", "UTF-8")).append("=")
                    .append(URLEncoder.encode(gitLabAccessToken, "UTF-8"));
            connection = (HttpURLConnection) new URL(urlBuilder.toString()).openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            int status = connection.getResponseCode();

            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    JSONParser parser = new JSONParser();
                    JSONArray jsonArray = (JSONArray) parser.parse(br);
                    for (Object obj : jsonArray) {
                        JSONObject jsonObject = (JSONObject) obj;
                        BranchDTO branchDTO = new BranchDTO();
                        branchDTO.setProjectId(projectId);
                        branchDTO.setBranchName((String) jsonObject.get("name"));
                        branchDTO.setUrl(repo.getURL() + "/tree/" + branchDTO.getBranchName());
                        branchDTOS.add(branchDTO);
                    }
            }
        } catch (IOException | ParseException ignored) {
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return branchDTOS;
    }

    @Transactional
    public List<GitLabRepositoryDTO> findRepositories()  {
        List<GitLabRepositoryDTO> result = new ArrayList<>();
        HttpURLConnection connection = null;
        try {
            StringBuilder urlBuilder = new StringBuilder(gitLabBaseUrl);
            urlBuilder.append("/api/v4/projects?owned=true&")
                    .append(URLEncoder.encode("private_token", "UTF-8"))
                    .append("=")
                    .append(URLEncoder.encode(gitLabAccessToken, "UTF-8"));
            URL url = new URL(urlBuilder.toString());
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            int status = connection.getResponseCode();
            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    JSONParser parser = new JSONParser();
                    JSONArray jsonArray = (JSONArray) parser.parse(br);
                    for (Object obj : jsonArray) {
                        JSONObject jsonObject = (JSONObject) obj;
                        GitLabRepositoryDTO gitLabRepositoryDTO = new GitLabRepositoryDTO();
                        gitLabRepositoryDTO.setRepositoryId((Long) jsonObject.get("id"));
                        gitLabRepositoryDTO.setRepositoryName((String) jsonObject.get("name"));
                        gitLabRepositoryDTO.setRepositoryUrl((String) jsonObject.get("web_url"));
                        result.add(gitLabRepositoryDTO);
                    }
            }
        } catch (IOException | ParseException ignored) {
        } finally {
            if(connection != null){
                connection.disconnect();
            }
        }

        return result;
    }

    @Transactional
    public List<GitCommitDTO> findIssueCommits(String issueId) throws IssueNotFoundException,
            OperationForbiddenException {
        Issue issue = issueRepository.findById(issueId).orElseThrow(() -> new IssueNotFoundException(issueId));
        if(!accessControlFacade.hasPrivilegesInProject(issue.getProject(), Privileges.READ_PRIVILEGE)){
            throw new OperationForbiddenException();
        }
        GitLabRepo projectRepository = issue.getProject().getGitLabRepo();

        List<GitCommitDTO> result = new ArrayList<>();
        HttpURLConnection connection = null;
        try {
        StringBuilder urlBuilder = new StringBuilder("https://");
            urlBuilder.append(new URL(projectRepository.getURL()).getAuthority());
            urlBuilder.append("/api/v4/projects/").append(projectRepository.getRepositoryId());
            urlBuilder.append("/repository/commits?all=true&per_page=100");
            urlBuilder.append("&").append(URLEncoder.encode("private_token", "UTF-8")).append("=")
                    .append(URLEncoder.encode(gitLabAccessToken, "UTF-8"));
            URL url = new URL(urlBuilder.toString());
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            int status = connection.getResponseCode();
            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    JSONParser parser = new JSONParser();
                    JSONArray jsonArray = (JSONArray) parser.parse(br);
                    for (Object obj : jsonArray) {
                        JSONObject jsonObject = (JSONObject) obj;
                        String commitMessage = (String) jsonObject.get("message");
                        if (commitMessage != null && commitMessage.contains(issueId)) {
                            GitCommitDTO gitCommitDTO = new GitCommitDTO();
                            gitCommitDTO.setCommitId((String) jsonObject.get("id"));
                            gitCommitDTO.setCommitShortId((String) jsonObject.get("short_id"));
                            gitCommitDTO.setCommitMessage(commitMessage);
                            gitCommitDTO.setCommiterName((String) jsonObject.get("committer_name"));
                            gitCommitDTO.setCommitDate((String) jsonObject.get("committed_date"));
                            gitCommitDTO.setProjectUrl(projectRepository.getURL());
                            result.add(gitCommitDTO);
                        }
                    }
            }
        } catch (IOException | ParseException ignored) {
        } finally {
            if(connection != null){
                connection.disconnect();
            }
        }
        if(!result.isEmpty()){
            Collections.reverse(result);
        }
        return result;
    }

    @Transactional
    public List<GitCommitDTO> findCycleCommits(String projectId, String cycleName) throws
            DevelopmentCycleNotFoundException,
            OperationForbiddenException {
        DevelopmentCycle cycle = developmentCycleRepository.findByProject_CodeAndName(projectId, cycleName)
                .orElseThrow(() -> new DevelopmentCycleNotFoundException(cycleName));
        if (!accessControlFacade.hasPrivilegesInProject(cycle.getProject(), Privileges.READ_PRIVILEGE)) {
            throw new OperationForbiddenException();
        }
        GitLabRepo projectRepository = cycle.getProject().getGitLabRepo();

        List<GitCommitDTO> result = new ArrayList<>();
        HttpURLConnection connection = null;
        try {
            StringBuilder urlBuilder = new StringBuilder("https://");
            urlBuilder.append(new URL(projectRepository.getURL()).getAuthority());
            urlBuilder.append("/api/v4/projects/").append(projectRepository.getRepositoryId());
            urlBuilder.append("/repository/commits?per_page=100");
            urlBuilder.append("&ref_name=").append(URLEncoder.encode(cycle.getGitBranch(), "UTF-8"));
            urlBuilder.append("&").append(URLEncoder.encode("private_token", "UTF-8")).append("=")
                    .append(URLEncoder.encode(gitLabAccessToken, "UTF-8"));
            URL url = new URL(urlBuilder.toString());
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            int status = connection.getResponseCode();
            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    JSONParser parser = new JSONParser();
                    JSONArray jsonArray = (JSONArray) parser.parse(br);
                    for (Object obj : jsonArray) {
                        JSONObject jsonObject = (JSONObject) obj;
                        String commitMessage = (String) jsonObject.get("message");
                        if (commitMessage != null) {
                            GitCommitDTO gitCommitDTO = new GitCommitDTO();
                            gitCommitDTO.setCommitId((String) jsonObject.get("id"));
                            gitCommitDTO.setCommitShortId((String) jsonObject.get("short_id"));
                            gitCommitDTO.setCommitMessage(commitMessage);
                            gitCommitDTO.setCommiterName((String) jsonObject.get("committer_name"));
                            gitCommitDTO.setCommitDate((String) jsonObject.get("committed_date"));
                            gitCommitDTO.setProjectUrl(projectRepository.getURL());
                            result.add(gitCommitDTO);
                        }
                    }
            }
        } catch (IOException | ParseException ignored) {
        } finally {
            if(connection != null){
                connection.disconnect();
            }
        }
        return result;
    }

}
