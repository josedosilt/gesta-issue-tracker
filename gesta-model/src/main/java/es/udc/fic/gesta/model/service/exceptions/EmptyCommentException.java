package es.udc.fic.gesta.model.service.exceptions;

public class EmptyCommentException extends Exception {

    public EmptyCommentException(){
        super("Comment Content can't be empty");
    }
}
