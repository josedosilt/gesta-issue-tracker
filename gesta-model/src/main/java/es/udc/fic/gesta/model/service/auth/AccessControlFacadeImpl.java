package es.udc.fic.gesta.model.service.auth;

import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.issue.Issue_;
import es.udc.fic.gesta.model.entities.privileges.project.ProjectPrivileges;
import es.udc.fic.gesta.model.entities.privileges.project.ProjectPrivilegesRepository;
import es.udc.fic.gesta.model.entities.privileges.project.ProjectPrivileges_;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.project.Project_;
import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.entities.user.UserRepository;
import es.udc.fic.gesta.model.entities.user.User_;
import es.udc.fic.gesta.model.entities.utils.Privileges;
import es.udc.fic.gesta.model.util.SecurityHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Join;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

@Component
public class AccessControlFacadeImpl implements AccessControlFacade {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProjectPrivilegesRepository projectPrivilegesRepository;

    @Override
    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @Override
    public boolean hasAccessToProject(Project project) {
        Authentication auth = this.getAuthentication();
        if(auth==null || auth instanceof AnonymousAuthenticationToken){
            return false;
        }
        boolean isAdmin = SecurityHelper.isAdmin(auth.getAuthorities());
        Optional<User> user = userRepository.findUserByEmail(auth.getName());

        return user.isPresent() &&
                (isAdmin || user.get().equals(project.getProjectLeader())
                        || project.getAssignedTo().contains(user.get()));
    }

    @Override
    public boolean isAdmin() {
        Authentication auth = this.getAuthentication();
        if(auth==null || auth instanceof AnonymousAuthenticationToken){
            return false;
        }
        return SecurityHelper.isAdmin(auth.getAuthorities());
    }

    @Override
    public boolean hasPrivilegesInProject(Project project, Privileges... privileges) {
        Authentication auth = this.getAuthentication();
        if(!(auth instanceof AnonymousAuthenticationToken)){
            // Admins have privileges in all projects by default
            if (SecurityHelper.isAdmin(auth.getAuthorities())) {
                return true;
            }
            Optional<User> user = userRepository.findUserByEmail(auth.getName());
            if (user.isPresent()) {
                Optional<ProjectPrivileges> projectPrivileges =
                        projectPrivilegesRepository.findByUserAndProject(user.get(), project);
                if(projectPrivileges.isPresent()) {
                    Collection<Privileges> userPrivileges = projectPrivileges.get().getPrivileges();
                    return userPrivileges != null && userPrivileges.containsAll(Arrays.asList(privileges));
                }
            }
        }

        return false;
    }

    @Override
    public Specification<Project> accessToProjectSpecification(Privileges... privileges) {
        return (Specification<Project>) (root, criteriaQuery, builder) -> {
            Authentication auth = this.getAuthentication();
            Join<Project,ProjectPrivileges> privilegesProjectJoin = root.join(Project_.projectPrivileges);
            Join<ProjectPrivileges, Privileges> privilegesTableJoin = privilegesProjectJoin.join(ProjectPrivileges_.PRIVILEGES);
            Join<ProjectPrivileges, User> userPrivilegesJoin = privilegesProjectJoin.join(ProjectPrivileges_.user);

            return builder.and(builder.equal(userPrivilegesJoin.get(User_.email), auth.getName()),
                    privilegesTableJoin.in((Object[]) privileges));
        };
    }

    public Specification<Issue> hasAccessToIssueSpecification(Privileges... privileges){
        return (Specification<Issue>) (root, criteriaQuery, builder) -> {
            Authentication auth = this.getAuthentication();
            Join<Issue, Project> projectJoin = root.join(Issue_.project);
            Join<Project,ProjectPrivileges> privilegesProjectJoin = projectJoin.join(Project_.projectPrivileges);
            Join<ProjectPrivileges, Privileges> privilegesTableJoin = privilegesProjectJoin.join(ProjectPrivileges_.PRIVILEGES);
            Join<ProjectPrivileges, User> userPrivilegesJoin = privilegesProjectJoin.join(ProjectPrivileges_.user);

            return builder.and(builder.equal(userPrivilegesJoin.get(User_.email), auth.getName()),
                    privilegesTableJoin.in((Object[]) privileges));
        };
    }
}
