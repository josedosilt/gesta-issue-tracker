package es.udc.fic.gesta.model.entities.comment;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long>, JpaSpecificationExecutor,
        PagingAndSortingRepository<Comment, Long> {

    Page<Comment> findCommentsByIssue_IssueID(String issueID, Pageable pageable);
}
