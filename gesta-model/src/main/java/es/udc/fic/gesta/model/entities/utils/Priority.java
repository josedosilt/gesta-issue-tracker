package es.udc.fic.gesta.model.entities.utils;

public enum Priority {
    HIGH,
    MEDIUM,
    LOW;
}
