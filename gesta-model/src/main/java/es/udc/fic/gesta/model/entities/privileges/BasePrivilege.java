package es.udc.fic.gesta.model.entities.privileges;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.entities.utils.PrivilegeLevel;
import es.udc.fic.gesta.model.entities.utils.Privileges;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
public abstract class BasePrivilege {

    @Id
    @Column(name = "privilege_id")
    @GeneratedValue
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id")
    @NotNull
    private User user;

    @ElementCollection
    private Collection<Privileges> privileges;

    @PrePersist
    public void prePersist() throws InvalidPrivilegeLevelException {
        for (Privileges privilege : privileges) {
            if (!privilege.getLevels().contains(this.getPrivilegeLevel())) {
                throw new InvalidPrivilegeLevelException(privilege, this.getPrivilegeLevel());
            }
        }
    }

    @Transient
    protected abstract PrivilegeLevel getPrivilegeLevel();
}
