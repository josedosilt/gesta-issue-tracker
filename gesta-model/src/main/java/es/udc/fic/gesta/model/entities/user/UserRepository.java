package es.udc.fic.gesta.model.entities.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface UserRepository
        extends JpaRepository<User, Long>, JpaSpecificationExecutor<User>, PagingAndSortingRepository<User, Long> {

    User findByEmail(String email);

    Optional<User> findUserByEmail(String email);

    @Query("SELECT DISTINCT u " +
            "FROM User u " +
            "INNER JOIN TimeLog t ON t.user = u " +
            "INNER JOIN Issue i ON t.issue = i " +
            "WHERE i.project.code = ?1")
    Set<User> findUsersWithTimeLogsInProject(String projectId);
}
