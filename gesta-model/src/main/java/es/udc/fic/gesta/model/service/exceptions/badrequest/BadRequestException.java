package es.udc.fic.gesta.model.service.exceptions.badrequest;

public abstract class BadRequestException extends Exception {

    public BadRequestException(String msg){
        super(msg);
    }
}
