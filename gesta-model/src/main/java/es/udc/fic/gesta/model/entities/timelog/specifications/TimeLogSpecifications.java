package es.udc.fic.gesta.model.entities.timelog.specifications;

import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.issue.Issue_;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.project.Project_;
import es.udc.fic.gesta.model.entities.timelog.TimeLog;
import es.udc.fic.gesta.model.entities.timelog.TimeLog_;
import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.entities.user.User_;
import es.udc.fic.gesta.model.entities.utils.SpecificationsUtils;
import es.udc.fic.gesta.model.util.UserKeyFilter;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;

public final class TimeLogSpecifications<T extends TimeLog> {

    private TimeLogSpecifications(){}

    public static Specification<TimeLog> projectIdEqualsTo(String projectId){
        return (Specification<TimeLog>) (root, criteriaQuery, builder) -> {
            Join<TimeLog, Issue> issueJoin = root.join(TimeLog_.issue);
            Join<Issue, Project> projectJoin = issueJoin.join(Issue_.project);
            return builder.equal(projectJoin.get(Project_.code), projectId);
        };
    }

    public static Specification<TimeLog> issueIdEqualsTo(String issueId){
        return (Specification<TimeLog>) (root, criteriaBuilder, builder) ->
        {
            Join<TimeLog, Issue> issueJoin = root.join(TimeLog_.issue);
           return builder.equal(issueJoin.get(Issue_.issueID), issueId);
        };
    }

    public static Specification<TimeLog> userEmailEqualsTo(String userEmail){
        return (Specification<TimeLog>) (root, criteriaBuilder, builder) ->
        {
            Join<TimeLog, User> userJoin = root.join(TimeLog_.user);
            return builder.equal(userJoin.get(User_.email), userEmail);
        };
    }

    public static Specification<TimeLog> userEmailOrIdIn(UserKeyFilter userKeyFilter){
        return (Specification<TimeLog>) (root, criteriaBuilder, builder) ->
        {
            Join<TimeLog, User> userJoin = root.join(TimeLog_.user);
            Predicate result = null;
            if(userKeyFilter.getEmails() != null){
                result = userJoin.get(User_.email).in(userKeyFilter.getEmails());
            }
            if(userKeyFilter.getIds() != null){
                result =(result == null)
                        ? userJoin.get(User_.id).in(userKeyFilter.getIds())
                        : builder.or(result, userJoin.get(User_.id).in(userKeyFilter.getIds()));
            }
            return result;
        };
    }

    public static Specification<TimeLog> buildFilters(@Nullable String project, @Nullable String issue,
                                                      @Nullable String user, boolean isAnd) {
        Specification<TimeLog> filters = null;
        if (project != null && !project.isEmpty()) {
            filters = projectIdEqualsTo(project);
        }
        if(issue != null && !issue.isEmpty()){
            filters = SpecificationsUtils.addOrCreateFilter(filters, issueIdEqualsTo(issue), isAnd);
        }
        if(user != null && !user.isEmpty()){
            filters = SpecificationsUtils.addOrCreateFilter(filters, userEmailEqualsTo(user), isAnd);
        }
        return filters;
    }

    public static Specification<TimeLog> buildFilters(UserKeyFilter users, @Nullable String project,
                                                      @Nullable String issue, boolean isAnd) {
        Specification<TimeLog> filters = buildFilters(project, issue, null, isAnd);

        if (!((users.getIds() == null || users.getIds().isEmpty()) &&
                (users.getEmails() == null || users.getEmails().isEmpty()))) {
            filters = SpecificationsUtils.addOrCreateFilter(filters, userEmailOrIdIn(users), isAnd);
        }
        return filters;
    }
}
