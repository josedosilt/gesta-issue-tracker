package es.udc.fic.gesta.model.service.dto;

import java.util.Date;

/**
 * The type Time log dto.
 */
public class TimeLogDTO {

    private Long timeLogId;

    private Long userId;

    private String userName;

    private Double hours;

    private String issueId;

    private Date date;

    private String comment;

    /**
     * Gets time log id.
     *
     * @return the time log id
     */
    public Long getTimeLogId() {
        return timeLogId;
    }

    /**
     * Sets time log id.
     *
     * @param timeLogId the time log id
     */
    public void setTimeLogId(Long timeLogId) {
        this.timeLogId = timeLogId;
    }

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * Gets user name.
     *
     * @return the user name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets user name.
     *
     * @param userName the user name
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Gets hours.
     *
     * @return the hours
     */
    public Double getHours() {
        return hours;
    }

    /**
     * Sets hours.
     *
     * @param hours the hours
     */
    public void setHours(Double hours) {
        this.hours = hours;
    }

    /**
     * Gets issue id.
     *
     * @return the issue id
     */
    public String getIssueId() {
        return issueId;
    }

    /**
     * Sets issue id.
     *
     * @param issueId the issue id
     */
    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Gets comment.
     *
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets comment.
     *
     * @param comment the comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }
}
