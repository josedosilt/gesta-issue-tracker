package es.udc.fic.gesta.model.util;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;

public final class SecurityHelper {

    private SecurityHelper(){}

    public static Authentication getCurrentAuthentication(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            return authentication;
        }
        return null;
    }

    public static boolean isAdmin(Collection<? extends GrantedAuthority> grantedAuthorities){
        for(GrantedAuthority authority : grantedAuthorities){
            if("ROLE_ADMIN".equals(authority.getAuthority())){
                return true;
            }
        }
        return false;
    }
}
