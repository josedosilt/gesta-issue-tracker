package es.udc.fic.gesta.model.service.exceptions.badrequest;

public class OperationForbiddenException extends BadRequestException {

    public OperationForbiddenException() {
        super("User is not allowed to complete this operation");
    }
}
