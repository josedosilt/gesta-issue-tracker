package es.udc.fic.gesta.model.util.form;

import es.udc.fic.gesta.model.entities.issue.IssueType;

import java.util.Date;

public class SearchIssueForm {

    private String id;

    private String summary;

    private String assignedToNameOrEmail;

    // TODO: private IssueStatus status;

    private Date startDate;

    private Date endDate;

    private IssueType issueType;

    private String developmentCycle;

    private String projectId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getAssignedToNameOrEmail() {
        return assignedToNameOrEmail;
    }

    public void setAssignedToNameOrEmail(String assignedToNameOrEmail) {
        this.assignedToNameOrEmail = assignedToNameOrEmail;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public IssueType getIssueType() {
        return issueType;
    }

    public void setIssueType(IssueType issueType) {
        this.issueType = issueType;
    }

    /**
     * Gets development cycle.
     *
     * @return the development cycle
     */
    public String getDevelopmentCycle() {
        return developmentCycle;
    }

    /**
     * Sets development cycle.
     *
     * @param developmentCycle the development cycle
     */
    public void setDevelopmentCycle(String developmentCycle) {
        this.developmentCycle = developmentCycle;
    }

    /**
     * Gets project id.
     *
     * @return the project id
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * Sets project id.
     *
     * @param projectId the project id
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }
}
