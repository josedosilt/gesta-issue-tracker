package es.udc.fic.gesta.model.entities.attached;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import es.udc.fic.gesta.model.entities.issue.Issue;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The entity Attached File.
 */
@Entity
@Table(name = "attached_file", uniqueConstraints = @UniqueConstraint(columnNames = { "issue", "file_name" }))
@Getter
@Setter
@NoArgsConstructor
public class AttachedFile {

    @Id
    @GeneratedValue
    private Long fileId;
    @ManyToOne(optional = false)
    @JoinColumn(name = "issue")
    private Issue issue;
    @Column(name = "file_name", nullable = false)
    private String fileName;
}
