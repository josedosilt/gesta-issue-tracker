package es.udc.fic.gesta.model.entities.repository;

import es.udc.fic.gesta.model.entities.project.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GitLabRepoRepository
        extends JpaRepository<GitLabRepo, String>, JpaSpecificationExecutor<GitLabRepo>,
        PagingAndSortingRepository<GitLabRepo, String> {

    Optional<GitLabRepo> findByProject(Project project);

    Optional<GitLabRepo> findByProject_Code(String projectCode);

}
