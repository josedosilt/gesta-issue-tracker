package es.udc.fic.gesta.model.service.exceptions;

public class IssueNotFoundException extends  EntityNotFoundException{

    public IssueNotFoundException(String issueID){
        super(String.format("Issue with id %s not found", issueID));
    }
}
