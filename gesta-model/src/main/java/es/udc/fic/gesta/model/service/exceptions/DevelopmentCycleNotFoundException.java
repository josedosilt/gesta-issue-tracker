package es.udc.fic.gesta.model.service.exceptions;

public class DevelopmentCycleNotFoundException extends  EntityNotFoundException{


    public DevelopmentCycleNotFoundException(String name) {
        super(String.format("Development Cycle with name %s not found", name));
    }


    public DevelopmentCycleNotFoundException(Long id) {
        super(String.format("Development Cycle with id %s not found", id));
    }
}
