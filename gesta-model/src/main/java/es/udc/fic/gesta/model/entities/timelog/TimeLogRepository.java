package es.udc.fic.gesta.model.entities.timelog;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface TimeLogRepository extends JpaRepository<TimeLog, Long>, JpaSpecificationExecutor<TimeLog>,
        PagingAndSortingRepository<TimeLog, Long> {

    @Query("SELECT DISTINCT t.issue.project.code FROM TimeLog t WHERE t.user.email = ?1")
    List<String> getProjectIdsWithTimeLogs(String userEmail);

    @Query("SELECT DISTINCT t.issue.issueID FROM TimeLog t WHERE t.user.email = ?1")
    List<String> getIssueIdsWithTimeLogs(String userEmail);
}
