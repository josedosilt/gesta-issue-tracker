package es.udc.fic.gesta.model.service.auth;

import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.utils.Privileges;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.Authentication;

public interface AccessControlFacade {

    Authentication getAuthentication();

    boolean hasAccessToProject(Project project);

    boolean isAdmin();

    boolean hasPrivilegesInProject(Project project, Privileges... privileges);

    Specification<Project> accessToProjectSpecification(Privileges... privileges);
    Specification<Issue> hasAccessToIssueSpecification(Privileges... privileges);

}
