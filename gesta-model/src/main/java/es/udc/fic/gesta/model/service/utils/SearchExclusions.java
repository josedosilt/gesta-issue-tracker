package es.udc.fic.gesta.model.service.utils;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * The type Search exclusions.
 */
public class SearchExclusions {
    private Set<Long> excludedUsers;
    private Set<String> excludedTasks;
    private Set<String> excludedProjects;

    /**
     * Instantiates a new Search exclusions.
     */
    public SearchExclusions() {
        this.excludedUsers = new HashSet<>();
        this.excludedTasks = new HashSet<>();
        this.excludedProjects = new HashSet<>();
    }

    /**
     * Add excluded user.
     *
     * @param id the id
     * @return true if the user id is added to the excluded users collection. False otherwise.
     */
    public boolean addExcludedUser(Long id) {
        return excludedUsers.add(id);
    }

    /**
     * Add all excluded users.
     *
     * @param ids the ids
     * @return true if the user ids are added to the excluded users collections. False otherwise.
     */
    public boolean addAllExcludedUsers(@NotNull Collection<Long> ids) {
        return excludedUsers.addAll(ids);
    }

    /**
     * Remove excluded user.
     *
     * @param id the id
     * @return True if the user id is removed correctly from the excluded users collection. False otherwise.
     */
    public boolean removeExcludedUser(Long id) {
        return excludedUsers.remove(id);
    }

    /**
     * Remove all excluded users.
     *
     * @param ids the ids
     * @return True if the users ids are removed correctly from the excluded users collection. False otherwise.
     */
    public boolean removeAllExcludedUsers(@NotNull Collection<Long> ids) {
        return excludedUsers.removeAll(ids);
    }

    /**
     * Gets excluded users.
     *
     * @return the excluded users
     */
    public Set<Long> getExcludedUsers() {
        return Collections.unmodifiableSet(excludedUsers);
    }

    /**
     * Add excluded task.
     *
     * @param id the id
     * @return True if the task id is added correctly to the excluded task collection. False otherwise.
     */
    public boolean addExcludedTask(String id) {
        return excludedTasks.add(id);
    }

    /**
     * Add all excluded tasks.
     *
     * @param ids the ids
     * @return True if the task ids are added correctly to the excluded task collection. False otherwise.
     */
    public boolean addAllExcludedTasks(@NotNull Collection<String> ids) {
        return excludedTasks.addAll(ids);
    }

    /**
     * Remove excluded task.
     *
     * @param id the id
     * @return True if the task id is removed correctly to the excluded task collection. False otherwise.
     */
    public boolean removeExcludedTask(String id) {
        return excludedTasks.remove(id);
    }

    /**
     * Remove all excluded tasks.
     *
     * @param ids the ids
     * @return True if the task ids are removed correctly to the excluded task collection. False otherwise.
     */
    public boolean removeAllExcludedTasks(@NotNull Collection<String> ids) {
        return excludedTasks.removeAll(ids);
    }

    /**
     * Gets excluded tasks.
     *
     * @return the excluded tasks
     */
    public Set<String> getExcludedTasks() {
        return Collections.unmodifiableSet(excludedTasks);
    }

    /**
     * Add excluded project.
     *
     * @param id the id
     * @return True if the project id is added correctly to the excluded task collection. False otherwise.
     */
    public boolean addExcludedProject(String id) {
        return excludedProjects.add(id);
    }

    /**
     * Add all excluded projects.
     *
     * @param ids the ids
     * @return True if the project ids are added correctly to the excluded task collection. False otherwise.
     */
    public boolean addAllExcludedProjects(@NotNull Collection<String> ids) {
        return excludedProjects.addAll(ids);
    }

    /**
     * Remove excluded project.
     *
     * @param id the id
     * @return True if the project id is removed correctly to the excluded task collection. False otherwise.
     */
    public boolean removeExcludedProject(String id) {
        return excludedProjects.remove(id);
    }

    /**
     * Remove all excluded projects.
     *
     * @param ids the ids
     * @return True if the project id are removed correctly to the excluded task collection. False otherwise.
     */
    public boolean removeAllExcludedProjects(@NotNull Collection<String> ids) {
        return excludedProjects.removeAll(ids);
    }

    /**
     * Gets excluded projects.
     *
     * @return the excluded projects
     */
    public Set<String> getExcludedProjects() {
        return Collections.unmodifiableSet(excludedProjects);
    }

    /**
     * Checks if all the excluded collections are empty
     *
     * @return true if all the excluded collections are empty. False otherwise.
     */
    public boolean isEmpty() {
        return excludedUsers.isEmpty() && excludedProjects.isEmpty() && excludedTasks.isEmpty();
    }

    /**
     * Check if the excluded users collection is empty.
     *
     * @return true if excluded users is empty. False otherwise
     */
    public boolean isEmptyExcludedUsers() {
        return excludedUsers.isEmpty();
    }

    /**
     * Check if the excluded tasks collection is empty.
     *
     * @return true if excluded tasks is empty. False otherwise
     */
    public boolean isEmptyExcludedtasks() {
        return excludedTasks.isEmpty();
    }

    /**
     * Check if the excluded projects collection is empty.
     *
     * @return true if excluded projects is empty. False otherwise
     */
    public boolean isEmptyExcludedProjects() {
        return excludedProjects.isEmpty();
    }
}
