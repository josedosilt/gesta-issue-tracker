package es.udc.fic.gesta.model.util.form;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class TimeLogForm {

    @NotNull(message = "{error.emptyIssueId}")
    @NotBlank(message = "{error.emptyIssueId}")
    private String issueId;

    private String comment;

    @NotNull(message = "{error.nullHours}")
    @Min(value = 0l, message = "{error.negativeHours}")
    private Double hours;

    @NotNull(message = "{error.nullDate}")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date date;

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Double getHours() {
        return hours;
    }

    public void setHours(Double hours) {
        this.hours = hours;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
