package es.udc.fic.gesta.model.service.issue.dto;

import es.udc.fic.gesta.model.service.issue.IssueDetails;

import java.util.List;

public class RelatedTasksDTO {

    List<IssueDetails> issuesLinkedTo;
    List<IssueDetails> issuesLinkedFrom;

    public RelatedTasksDTO() {
    }

    public RelatedTasksDTO(List<IssueDetails> taskLinkedTo,
                           List<IssueDetails> taskLinkedFrom) {
        this.issuesLinkedTo = taskLinkedTo;
        this.issuesLinkedFrom = taskLinkedFrom;
    }

    public List<IssueDetails> getIssuesLinkedTo() {
        return issuesLinkedTo;
    }

    public void setIssuesLinkedTo(List<IssueDetails> issuesLinkedTo) {
        this.issuesLinkedTo = issuesLinkedTo;
    }

    public List<IssueDetails> getIssuesLinkedFrom() {
        return issuesLinkedFrom;
    }

    public void setIssuesLinkedFrom(List<IssueDetails> issuesLinkedFrom) {
        this.issuesLinkedFrom = issuesLinkedFrom;
    }
}
