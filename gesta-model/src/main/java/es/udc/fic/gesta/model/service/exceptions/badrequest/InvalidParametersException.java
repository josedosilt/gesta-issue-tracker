package es.udc.fic.gesta.model.service.exceptions.badrequest;

public class InvalidParametersException extends BadRequestException{


    public InvalidParametersException() {
        super("Invalid parameters. Some parameters might be mandatory and weren't provided");
    }
}
