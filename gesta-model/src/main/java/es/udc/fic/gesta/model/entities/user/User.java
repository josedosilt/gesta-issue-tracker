package es.udc.fic.gesta.model.entities.user;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import es.udc.fic.gesta.model.entities.privileges.project.ProjectPrivileges;
import es.udc.fic.gesta.model.entities.project.Project;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class User implements Serializable {

    private static final long serialVersionUID = -3210594041102407826L;
    @Id
    @GeneratedValue
    @Column(name = "user_id")
    private Long id;

    @Column(nullable = false, unique = true)
    private String email;

    private String fullName;

    private String password;

    private String role = "ROLE_USER";

    private Instant created;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "user_project", joinColumns = { @JoinColumn(name = "project_id") }, inverseJoinColumns = {
            @JoinColumn(name = "user_id") })
    private List<Project> projects;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
    private List<ProjectPrivileges> projectPrivileges;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
        this.created = Instant.now();
        this.projects = new ArrayList<>();
    }

    public User(String email, String password, String role) {
        this(email, password);
        this.role = role;
    }
}
