package es.udc.fic.gesta.model.service.exceptions;

public class EmailAlreadyInUseException extends Exception {

    public EmailAlreadyInUseException(String email) {
        super(String.format("Email %s is already in use by another user", email));
    }
}
