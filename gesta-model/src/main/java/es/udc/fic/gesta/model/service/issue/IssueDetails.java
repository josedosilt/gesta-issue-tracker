package es.udc.fic.gesta.model.service.issue;

import es.udc.fic.gesta.model.service.issue.dto.AttachedFileDTO;
import es.udc.fic.gesta.model.util.IssueStatus;
import es.udc.fic.gesta.model.entities.issue.IssueType;
import es.udc.fic.gesta.model.entities.utils.Priority;
import es.udc.fic.gesta.model.service.dto.DevelopmentCycleDTO;
import es.udc.fic.gesta.model.service.user.UserProfileDTO;

import java.util.Date;
import java.util.List;

/**
 * Class IssueDetails used to share data of an issue outside the service.
 */
public class IssueDetails {

    private String issueID;
    private String projectID;
    private UserProfileDTO assignedUser;
    private UserProfileDTO creator;
    private Date startDate;
    private Date endDate;
    private Double estimatedHours;
    private String description;
    private Priority priority;
    private IssueType issueType;
    private String summary;
    private DevelopmentCycleDTO developmentCycleDTO;
    private IssueStatus issueStatus;
    private boolean projectHasRepository;
    private List<AttachedFileDTO> attachedFileDTOList;

    /**
     * Gets issue id.
     *
     * @return the issue id
     */
    public String getIssueID() {
        return issueID;
    }

    /**
     * Sets issue id.
     *
     * @param issueID the issue id
     */
    public void setIssueID(String issueID) {
        this.issueID = issueID;
    }

    /**
     * Gets project id.
     *
     * @return the project id
     */
    public String getProjectID() {
        return projectID;
    }

    /**
     * Sets project id.
     *
     * @param projectID the project id
     */
    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    /**
     * Gets assigned user.
     *
     * @return the assigned user
     */
    public UserProfileDTO getAssignedUser() {
        return assignedUser;
    }

    /**
     * Sets assigned user.
     *
     * @param assignedUser the assigned user
     */
    public void setAssignedUser(UserProfileDTO assignedUser) {
        this.assignedUser = assignedUser;
    }

    /**
     * Gets creator.
     *
     * @return the creator
     */
    public UserProfileDTO getCreator() {
        return creator;
    }

    /**
     * Sets creator.
     *
     * @param creator the creator
     */
    public void setCreator(UserProfileDTO creator) {
        this.creator = creator;
    }

    /**
     * Gets start date.
     *
     * @return the start date
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Sets start date.
     *
     * @param startDate the start date
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Gets end date.
     *
     * @return the end date
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Sets end date.
     *
     * @param endDate the end date
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * Gets estimated hours.
     *
     * @return the estimated hours
     */
    public Double getEstimatedHours() {
        return estimatedHours;
    }

    /**
     * Sets estimated hours.
     *
     * @param estimatedHours the estimated hours
     */
    public void setEstimatedHours(Double estimatedHours) {
        this.estimatedHours = estimatedHours;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets priority.
     *
     * @return the priority
     */
    public Priority getPriority() {
        return priority;
    }

    /**
     * Sets priority.
     *
     * @param priority the priority
     */
    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    /**
     * Gets issue type.
     *
     * @return the issue type
     */
    public IssueType getIssueType() {
        return issueType;
    }

    /**
     * Sets issue type.
     *
     * @param issueType the issue type
     */
    public void setIssueType(IssueType issueType) {
        this.issueType = issueType;
    }

    /**
     * Gets summary.
     *
     * @return the summary
     */
    public String getSummary() {
        return summary;
    }

    /**
     * Sets summary.
     *
     * @param summary the summary
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     * Gets development cycle dto.
     *
     * @return the development cycle dto
     */
    public DevelopmentCycleDTO getDevelopmentCycleDTO() {
        return developmentCycleDTO;
    }

    /**
     * Sets development cycle dto.
     *
     * @param developmentCycleDTO the development cycle dto
     */
    public void setDevelopmentCycleDTO(DevelopmentCycleDTO developmentCycleDTO) {
        this.developmentCycleDTO = developmentCycleDTO;
    }

    /**
     * Gets issue status.
     *
     * @return the issue status
     */
    public IssueStatus getIssueStatus() {
        return issueStatus;
    }

    /**
     * Sets issue status.
     *
     * @param issueStatus the issue status
     */
    public void setIssueStatus(IssueStatus issueStatus) {
        this.issueStatus = issueStatus;
    }

    public boolean isProjectHasRepository() {
        return projectHasRepository;
    }

    public void setProjectHasRepository(boolean projectHasRepository) {
        this.projectHasRepository = projectHasRepository;
    }

    /**
     * Gets attached file dto list.
     *
     * @return the attached file dto list
     */
    public List<AttachedFileDTO> getAttachedFileDTOList() {
        return attachedFileDTOList;
    }

    /**
     * Sets attached file dto list.
     *
     * @param attachedFileDTOList the attached file dto list
     */
    public void setAttachedFileDTOList(List<AttachedFileDTO> attachedFileDTOList) {
        this.attachedFileDTOList = attachedFileDTOList;
    }
}
