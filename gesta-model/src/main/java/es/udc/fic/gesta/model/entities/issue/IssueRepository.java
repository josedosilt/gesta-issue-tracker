package es.udc.fic.gesta.model.entities.issue;

import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.cycle.DevelopmentCycle;
import es.udc.fic.gesta.model.entities.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface IssueRepository extends JpaRepository<Issue, String>, JpaSpecificationExecutor<Issue>,
        PagingAndSortingRepository<Issue, String> {

    long countByProject(Project project);

    List<Issue> findByDevelopmentCycle(DevelopmentCycle developmentCycle);

    List<Issue> findByDevelopmentCycle_Id(Long id);

    List<Issue> findByDevelopmentCycle_Name(String name);

    Page<Issue> findByProject(Project project, Pageable pageable);

    List<Issue> findByRelatedIssuesContains(Issue issue);

    Page<Issue> findIssuesByWatchersContains(User user, Pageable pageable);
}
