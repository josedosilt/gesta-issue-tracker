package es.udc.fic.gesta.model.entities.repository;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import es.udc.fic.gesta.model.entities.project.Project;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GitLabRepo {

    @Id
    private String repositoryId;

    private String repositoryName;

    private String URL;

    @OneToOne
    private Project project;

}
