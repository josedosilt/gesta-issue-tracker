package es.udc.fic.gesta.model.entities.cycle;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.project.Project;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "development_cycle", uniqueConstraints = @UniqueConstraint(columnNames = { "project", "cycle_name" }))
@Getter
@Setter
@NoArgsConstructor
public class DevelopmentCycle {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "cycle_name")
    @NotNull
    @NotEmpty
    private String name;

    @ManyToOne
    @JoinColumn(name = "project")
    @NotNull
    private Project project;

    @OneToMany(mappedBy = "developmentCycle", cascade = CascadeType.ALL)
    private List<Issue> issueList;

    private String gitBranch;

    public DevelopmentCycle(Project project, String name) {
        this.project = project;
        this.name = name;
    }
}
