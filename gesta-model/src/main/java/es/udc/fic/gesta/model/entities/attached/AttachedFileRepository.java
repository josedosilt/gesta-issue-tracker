package es.udc.fic.gesta.model.entities.attached;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import es.udc.fic.gesta.model.entities.issue.Issue;

@Repository
public interface AttachedFileRepository
        extends JpaRepository<AttachedFile, Long>, JpaSpecificationExecutor<AttachedFile>,
        PagingAndSortingRepository<AttachedFile, Long> {

    List<AttachedFile> findByIssue(Issue issue);

    List<AttachedFile> findByIssue_IssueID(String issueId);

}
