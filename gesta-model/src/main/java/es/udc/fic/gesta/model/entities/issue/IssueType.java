package es.udc.fic.gesta.model.entities.issue;

public enum IssueType {
    UPGRADE,
    TASK,
    ISSUE;
}
