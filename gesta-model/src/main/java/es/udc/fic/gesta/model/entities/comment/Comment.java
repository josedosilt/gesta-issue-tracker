package es.udc.fic.gesta.model.entities.comment;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.user.User;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Comment {

    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    @NotBlank
    private String content;

    @ManyToOne
    private User user;

    @ManyToOne
    private Issue issue;
    private Date createdDate;
    private Date modificationDate;

    @PrePersist
    public void prePersist() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        this.createdDate = calendar.getTime();
        this.modificationDate = calendar.getTime();
    }

    @PreUpdate
    public void preUpdate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        this.modificationDate = calendar.getTime();
    }

}
