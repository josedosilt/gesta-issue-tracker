package es.udc.fic.gesta.model.service.user;

import es.udc.fic.gesta.model.service.project.ProjectDetails;

import java.util.List;
import java.util.Objects;

public class UserProfileDTO {

    private Long id;

    private String email;

    private String fullName;

    private List<ProjectDetails> projects;

    public UserProfileDTO(Long id, String email, String fullName) {
        this.id = id;
        this.email = email;
        this.fullName = fullName;
    }

    public UserProfileDTO(Long id, String email, String fullName, List<ProjectDetails> projects) {
        this.id = id;
        this.email = email;
        this.projects = projects;
        this.fullName = fullName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<ProjectDetails> getProjects() {
        return projects;
    }

    public void setProjects(List<ProjectDetails> projects) {
        this.projects = projects;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserProfileDTO that = (UserProfileDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(email, that.email) &&
                Objects.equals(fullName, that.fullName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, fullName);
    }
}
