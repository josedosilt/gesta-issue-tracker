package es.udc.fic.gesta.model.service.git.dto;

public class GitCommitDTO {

    private String commitId;
    private String commitShortId;
    private String commitMessage;
    private String commiterName;
    private String commitDate;
    private String projectUrl;

    public String getCommitId() {
        return commitId;
    }

    public void setCommitId(String commitId) {
        this.commitId = commitId;
    }

    public String getCommitShortId() {
        return commitShortId;
    }

    public void setCommitShortId(String commitShortId) {
        this.commitShortId = commitShortId;
    }

    public String getCommitMessage() {
        return commitMessage;
    }

    public void setCommitMessage(String commitMessage) {
        this.commitMessage = commitMessage;
    }

    public String getCommiterName() {
        return commiterName;
    }

    public void setCommiterName(String commiterName) {
        this.commiterName = commiterName;
    }

    public String getCommitDate() {
        return commitDate;
    }

    public void setCommitDate(String commitDate) {
        this.commitDate = commitDate;
    }

    public String getProjectUrl() {
        return projectUrl;
    }

    public void setProjectUrl(String projectUrl) {
        this.projectUrl = projectUrl;
    }

    public String getCommitUrl(){
        StringBuilder sb = new StringBuilder(projectUrl);
        sb.append("/commit/").append(this.commitShortId);
        return sb.toString();
    }
}
