package es.udc.fic.gesta.model.service.timelog;

import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.issue.IssueRepository;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.project.ProjectRepository;
import es.udc.fic.gesta.model.entities.timelog.TimeLog;
import es.udc.fic.gesta.model.entities.timelog.TimeLogRepository;
import es.udc.fic.gesta.model.entities.timelog.specifications.TimeLogSpecifications;
import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.entities.user.UserRepository;
import es.udc.fic.gesta.model.service.dto.TimeLogDTO;
import es.udc.fic.gesta.model.service.exceptions.IssueNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.BadRequestException;
import es.udc.fic.gesta.model.service.exceptions.UserNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.InvalidParametersException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.user.UserProfileDTO;
import es.udc.fic.gesta.model.service.utils.EntityAdapters;
import es.udc.fic.gesta.model.util.UserKeyFilter;
import es.udc.fic.gesta.model.util.form.TimeLogForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
@Validated
public class TimeLogService {

    @Autowired
    private TimeLogRepository timeLogRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private IssueRepository issueRepository;
    @Autowired
    private ProjectRepository projectRepository;


    @Transactional(dontRollbackOn = {IssueNotFoundException.class, UserNotFoundException.class})
    public void logTime(String userEmail, @Valid TimeLogForm timeLogForm) throws UserNotFoundException,
            IssueNotFoundException {
        User user = userRepository.findByEmail(userEmail);
        if(user == null){
            throw new UserNotFoundException(new HashMap<String, Object>(){{
                put("email", userEmail);
            }});
        }
        Issue issue = issueRepository.findById(timeLogForm.getIssueId())
                .orElseThrow(() -> new IssueNotFoundException(timeLogForm.getIssueId()));

        TimeLog timeLog = new TimeLog();
        timeLog.setUser(user);
        timeLog.setIssue(issue);
        timeLog.setComment(timeLogForm.getComment());
        timeLog.setDate(timeLogForm.getDate());
        timeLog.setHours(BigDecimal.valueOf(timeLogForm.getHours()));

        timeLogRepository.save(timeLog);
    }

    @Transactional(dontRollbackOn = UserNotFoundException.class)
    public List<TimeLogDTO> getUserTimeLogs(String userEmail, String issueId, String projectId) throws
            UserNotFoundException {
        User user = userRepository.findByEmail(userEmail);
        if(user == null){
            throw new UserNotFoundException(userEmail);
        }

        return getTimeLogDTOS(TimeLogSpecifications.buildFilters(projectId, issueId, userEmail, true));
    }

    @Transactional(dontRollbackOn = {UserNotFoundException.class, BadRequestException.class})
    public List<TimeLogDTO> getProjectOrIssueTimeLogs(String leaderEmail, String issueId, String projectId,
                                                      UserKeyFilter users) throws UserNotFoundException,
            BadRequestException {

        User user = userRepository.findByEmail(leaderEmail);
        if (user == null) {
            throw new UserNotFoundException(leaderEmail);
        }
        Optional<Project> project;
        if(projectId == null || projectId.isEmpty()){
            if(issueId == null || issueId.isEmpty()){
                throw new InvalidParametersException();
            }
            project = projectRepository.findByIssueId(issueId);
        } else{
            project = projectRepository.findProjectByCode(projectId);
        }

        if(!project.isPresent() || !user.equals(project.get().getProjectLeader())){
            throw new OperationForbiddenException();
        }

        return getTimeLogDTOS(TimeLogSpecifications.buildFilters(users, projectId, issueId, true));
    }

    public List<TimeLogDTO> getProjectTimeLogs(String leaderEmail, String projectId,
                                                @Nullable UserKeyFilter users) throws UserNotFoundException,
            BadRequestException {
        return getProjectOrIssueTimeLogs(leaderEmail, null, projectId, users);
    }

    public List<TimeLogDTO> getIssueTimeLogs(String leaderEmail, String issueId,
                                                @Nullable UserKeyFilter users) throws UserNotFoundException,
            BadRequestException {
        return getProjectOrIssueTimeLogs(leaderEmail, issueId, null, users);
    }

    private List<TimeLogDTO> getTimeLogDTOS(Specification<TimeLog> timeLogSpecification) {
        List<TimeLog> timeLogs =
                timeLogRepository.findAll(timeLogSpecification);
        List<TimeLogDTO> timeLogDTOS = new ArrayList<>();
        for(TimeLog  timeLog : timeLogs){
            timeLogDTOS.add(EntityAdapters.createTimeLogDTO(timeLog));
        }
        return timeLogDTOS;
    }

    @Transactional
    public List<String> getProjectIdsFromTimeLogs(String userEmail) {

        return timeLogRepository.getProjectIdsWithTimeLogs(userEmail);
    }

    @Transactional
    public List<String> getIssueIdsFromTimeLogs(String userEmail) {
        return timeLogRepository.getIssueIdsWithTimeLogs(userEmail);
    }

    @Transactional
    public List<UserProfileDTO> getUserFromProjectWithTimeLogs(String projectId){
        List<UserProfileDTO> userProfileDTOS = new ArrayList<>();
        for(User user : userRepository.findUsersWithTimeLogsInProject(projectId)){
            userProfileDTOS.add(EntityAdapters.createUserProfile(user, false));
        }
        return userProfileDTOS;
    }
}
