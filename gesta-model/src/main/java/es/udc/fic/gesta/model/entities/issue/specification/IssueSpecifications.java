package es.udc.fic.gesta.model.entities.issue.specification;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

import org.springframework.data.jpa.domain.Specification;

import es.udc.fic.gesta.model.entities.cycle.DevelopmentCycle;
import es.udc.fic.gesta.model.entities.cycle.DevelopmentCycle_;
import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.issue.IssueType;
import es.udc.fic.gesta.model.entities.issue.Issue_;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.project.Project_;
import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.entities.user.User_;
import es.udc.fic.gesta.model.entities.utils.SpecificationsUtils;
import es.udc.fic.gesta.model.service.utils.SearchExclusions;
import es.udc.fic.gesta.model.util.DataAdapters;
import es.udc.fic.gesta.model.util.form.SearchIssueForm;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class IssueSpecifications {

    public static Specification<Issue> assignedToEmailOrNameLike(String nameOrMail) {
        return (Specification<Issue>) (root, criteriaQuery, builder) -> {

            Join<Issue, User> userJoin = root.join(Issue_.assignedTo);

            return builder.or(
                    builder.like(userJoin.get(User_.email), "%" + nameOrMail + "%"),
                    builder.like(builder.lower(userJoin.get(User_.fullName)), "%" + nameOrMail.toLowerCase() + "%"));
        };
    }

    public static Specification<Issue> endDateLessThanOrEqualsTo(Date endDate) {
        return (Specification<Issue>) (root, criteriaQuery, builder) -> {
            Calendar calendar = DataAdapters
                    .clearCalendarFields(DataAdapters.getCalendarFromDate(endDate), Calendar.MILLISECOND,
                            Calendar.SECOND, Calendar.MINUTE, Calendar.HOUR);

            return builder.lessThanOrEqualTo(root.get(Issue_.endDate), calendar.getTime());
        };
    }

    public static Specification<Issue> issueIdLike(String issueId) {
        return (Specification<Issue>) (root, criteriaQuery, builder) -> builder
                .like(builder.lower(root.get(Issue_.issueID)), "%" + issueId.toLowerCase() + "%");
    }

    public static Specification<Issue> startDateLessThanOrEqualsTo(Date startDate) {
        return (Specification<Issue>) (root, criteriaQuery, builder) -> {
            Calendar calendar = DataAdapters
                    .clearCalendarFields(DataAdapters.getCalendarFromDate(startDate), Calendar.MILLISECOND,
                            Calendar.SECOND, Calendar.MINUTE, Calendar.HOUR);

            return builder.greaterThanOrEqualTo(root.get(Issue_.startDate), calendar.getTime());
        };
    }

    public static Specification<Issue> summaryLike(String summary) {
        return (Specification<Issue>) (root, criteriaQuery, builder) -> builder
                .like(builder.lower(root.get(Issue_.summary)), "%" + summary.toLowerCase() + "%");
    }

    public static Specification<Issue> userHasProjectAssigned(String userEmail) {
        return (Specification<Issue>) (root, criteriaQuery, builder) -> {
            Join<Issue, Project> projectJoin = root.join(Issue_.project);
            Join<Project, User> assignedToJoin = projectJoin.join(Project_.assignedTo);
            Join<Project, User> projectLeaderJoin = projectJoin.join(Project_.projectLeader);

            return builder.or(
                    builder.equal(projectLeaderJoin.get(User_.email), userEmail),
                    builder.equal(assignedToJoin.get(User_.email), userEmail));

        };
    }

    public static Specification<Issue> issueTypeEqualsTo(IssueType issueType) {
        return (Specification<Issue>) (root, criteriaQuery, builder) -> builder.equal(root.get(Issue_.type), issueType);
    }

    public static Specification<Issue> issueFromProject(String projectId) {
        return (Specification<Issue>) (root, criteriaQuery, builder) -> {
            Join<Issue, Project> projectJoin = root.join(Issue_.project);
            return builder.equal(projectJoin.get(Project_.code), projectId);
        };
    }

    public static Specification<Issue> issueInDevelopmentCycle(String cycle) {
        return (Specification<Issue>) (root, criteriaQuery, builder) -> {
            Join<Issue, DevelopmentCycle> cycleJoin = root.join(Issue_.developmentCycle);
            return builder.equal(cycleJoin.get(DevelopmentCycle_.name), cycle);
        };
    }

    public static Optional<Specification<Issue>> getSpecificationFromSearchIssueForm(SearchIssueForm searchIssueForm) {
        Specification<Issue> filter = null;
        if (searchIssueForm.getAssignedToNameOrEmail() != null &&
                !searchIssueForm.getAssignedToNameOrEmail().isEmpty()) {
            filter = assignedToEmailOrNameLike(searchIssueForm.getAssignedToNameOrEmail());
        }
        if (searchIssueForm.getId() != null && !searchIssueForm.getId().isEmpty()) {
            filter = SpecificationsUtils.addOrCreateFilterWithAnd(Specification.where(filter),
                    issueIdLike(searchIssueForm.getId()));
        }
        if (searchIssueForm.getSummary() != null && !searchIssueForm.getSummary().isEmpty()) {
            filter = SpecificationsUtils.addOrCreateFilterWithAnd(filter, summaryLike(searchIssueForm.getSummary()));
        }
        if (searchIssueForm.getEndDate() != null) {
            filter = SpecificationsUtils
                    .addOrCreateFilterWithAnd(filter, endDateLessThanOrEqualsTo(searchIssueForm.getEndDate()));
        }
        if (searchIssueForm.getStartDate() != null) {
            filter = SpecificationsUtils
                    .addOrCreateFilterWithAnd(filter, startDateLessThanOrEqualsTo(searchIssueForm.getStartDate()));
        }
        if (searchIssueForm.getIssueType() != null) {
            filter = SpecificationsUtils
                    .addOrCreateFilterWithAnd(filter, issueTypeEqualsTo(searchIssueForm.getIssueType()));
        }
        if (searchIssueForm.getProjectId() != null) {
            filter = SpecificationsUtils
                    .addOrCreateFilterWithAnd(filter, issueFromProject(searchIssueForm.getProjectId()));
        }
        if (searchIssueForm.getDevelopmentCycle() != null && !searchIssueForm.getDevelopmentCycle().isEmpty()) {
            filter = SpecificationsUtils
                    .addOrCreateFilterWithAnd(filter, issueInDevelopmentCycle(searchIssueForm.getDevelopmentCycle()));
        }
        return Optional.ofNullable(filter);
    }

    public static Specification<Issue> applyExclusions(Specification<Issue> currentFilter,
            SearchExclusions searchExclusions) {
        if (searchExclusions == null || searchExclusions.isEmpty()) {
            return currentFilter;
        }
        Specification<Issue> newFilter = (Specification<Issue>) (root, criteriaQuery, builder) -> {
            Predicate predicate = null;
            if (!searchExclusions.isEmptyExcludedtasks()) {
                final Path<String> issueIdPath = root.get(Issue_.issueID);
                predicate = issueIdPath.in(searchExclusions.getExcludedTasks()).not();
            }
            if (!searchExclusions.isEmptyExcludedProjects()) {
                final Join<Issue, Project> projectJoin = root.join(Issue_.project);
                final Path<String> projectId = projectJoin.get(Project_.code);
                final Predicate newPredicate = projectId.in(searchExclusions.getExcludedProjects()).not();
                if (predicate == null) {
                    predicate = newPredicate;
                } else {
                    predicate = builder.and(predicate, newPredicate);
                }
            }
            if (!searchExclusions.isEmptyExcludedUsers()) {
                final Join<Issue, User> userJoin = root.join(Issue_.assignedTo);
                final Path<Long> userId = userJoin.get(User_.id);
                final Predicate newPredicate = userId.in(searchExclusions.getExcludedUsers()).not();
                if (predicate == null) {
                    predicate = newPredicate;
                } else {
                    predicate = builder.and(predicate, newPredicate);
                }
            }
            return predicate;
        };

        return SpecificationsUtils.addOrCreateFilterWithAnd(currentFilter, newFilter);
    }
}
