package es.udc.fic.gesta.model.service.exceptions.cycle;

public class NameAlreadyExistsForProjectException extends BadNameException {

    public NameAlreadyExistsForProjectException(String name, String projectId) {
        super(String.format("Name %s already exists for the project %s", name, projectId));
    }
}
