package es.udc.fic.gesta.model.entities.cycle;

import es.udc.fic.gesta.model.entities.project.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface DevelopmentCycleRepository
        extends JpaRepository<DevelopmentCycle, Long>, JpaSpecificationExecutor<DevelopmentCycle>,
        PagingAndSortingRepository<DevelopmentCycle, Long> {

    Optional<DevelopmentCycle> findByName(String name);

    Optional<DevelopmentCycle> findByProject_CodeAndName(String projectId, String name);

    Page<DevelopmentCycle> findByProject(Project project, Pageable pageable);

    Page<DevelopmentCycle> findByProjectAndNameContainsIgnoreCase(Project project, String name,  Pageable pageable);

    Page<DevelopmentCycle> findByProject_Code(String projectCode, Pageable pageable);

    Page<DevelopmentCycle> findByProject_CodeAndNameContainsIgnoreCase(String projectCode, String name, Pageable pageable);

    boolean existsByNameAndProject(String name, Project project);

    boolean existsByNameAndProject_Code(String name, String projectCode);
}
