package es.udc.fic.gesta.model.util.form;

import es.udc.fic.gesta.model.util.ProjectState;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ProjectForm {

    @NotNull(message = "{project.error.emptyName}")
    @NotBlank(message = "{project.error.emptyName}")
    public String projectName;

    @NotNull(message = "{project.error.nullState}")
    public ProjectState projectState;

    @NotNull(message = "{project.error.leaderNull}")
    public Long projectLeaderID;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public ProjectState getProjectState() {
        return projectState;
    }

    public void setProjectState(ProjectState projectState) {
        this.projectState = projectState;
    }

    public Long getProjectLeaderID() {
        return projectLeaderID;
    }

    public void setProjectLeaderID(Long projectLeaderID) {
        this.projectLeaderID = projectLeaderID;
    }
}
