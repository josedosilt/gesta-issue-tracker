package es.udc.fic.gesta.model.service.user;

import es.udc.fic.gesta.model.service.utils.SearchExclusions;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.project.Project_;
import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.entities.user.User_;
import es.udc.fic.gesta.model.entities.utils.SpecificationsUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

public class UserSpecifications {

    /**
     * Creates a predicate to filter users by email and/or name.
     *
     * @param email
     * @param name
     * @param isAnd Marks if the filter should be a logical and. If false, the filter will be using a logical or.
     * @return
     */
    public static Specification<User> nameOrEmailsEqualsTo(String email, String name, boolean isAnd) {
        return (Specification<User>) (root, criteriaQuery, builder) -> {
            Predicate predicateMail = null;
            Predicate predicateUser = null;
            if (email != null && !email.isEmpty()) {
                predicateMail = builder.like(root.get(User_.email), "%" + email + "%");
                if (name == null || name.isEmpty()) {
                    return predicateMail;
                }
            }
            if (name != null && !name.isEmpty()) {
                predicateUser =
                        builder.like(builder.lower(root.get(User_.fullName)), "%" + name.toLowerCase() + "%");
                if (email == null || email.isEmpty()) {
                    return predicateUser;
                }
            }
            if (isAnd) {
                return builder.and(predicateUser, predicateMail);
            }
            return builder.or(predicateUser, predicateMail);
        };
    }

    /**
     * Creates a predicate to filter users based in a project code on the projects asignet to that user
     *
     * @param projectCode
     * @return
     */
    public static Specification<User> withProjectAsigned(String projectCode) {
        return (Specification<User>) (root, criteriaQuery, builder) -> {
            Join<User, Project> project = root.join(User_.PROJECTS);
            return builder.equal(project.get(Project_.CODE), projectCode);
        };
    }

    /**
     * Apply to a user filter the exclusions specified in {@link SearchExclusions}.
     *
     * @param currentFilter    the current filter
     * @param searchExclusions the search exclusions
     * @return the specification
     */
    public static Specification<User> applyExclusions(Specification<User> currentFilter,
                                                      SearchExclusions searchExclusions) {
        if (searchExclusions == null || searchExclusions.isEmpty()) {
            return currentFilter;
        }
        Specification<User> newFilter = (Specification<User>) (root, criteriaQuery, builder) -> {
            Predicate predicate = null;
            if (!searchExclusions.isEmptyExcludedUsers()) {
                final Path<Long> userGroup = root.get(User_.id);
                predicate = userGroup.in(searchExclusions.getExcludedUsers()).not();
            }
            if (!searchExclusions.isEmptyExcludedProjects()) {
                Join<User, Project> projectJoin = root.join(User_.projects);
                final Path<String> projectPath = projectJoin.get(Project_.code);
                final Predicate projectPredicate = projectPath.in(searchExclusions.getExcludedProjects()).not();
                if (predicate == null) {
                    predicate = projectPredicate;
                } else {
                    predicate = builder.or(predicate, projectPredicate);
                }
            }
            return predicate;
        };
        return SpecificationsUtils.addOrCreateFilterWithAnd(currentFilter, newFilter);
    }
}
