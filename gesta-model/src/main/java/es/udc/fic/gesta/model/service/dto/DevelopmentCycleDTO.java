package es.udc.fic.gesta.model.service.dto;

import es.udc.fic.gesta.model.service.issue.IssueDetails;

import java.util.ArrayList;
import java.util.List;

public class DevelopmentCycleDTO {

    public Long id;

    public String name;

    public String projectId;

    public List<IssueDetails> issueList;

    private String gitBranch;

    private String gitBranchUrl;

    public DevelopmentCycleDTO(Long id, String name, String projectId) {
        this.id = id;
        this.name = name;
        this.projectId = projectId;
        issueList = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public List<IssueDetails> getIssueList() {
        return issueList;
    }

    public void setIssueList(List<IssueDetails> issueList) {
        this.issueList = issueList;
    }

    public String getGitBranch() {
        return gitBranch;
    }

    public void setGitBranch(String gitBranch) {
        this.gitBranch = gitBranch;
    }

    public String getGitBranchUrl() {
        return gitBranchUrl;
    }

    public void setGitBranchUrl(String gitBranchUrl) {
        this.gitBranchUrl = gitBranchUrl;
    }
}
