package es.udc.fic.gesta.model.service.issue;

import es.udc.fic.gesta.model.service.exceptions.*;
import es.udc.fic.gesta.model.service.utils.EntityAdapters;
import es.udc.fic.gesta.model.service.utils.SearchExclusions;
import es.udc.fic.gesta.model.entities.attached.AttachedFile;
import es.udc.fic.gesta.model.entities.attached.AttachedFileRepository;
import es.udc.fic.gesta.model.entities.comment.Comment;
import es.udc.fic.gesta.model.entities.comment.CommentRepository;
import es.udc.fic.gesta.model.entities.comment.Comment_;
import es.udc.fic.gesta.model.entities.cycle.DevelopmentCycle;
import es.udc.fic.gesta.model.entities.cycle.DevelopmentCycleRepository;
import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.issue.IssueRepository;
import es.udc.fic.gesta.model.entities.issue.Issue_;
import es.udc.fic.gesta.model.entities.issue.specification.IssueSpecifications;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.project.ProjectRepository;
import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.entities.user.UserRepository;
import es.udc.fic.gesta.model.entities.utils.Privileges;
import es.udc.fic.gesta.model.entities.utils.SpecificationsUtils;
import es.udc.fic.gesta.model.service.auth.AccessControlFacade;
import es.udc.fic.gesta.model.service.dto.CommentDTO;
import es.udc.fic.gesta.model.service.exceptions.*;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.issue.dto.RelatedTasksDTO;
import es.udc.fic.gesta.model.service.project.exception.ProjectNotFoundException;
import es.udc.fic.gesta.model.service.user.UserProfileDTO;
import es.udc.fic.gesta.model.util.IssueForm;
import es.udc.fic.gesta.model.util.IssueStatus;
import es.udc.fic.gesta.model.util.form.SearchIssueForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Service
@Validated
public class IssueService {

    private Path attachedFilePath;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private IssueRepository issueRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private DevelopmentCycleRepository developmentCycleRepository;
    @Autowired
    private AccessControlFacade accessControlFacade;
    @Autowired
    private AttachedFileRepository attachedFileRepository;

    @Autowired
    public IssueService(@Value("${gesta.attachedFilePath:attached}") String attachedFilePath){
        this.attachedFilePath = Paths.get(attachedFilePath);
    }

    @Transactional
    public Issue createIssue(@Valid IssueForm issueForm, String creatorEmail) throws UserNotFoundException,
            ProjectNotFoundException, ProjectNotAssignedException, OperationForbiddenException {
        Project project = projectRepository.findByCode(issueForm.getProjectID());
        if (project == null) {
            throw new ProjectNotFoundException(issueForm.getProjectID());
        }
        if(!accessControlFacade.hasPrivilegesInProject(project, Privileges.CREATE_TASK_PRIVILEGE)){
            throw new OperationForbiddenException();
        }
        long issueNumber = issueRepository.countByProject(project) + 1;
        Issue issue = new Issue();
        issue.setIssueID(issueForm.getProjectID() + "-" + issueNumber);
        issue.setProject(project);

        User creator = userRepository.findByEmail(creatorEmail);
        if (creator == null) {
            throw new UsernameNotFoundException(creatorEmail);
        }

        if (!creator.equals(project.getProjectLeader()) && !project.getAssignedTo().contains(creator)) {
            throw new ProjectNotAssignedException(project.getCode(), creatorEmail);
        }

        issue.setCreatedBy(creator);
        if (issueForm.getAssignedTo() != null) {
            Optional<User> assignedTo = userRepository.findById(issueForm.getAssignedTo());
            if (!assignedTo.isPresent()) {
                throw new UserNotFoundException(issueForm.getAssignedTo());
            }
            issue.setAssignedTo(assignedTo.get());
        }
        if(issueForm.getDevelopmentCycle() != null){
            issue.setDevelopmentCycle(
                    developmentCycleRepository
                            .findByProject_CodeAndName(project.getCode(), issueForm.getDevelopmentCycle())
                            .orElse(null));
        }
        issue.setEstimatedHours(issueForm.getEstimatedHours());
        issue.setPriority(issueForm.getPriority());
        issue.setSummary(issueForm.getSummary());
        issue.setType(issueForm.getIssueType());
        issue.setIssueStatus(IssueStatus.OPEN);
        return issueRepository.save(issue);
    }

    @Transactional
    public IssueDetails getIssueDetails(String issueId) throws IssueNotFoundException, OperationForbiddenException {
        return getIssueDetails(issueId, false);
    }

    @Transactional
    public IssueDetails getIssueDetails(String issueId, boolean recoverAttachedFiles) throws IssueNotFoundException,
            OperationForbiddenException {
        Issue issue = issueRepository.findById(issueId).orElseThrow(() -> new IssueNotFoundException(issueId));
        if (!accessControlFacade.hasPrivilegesInProject(issue.getProject(), Privileges.READ_PRIVILEGE)) {
            throw new OperationForbiddenException();
        }
        return EntityAdapters.createIssueDetails(issue, recoverAttachedFiles);
    }

    @Transactional(dontRollbackOn = {IssueNotFoundException.class, UserNotFoundException.class})
    public IssueDetails editIssue(@Valid IssueForm issueForm, String issueID) throws IssueNotFoundException,
            UserNotFoundException, OperationForbiddenException {

        Optional<Issue> optionalIssue = issueRepository.findById(issueID);
        if (!optionalIssue.isPresent()) {
            throw new IssueNotFoundException(issueID);
        }
        if (!accessControlFacade.hasPrivilegesInProject(optionalIssue.get().getProject(), Privileges.WRITE_PRIVILEGE)){
            throw new OperationForbiddenException();
        }
        Issue issue = optionalIssue.get();
        issue.setDescription(issueForm.getDescription());
        issue.setType(issueForm.getIssueType());
        issue.setSummary(issueForm.getSummary());
        issue.setEstimatedHours(issueForm.getEstimatedHours());
        issue.setPriority(issueForm.getPriority());

        Optional<User> user = userRepository.findById(issueForm.getAssignedTo());
        if (!user.isPresent()) {
            throw new UserNotFoundException(issueForm.getAssignedTo());
        }
        if (!user.get().equals(issue.getAssignedTo())) {
            issue.setAssignedTo(user.get());
        }

        if (issueForm.getDevelopmentCycle() != null && !issueForm.getDevelopmentCycle().isEmpty()) {
            DevelopmentCycle developmentCycle =
                    developmentCycleRepository
                            .findByProject_CodeAndName(issue.getProject().getCode(), issueForm.getDevelopmentCycle())
                            .orElse(null);
            if (developmentCycle != issue.getDevelopmentCycle() && developmentCycle != null &&
                    !developmentCycle.equals(issue.getDevelopmentCycle())) {
                issue.setDevelopmentCycle(developmentCycle);
            }
        }else{
            issue.setDevelopmentCycle(null);
        }

        return EntityAdapters.createIssueDetails(issueRepository.save(issue));
    }

    @Transactional
    public Page<CommentDTO> getComments(String issueId, int page, int pageSize) throws OperationForbiddenException {
        Optional<Issue> optionalIssue = issueRepository.findById(issueId);
        if (!optionalIssue.isPresent() || !accessControlFacade
                .hasPrivilegesInProject(optionalIssue.get().getProject(), Privileges.READ_PRIVILEGE)) {
            throw new OperationForbiddenException();
        }
        List<CommentDTO> commentDTOS = new ArrayList<>();
        Page<Comment> commentPage =
                commentRepository.findCommentsByIssue_IssueID(issueId, PageRequest.of(page, pageSize,
                        Sort.by(Sort.Direction.DESC, Comment_.CREATED_DATE)));
        if (!commentPage.isEmpty()) {
            for (Comment comment : commentPage.getContent()) {
                commentDTOS.add(EntityAdapters.createCommentDTO(comment));
            }
        }
        return new PageImpl<>(commentDTOS, commentPage.getPageable(), commentPage.getTotalElements());
    }

    @Transactional
    public void commentIssue(String issueId, String creatorEmail, String commentContent) throws IssueNotFoundException,
            UserNotFoundException, EmptyCommentException, OperationForbiddenException {
        Comment comment = new Comment();
        User user = userRepository.findByEmail(creatorEmail);
        if (user == null) {
            throw new UserNotFoundException(new HashMap<String, Object>() {{
                put("email", creatorEmail);
            }});
        }
        comment.setUser(user);

        Optional<Issue> issue = issueRepository.findById(issueId);
        if (!issue.isPresent()) {
            throw new IssueNotFoundException(issueId);
        }
        if (!accessControlFacade
                .hasPrivilegesInProject(issue.get().getProject(), Privileges.READ_PRIVILEGE)) {
            throw new OperationForbiddenException();
        }
        comment.setIssue(issue.get());
        if (commentContent == null || commentContent.isEmpty()) {
            throw new EmptyCommentException();
        }
        comment.setContent(commentContent);

        commentRepository.save(comment);
    }

    @Transactional(dontRollbackOn = UserNotFoundException.class)
    public Page<IssueDetails> searchIssues(SearchIssueForm searchIssueForm,
                                           SearchExclusions searchExclusions, int page,
                                           int pageSize) {
        Specification<Issue> filter = null;
        if (!accessControlFacade.isAdmin()) {
            filter = accessControlFacade.hasAccessToIssueSpecification(Privileges.READ_PRIVILEGE);
        }

        Optional<Specification<Issue>> issueSpecification =
                IssueSpecifications.getSpecificationFromSearchIssueForm(searchIssueForm);
        if (issueSpecification.isPresent()) {
            filter = SpecificationsUtils.addOrCreateFilterWithAnd(filter, issueSpecification.get());
        }

        filter= IssueSpecifications.applyExclusions(filter, searchExclusions);
        Page<Issue> issuePage;
        Pageable pageable = PageRequest.of(page, pageSize, Sort.by(Issue_.ISSUE_ID));
        if (filter != null) {
            issuePage = issueRepository
                    .findAll(filter, pageable);
        } else {
            issuePage = issueRepository.findAll(pageable);
        }
        List<IssueDetails> resultContent = new ArrayList<>();
        if (!issuePage.isEmpty()) {
            for (Issue issue : issuePage.getContent()) {
                resultContent.add(EntityAdapters.createIssueDetails(issue));
            }
        }

        return new PageImpl<>(resultContent, PageRequest.of(page, pageSize), issuePage.getTotalElements());
    }

    @Transactional(dontRollbackOn = {IssueNotFoundException.class, UserNotFoundException.class})
    public IssueDetails addWatchers(@NotNull String issueID, @NotNull @NotEmpty Set<Long> watchersIds) throws
            IssueNotFoundException, UserNotFoundException, OperationForbiddenException {
        Issue issue = issueRepository.findById(issueID).orElseThrow(() -> new IssueNotFoundException(issueID));

        if (!accessControlFacade
                .hasPrivilegesInProject(issue.getProject(), Privileges.WRITE_PRIVILEGE)) {
            throw new OperationForbiddenException();
        }
        List<User> watchers = userRepository.findAllById(watchersIds);
        if (watchers.size() != watchersIds.size()) {
            List<Long> watchersIdsCopy = new ArrayList<>(watchersIds);
            for (User user : watchers) {
                watchersIdsCopy.remove(user.getId());
            }
            if (!watchersIdsCopy.isEmpty()) {
                throw new UserNotFoundException(watchersIdsCopy);
            }
        }
        if (issue.getWatchers() != null) {
            issue.getWatchers().addAll(watchers);
        } else {
            issue.setWatchers(new HashSet<>(watchers));
        }
        return EntityAdapters.createIssueDetails(issueRepository.save(issue));
    }

    @Transactional
    public List<UserProfileDTO> getIssueWatchers(String issueId) throws IssueNotFoundException,
            OperationForbiddenException {

        List<UserProfileDTO> watchers = new ArrayList<>();
        Issue issue = issueRepository.findById(issueId).orElseThrow(() -> new IssueNotFoundException(issueId));
        if (!accessControlFacade
                .hasPrivilegesInProject(issue.getProject(), Privileges.READ_PRIVILEGE)) {
            throw new OperationForbiddenException();
        }
        if (issue.getWatchers() != null) {
            for (User user : issue.getWatchers()) {
                watchers.add(EntityAdapters.createUserProfile(user, false));
            }
        }
        return watchers;
    }

    @Transactional(dontRollbackOn = {UserNotFoundException.class, IssueNotFoundException.class})
    public void removeFromWatchers(String userEmail, String issueId) throws UserNotFoundException,
            IssueNotFoundException, OperationForbiddenException {
        User user = userRepository.findUserByEmail(userEmail).orElseThrow(() -> new UserNotFoundException(userEmail));
        Issue issue = issueRepository.findById(issueId).orElseThrow(() -> new IssueNotFoundException(issueId));
        if (!accessControlFacade
                .hasPrivilegesInProject(issue.getProject(), Privileges.WRITE_PRIVILEGE)) {
            throw new OperationForbiddenException();
        }
        Set<User> watchers = issue.getWatchers();
        if (watchers != null) {
            watchers.remove(user);
            issue.setWatchers(watchers);
            issueRepository.save(issue);
        }
    }

    @Transactional(dontRollbackOn = {UserNotFoundException.class, IssueNotFoundException.class,
            OperationForbiddenException.class})
    public void removeWatchers(String userEmail, String issueId, @NotNull @NotEmpty List<Long> idsToRemove) throws
            UserNotFoundException, IssueNotFoundException, OperationForbiddenException {
        User user = userRepository.findUserByEmail(userEmail).orElseThrow(() -> new UserNotFoundException(userEmail));
        Issue issue = issueRepository.findById(issueId).orElseThrow(() -> new IssueNotFoundException(issueId));
        if (!accessControlFacade
                .hasPrivilegesInProject(issue.getProject(), Privileges.WRITE_PRIVILEGE)) {
            throw new OperationForbiddenException();
        }
        if (!(issue.getCreatedBy().equals(user)
                || issue.getAssignedTo().equals(user)
                || issue.getProject().getProjectLeader().equals(user))) {
            throw new OperationForbiddenException();
        }
        List<User> usersToRemove = userRepository.findAllById(idsToRemove);
        Set<User> watchers = issue.getWatchers();
        if (usersToRemove != null && !usersToRemove.isEmpty() && watchers != null) {
            watchers.removeAll(usersToRemove);
            issue.setWatchers(watchers);
            issueRepository.save(issue);
        }
    }

    @Transactional(dontRollbackOn = {IssueNotFoundException.class, UserNotFoundException.class})
    public boolean canRemoveWatchers(String userEmail, String issueId) throws IssueNotFoundException,
            UserNotFoundException {

        User user = userRepository.findUserByEmail(userEmail).orElseThrow(() -> new UserNotFoundException(userEmail));
        Issue issue = issueRepository.findById(issueId).orElseThrow(() -> new IssueNotFoundException(issueId));
        return accessControlFacade.hasPrivilegesInProject(issue.getProject(), Privileges.WRITE_PRIVILEGE) &&
                (issue.getCreatedBy().equals(user)
                        || issue.getAssignedTo().equals(user)
                        || issue.getProject().getProjectLeader().equals(user));
    }

    @Transactional(dontRollbackOn = {IssueNotFoundException.class, OperationForbiddenException.class})
    public void changeIssueStatus(String issueId, IssueStatus issueStatus) throws IssueNotFoundException,
            OperationForbiddenException {
        Issue issue = issueRepository.findById(issueId).orElseThrow(() -> new IssueNotFoundException(issueId));
        if (!accessControlFacade.hasPrivilegesInProject(issue.getProject(), Privileges.WRITE_PRIVILEGE)) {
            throw new OperationForbiddenException();
        }
        issue.setIssueStatus(issueStatus);
        issueRepository.save(issue);
    }

    @Transactional(dontRollbackOn = {IssueNotFoundException.class, OperationForbiddenException.class})
    public void addRelatedIssue(String issueId, String relatedIssueId) throws IssueNotFoundException,
            OperationForbiddenException {
        Issue issue = issueRepository.findById(issueId).orElseThrow(() -> new IssueNotFoundException(issueId));
        Issue relatedIssue =
                issueRepository.findById(relatedIssueId).orElseThrow(() -> new IssueNotFoundException(relatedIssueId));
        if (!accessControlFacade.hasPrivilegesInProject(issue.getProject(), Privileges.WRITE_PRIVILEGE)) {
            throw new OperationForbiddenException();
        }
        List<Issue> relatedIssues = issue.getRelatedIssues();
        if(relatedIssues == null){
            relatedIssues = new ArrayList<>();
        }
        if(!relatedIssues.contains(relatedIssue)){
            relatedIssues.add(relatedIssue);
        }
        issue.setRelatedIssues(relatedIssues);
        issueRepository.save(issue);
    }

    public RelatedTasksDTO getRelatedIssues(String issueId) throws IssueNotFoundException,
            OperationForbiddenException {
        Issue issue = issueRepository.findById(issueId).orElseThrow(() -> new IssueNotFoundException(issueId));
        if(!accessControlFacade.hasPrivilegesInProject(issue.getProject(),Privileges.READ_PRIVILEGE)){
            throw new OperationForbiddenException();
        }
        List<IssueDetails> relatedIssuesTo = new ArrayList<>();
        for(Issue relatedIssue : issue.getRelatedIssues()){
            relatedIssuesTo.add(EntityAdapters.createIssueDetails(relatedIssue));
        }
        List<IssueDetails> relatedIssuesFrom = new ArrayList<>();
        List<Issue> issuesRelated = issueRepository.findByRelatedIssuesContains(issue);
        for(Issue issueRelated : issuesRelated){
            relatedIssuesFrom.add(EntityAdapters.createIssueDetails(issueRelated));
        }
        return new RelatedTasksDTO(relatedIssuesTo, relatedIssuesFrom);
    }

    @Transactional(dontRollbackOn = {OperationForbiddenException.class, UserNotFoundException.class})
    public Page<IssueDetails> findWatchedIssues(int page, int pageSize) throws OperationForbiddenException, UserNotFoundException {
        Authentication auth = accessControlFacade.getAuthentication();
        if(auth==null || auth instanceof AnonymousAuthenticationToken){
            throw new OperationForbiddenException();
        }
        User user = userRepository.findUserByEmail(auth.getName())
                .orElseThrow(() -> new UserNotFoundException(auth.getName()));
        Page<Issue> issues = issueRepository
                .findIssuesByWatchersContains(user, PageRequest.of(page, pageSize, Sort.by(Issue_.END_DATE)));

        List<IssueDetails> issueDetails = new ArrayList<>();
        for (Issue issue : issues.getContent()) {
            issueDetails.add(EntityAdapters.createIssueDetails(issue));
        }
        return new PageImpl<>(issueDetails, issues.getPageable(), issues.getTotalElements());
    }

    @Transactional(dontRollbackOn = {IssueNotFoundException.class, OperationForbiddenException.class})
    public void attachFile(@NotNull String issueId, @NotNull MultipartFile file) throws IssueNotFoundException,
            OperationForbiddenException, StorageException {
        Issue issue = issueRepository.findById(issueId).orElseThrow(() -> new IssueNotFoundException(issueId));
        if (!accessControlFacade.hasPrivilegesInProject(issue.getProject(), Privileges.WRITE_PRIVILEGE)) {
            throw new OperationForbiddenException();
        }
        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + file.getOriginalFilename());
            }
            AttachedFile attachedFile = new AttachedFile();
            attachedFile.setFileName(file.getOriginalFilename());
            attachedFile.setIssue(issue);
            attachedFileRepository.save(attachedFile);
            Path issuePath = this.attachedFilePath.resolve(issueId);
            if(Files.notExists(issuePath)){
                Files.createDirectory(issuePath);
            }
            Files.copy(file.getInputStream(), issuePath.resolve(Objects.requireNonNull(file.getOriginalFilename())));
        } catch (IOException e) {
            throw new StorageException("Failed to store file " + file.getOriginalFilename(), e);
        }
    }


    @Transactional
    public Resource loadAttachedFile(@NotNull @NotBlank String issueId,@NotNull @NotBlank String filename) throws
            IssueNotFoundException,
            OperationForbiddenException, StorageFileNotFoundException {
        Issue issue = issueRepository.findById(issueId).orElseThrow(() -> new IssueNotFoundException(issueId));
        if(!accessControlFacade.hasPrivilegesInProject(issue.getProject(), Privileges.READ_PRIVILEGE)){
            throw new OperationForbiddenException();
        }
        try {
            Path file = this.attachedFilePath.resolve(issueId + "/" + filename);
            Resource resource = new UrlResource(file.toUri());
            if(resource.exists() || resource.isReadable()) {
                return resource;
            }
            else {
                throw new StorageFileNotFoundException("Could not read file: " + filename);

            }
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }
}
