package es.udc.fic.gesta.model.service.dto;

import es.udc.fic.gesta.model.service.user.UserProfileDTO;

import java.util.Date;

public class CommentDTO {

    private Long commentID;

    private UserProfileDTO commentedBy;

    private String content;

    private Date commentDate;

    public Long getCommentID() {
        return commentID;
    }

    public void setCommentID(Long commentID) {
        this.commentID = commentID;
    }

    public UserProfileDTO getCommentedBy() {
        return commentedBy;
    }

    public void setCommentedBy(UserProfileDTO commentedBy) {
        this.commentedBy = commentedBy;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }
}
