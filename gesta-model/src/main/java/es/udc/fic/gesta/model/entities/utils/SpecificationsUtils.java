package es.udc.fic.gesta.model.entities.utils;

import org.springframework.data.jpa.domain.Specification;

public final class SpecificationsUtils<T> {

    private SpecificationsUtils(){}

    public static <T> Specification<T> addOrCreateFilterWithOr(Specification<T> currentFilter, Specification<T> newFilter){
        return (currentFilter != null)
                ? currentFilter.or(newFilter)
                : newFilter;
    }

    public static <T> Specification<T> addOrCreateFilterWithAnd(Specification<T> currentFilter, Specification<T> newFilter){
        return (currentFilter != null)
                ? currentFilter.and(newFilter)
                : newFilter;
    }

    public static <T> Specification<T> addOrCreateFilter(Specification<T> currentFilter, Specification<T> newFilter,
                                                         boolean isAnd) {
        if (isAnd) {
            return addOrCreateFilterWithAnd(currentFilter, newFilter);
        }
        return addOrCreateFilterWithOr(currentFilter, newFilter);
    }
}
