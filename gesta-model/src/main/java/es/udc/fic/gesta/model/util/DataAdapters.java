package es.udc.fic.gesta.model.util;

import java.util.Calendar;
import java.util.Date;

public final class DataAdapters {

    private DataAdapters(){};

    public static final Calendar getCalendarFromDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    public static final Calendar clearCalendarFields(Calendar calendar, int... calendarFields){
        if(calendarFields != null){
            for(int field : calendarFields){
                calendar.set(field, 0);
            }
        }
        return calendar;
    }
}
