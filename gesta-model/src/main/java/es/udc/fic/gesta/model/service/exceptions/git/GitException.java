package es.udc.fic.gesta.model.service.exceptions.git;

public abstract class GitException extends Exception {
    private static final long serialVersionUID = 6105891929676751354L;

    public GitException(String msg) {
        super(msg);
    }
}
