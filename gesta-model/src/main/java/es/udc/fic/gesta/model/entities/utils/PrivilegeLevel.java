package es.udc.fic.gesta.model.entities.utils;

public enum PrivilegeLevel {
    PROJECT,
    GLOBAL
}
