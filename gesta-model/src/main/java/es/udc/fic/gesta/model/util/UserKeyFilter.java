package es.udc.fic.gesta.model.util;

import java.util.List;

public class UserKeyFilter {

    private final List<Long> ids;

    private final List<String> emails;

    public UserKeyFilter(List<Long> ids, List<String> emails) {
        this.ids = ids;
        this.emails = emails;
    }

    public List<Long> getIds() {
        return ids;
    }

    public List<String> getEmails() {
        return emails;
    }
}
