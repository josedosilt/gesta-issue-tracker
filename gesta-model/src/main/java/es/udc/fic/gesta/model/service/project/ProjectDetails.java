package es.udc.fic.gesta.model.service.project;

import es.udc.fic.gesta.model.service.issue.IssueDetails;
import es.udc.fic.gesta.model.service.user.UserProfileDTO;
import es.udc.fic.gesta.model.util.ProjectState;

import java.util.List;

public class ProjectDetails {

    private String projectID;

    private String projectName;

    private UserProfileDTO projectLeader;

    private List<UserProfileDTO> assignedTo;

    private ProjectState projectState;

    private boolean userAssignee = false;

    private List<IssueDetails> issueDetails;

    private boolean hasGitRepository;

    public String getProjectID() {
        return projectID;
    }

    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public UserProfileDTO getProjectLeader() {
        return projectLeader;
    }

    public void setProjectLeader(UserProfileDTO projectLeader) {
        this.projectLeader = projectLeader;
    }

    public List<UserProfileDTO> getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(List<UserProfileDTO> assignedTo) {
        this.assignedTo = assignedTo;
    }

    public ProjectState getProjectState() {
        return this.projectState;
    }

    public void setProjectState(ProjectState projectState) {
        this.projectState = projectState;
    }

    public boolean isUserAssignee() {
        return userAssignee;
    }

    public void setUserAssignee(boolean userAssignee) {
        this.userAssignee = userAssignee;
    }

    public List<IssueDetails> getIssueDetails() {
        return issueDetails;
    }

    public void setIssueDetails(List<IssueDetails> issueDetails) {
        this.issueDetails = issueDetails;
    }

    public boolean isHasGitRepository() {
        return hasGitRepository;
    }

    public void setHasGitRepository(boolean hasGitRepository) {
        this.hasGitRepository = hasGitRepository;
    }
}
