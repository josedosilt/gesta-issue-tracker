package es.udc.fic.gesta.model.util;

import es.udc.fic.gesta.model.entities.issue.IssueType;
import es.udc.fic.gesta.model.entities.utils.Priority;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Represents an html form for creating or editing Issues
 */
public class IssueForm {

    @NotNull(message = "{error.nullProjectID}")
    @NotBlank(message = "{error.nullProjectID}")
    private String projectID;
    private Long assignedTo;
    @NotNull(message = "{error.nullSummary}")
    @NotBlank(message = "{error.nullSummary}")
    private String summary;
    private String description;
    private Double estimatedHours;
    @NotNull(message = "{error.nullPriority}")
    private Priority priority;
    @NotNull(message = "{error.nullIssueType}")
    private IssueType issueType;
    private String developmentCycle;

    /**
     * Gets project id.
     *
     * @return the project id
     */
    public String getProjectID() {
        return projectID;
    }

    /**
     * Sets project id.
     *
     * @param projectID the project id
     */
    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    /**
     * Gets assigned to.
     *
     * @return the assigned to
     */
    public Long getAssignedTo() {
        return assignedTo;
    }

    /**
     * Sets assigned to.
     *
     * @param assignedTo the assigned to
     */
    public void setAssignedTo(Long assignedTo) {
        this.assignedTo = assignedTo;
    }

    /**
     * Gets summary.
     *
     * @return the summary
     */
    public String getSummary() {
        return summary;
    }

    /**
     * Sets summary.
     *
     * @param summary the summary
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets estimated hours.
     *
     * @return the estimated hours
     */
    public Double getEstimatedHours() {
        return estimatedHours;
    }

    /**
     * Sets estimated hours.
     *
     * @param estimatedHours the estimated hours
     */
    public void setEstimatedHours(Double estimatedHours) {
        this.estimatedHours = estimatedHours;
    }

    /**
     * Gets priority.
     *
     * @return the priority
     */
    public Priority getPriority() {
        return priority;
    }

    /**
     * Sets priority.
     *
     * @param priority the priority
     */
    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    /**
     * Gets issue type.
     *
     * @return the issue type
     */
    public IssueType getIssueType() {
        return issueType;
    }

    /**
     * Sets issue type.
     *
     * @param issueType the issue type
     */
    public void setIssueType(IssueType issueType) {
        this.issueType = issueType;
    }

    /**
     * Gets development cycle.
     *
     * @return the development cycle
     */
    public String getDevelopmentCycle() {
        return developmentCycle;
    }

    /**
     * Sets development cycle.
     *
     * @param developmentCycle the development cycle
     */
    public void setDevelopmentCycle(String developmentCycle) {
        this.developmentCycle = developmentCycle;
    }
}
