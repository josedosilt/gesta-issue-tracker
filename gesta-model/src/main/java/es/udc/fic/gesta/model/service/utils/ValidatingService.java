package es.udc.fic.gesta.model.service.utils;

import es.udc.fic.gesta.model.util.IssueForm;
import es.udc.fic.gesta.model.util.form.ProjectForm;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@Service
@Validated
public class ValidatingService{

    public void validateProjectForm(@Valid ProjectForm projectForm){
      // do something
    }

    public void validateIssueForm(@Valid IssueForm issueForm){
        // do something
    }

}
