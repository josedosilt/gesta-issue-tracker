package es.udc.fic.gesta.model.service.exceptions;

import java.util.Map;

public class EntityNotFoundException extends Exception{

    private final static String MESSAGE_FORMAT = "Entity of type %s with id %s doesn't exist";

    private final static String COMPOSE_FILTER_MESSAGE = "Entity of type %s not found with filters %s";


    public EntityNotFoundException(Class type, Object id) {
        super(String.format(MESSAGE_FORMAT, type.toString(), id.toString()));
    }

    public EntityNotFoundException(Class type, Map<String,Object> filters){
        super(String.format(COMPOSE_FILTER_MESSAGE, type.toString(), filters.toString()));
    }

    protected EntityNotFoundException(String message){
        super(message);
    }
}
