package es.udc.fic.gesta.model.entities.utils;

import java.util.*;

public enum Privileges {
    READ_PRIVILEGE(Arrays.asList(PrivilegeLevel.GLOBAL,PrivilegeLevel.PROJECT)),
    WRITE_PRIVILEGE(Arrays.asList(PrivilegeLevel.GLOBAL,PrivilegeLevel.PROJECT)),
    CREATE_TASK_PRIVILEGE(Collections.singletonList(PrivilegeLevel.PROJECT)),
    CREATE_CYCLE_PRIVILEGE(Collections.singletonList(PrivilegeLevel.PROJECT));

    private final Collection<PrivilegeLevel> levels;

    Privileges(Collection<PrivilegeLevel> levels){
        this.levels = levels;
    }

    public Collection<PrivilegeLevel> getLevels(){
        return this.levels;
    }


    public static List<Privileges> valuesByLevel(PrivilegeLevel level){
        ArrayList<Privileges> result = new ArrayList<>();
        for(Privileges privilege : values()){
            if(privilege.getLevels().contains(level)){
                result.add(privilege);
            }
        }
        return result;
    }

}
