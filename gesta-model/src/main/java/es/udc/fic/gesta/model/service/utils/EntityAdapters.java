package es.udc.fic.gesta.model.service.utils;

import es.udc.fic.gesta.model.service.project.ProjectDetails;
import es.udc.fic.gesta.model.entities.attached.AttachedFile;
import es.udc.fic.gesta.model.entities.comment.Comment;
import es.udc.fic.gesta.model.entities.cycle.DevelopmentCycle;
import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.timelog.TimeLog;
import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.service.dto.CommentDTO;
import es.udc.fic.gesta.model.service.dto.DevelopmentCycleDTO;
import es.udc.fic.gesta.model.service.dto.TimeLogDTO;
import es.udc.fic.gesta.model.service.issue.IssueDetails;
import es.udc.fic.gesta.model.service.issue.dto.AttachedFileDTO;
import es.udc.fic.gesta.model.service.user.UserProfileDTO;

import java.util.ArrayList;
import java.util.List;

public class EntityAdapters {

    private EntityAdapters(){}

    public static UserProfileDTO createUserProfile(User user, boolean obtainProjects) {
        UserProfileDTO userProfileDTO = new UserProfileDTO(user.getId(), user.getEmail(), user.getFullName());
        List<ProjectDetails> projectDetails = new ArrayList<>();
        if(obtainProjects){
            for(Project project : user.getProjects()){
                projectDetails.add(createProjectDetails(project, false));
            }
        }
        userProfileDTO.setProjects(projectDetails);
        return userProfileDTO;
    }

    public static ProjectDetails createProjectDetails(Project project, boolean obtainAssignees){
        ProjectDetails projectDetails = new ProjectDetails();
        projectDetails.setProjectID(project.getCode());
        projectDetails.setProjectName(project.getName());
        projectDetails.setProjectLeader(createUserProfile(project.getProjectLeader(), false));
        List<UserProfileDTO> users = new ArrayList<UserProfileDTO>();
        for(User user : project.getAssignedTo()){
            users.add(createUserProfile(user, false));
        }
        projectDetails.setAssignedTo(users);
        projectDetails.setProjectState(project.getProjectState());
        projectDetails.setHasGitRepository(project.getGitLabRepo() != null);
        return projectDetails;
    }

    public static IssueDetails createIssueDetails(Issue issue) {
        return createIssueDetails(issue, false);
    }

    public static IssueDetails createIssueDetails(Issue issue,
                                                  boolean recoverAttachedFiles){
        IssueDetails issueDetails = new IssueDetails();
        issueDetails.setIssueID(issue.getIssueID());
        if(issue.getAssignedTo() != null) {
            issueDetails.setAssignedUser(createUserProfile(issue.getAssignedTo(), false));
        }
        issueDetails.setCreator(createUserProfile(issue.getCreatedBy(), false));
        issueDetails.setDescription(issue.getDescription());
        issueDetails.setEndDate(issue.getEndDate());
        issueDetails.setEstimatedHours(issue.getEstimatedHours());
        issueDetails.setIssueType(issue.getType());
        issueDetails.setPriority(issue.getPriority());
        issueDetails.setProjectID(issue.getProject().getCode());
        issueDetails.setStartDate(issue.getStartDate());
        issueDetails.setSummary(issue.getSummary());
        issueDetails.setIssueStatus(issue.getIssueStatus());
        if(issue.getDevelopmentCycle() != null){
            issueDetails.setDevelopmentCycleDTO(createDevelopmentCycleDTO(issue.getDevelopmentCycle(), false));
        }
        issueDetails.setProjectHasRepository(issue.getProject().getGitLabRepo() != null);
        if(recoverAttachedFiles){
            List<AttachedFileDTO> attachedFileDTOS = new ArrayList<>();
            for(AttachedFile file : issue.getAttachedFiles()){
                attachedFileDTOS.add(new AttachedFileDTO(file.getFileId(), file.getFileName()));
            }
            issueDetails.setAttachedFileDTOList(attachedFileDTOS);
        }
        return issueDetails;
    }

    public static CommentDTO createCommentDTO(Comment comment){
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setCommentID(comment.getId());
        commentDTO.setContent(comment.getContent());
        commentDTO.setCommentedBy(createUserProfile(comment.getUser(), false));
        commentDTO.setCommentDate(comment.getCreatedDate());
        return commentDTO;
    }

    public static TimeLogDTO createTimeLogDTO(TimeLog source){
        TimeLogDTO result = new TimeLogDTO();
        result.setComment(source.getComment());
        result.setDate(source.getDate());
        result.setHours(source.getHours().doubleValue());
        result.setIssueId(source.getIssue().getIssueID());
        result.setUserId(source.getUser().getId());
        result.setUserName(source.getUser().getFullName());
        result.setTimeLogId(source.getId());

        return result;
    }

    public static DevelopmentCycleDTO createDevelopmentCycleDTO(DevelopmentCycle source, boolean recoverIssues) {
        DevelopmentCycleDTO result =
                new DevelopmentCycleDTO(source.getId(), source.getName(), source.getProject().getCode());
        if (recoverIssues) {
            List<IssueDetails> issueDetails = new ArrayList<>();
            for (Issue issue : source.getIssueList()) {
                issueDetails.add(EntityAdapters.createIssueDetails(issue));
            }
            result.setIssueList(issueDetails);
        }
        result.setGitBranch(source.getGitBranch());
        if(source.getProject().getGitLabRepo() != null) {
            result.setGitBranchUrl(source.getProject().getGitLabRepo().getURL());
        }
        return result;
    }
}
