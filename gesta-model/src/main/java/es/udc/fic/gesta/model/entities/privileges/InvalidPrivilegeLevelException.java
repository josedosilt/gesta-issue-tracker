package es.udc.fic.gesta.model.entities.privileges;

import es.udc.fic.gesta.model.entities.utils.PrivilegeLevel;
import es.udc.fic.gesta.model.entities.utils.Privileges;

public class InvalidPrivilegeLevelException extends Exception {

    public InvalidPrivilegeLevelException(Privileges privilege, PrivilegeLevel level){
        super(String.format("Invalid Privilege: Privilege %s isn't allowed for the required level %s",
                privilege.name(), level.name()));
    }
}
