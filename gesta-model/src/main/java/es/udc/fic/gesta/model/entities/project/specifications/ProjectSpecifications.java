package es.udc.fic.gesta.model.entities.project.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.Specification;

import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.project.Project_;
import es.udc.fic.gesta.model.entities.user.User_;
import es.udc.fic.gesta.model.entities.utils.SpecificationsUtils;
import es.udc.fic.gesta.model.service.utils.SearchExclusions;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ProjectSpecifications {

    /**
     * Creates a {@link Specification} to check if a string is either like the name
     * or the code of a project.
     *
     * @param param {@link String} to compare with the code and the name. Should not
     *              be {@code null}
     * @return
     */
    public static Specification<Project> nameOrCodeLikeTo(@NotNull String param) {
        return new Specification<Project>() {
            @Override
            public Predicate toPredicate(Root<Project> root, CriteriaQuery<?> criteriaQuery,
                    CriteriaBuilder builder) {

                String paramLike = "%" + param.toLowerCase() + "%";
                Predicate namePredicate = builder.like(builder.lower(root.get(Project_.NAME)), paramLike);
                Predicate codePredicate = builder.like(builder.lower(root.get(Project_.CODE)), paramLike);
                return builder.or(namePredicate, codePredicate);
            }
        };
    }

    /**
     * Creates a {@link Specification} to check if a string is like the name of a
     * project leader
     *
     * @param name full name or part of a full name to search. Can't be {@code null}
     * @return
     */
    public static Specification<Project> projectLeaderNameEqualsTo(@NotNull String name) {
        return new Specification<Project>() {
            @Override
            public Predicate toPredicate(Root<Project> root, CriteriaQuery<?> criteriaQuery,
                    CriteriaBuilder builder) {
                String nameLike = "%" + name.toLowerCase() + "%";
                return builder.like(builder.lower(root.join(Project_.PROJECT_LEADER).get(User_.FULL_NAME)), nameLike);
            }
        };
    }

    /**
     * Apply project exclusions to a project filter.
     *
     * @param currentFilter    the current filter
     * @param searchExclusions the search exclusions
     * @return the specification
     */
    public static Specification<Project> applyProjectExclusions(Specification<Project> currentFilter,
            SearchExclusions searchExclusions) {
        if (searchExclusions == null || searchExclusions.isEmptyExcludedProjects()) {
            return currentFilter;
        }
        Specification<Project> newFilter = (Specification<Project>) (root, criteriaQuery, criteriaBuilder) -> root
                .get(Project_.code).in(searchExclusions.getExcludedProjects()).not();
        return SpecificationsUtils.addOrCreateFilterWithAnd(currentFilter, newFilter);
    }
}
