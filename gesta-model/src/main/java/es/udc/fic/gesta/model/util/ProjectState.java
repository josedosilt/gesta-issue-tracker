package es.udc.fic.gesta.model.util;

public enum ProjectState {
    OPEN, CLOSE;
}
