package es.udc.fic.gesta.model.issue;

import es.udc.fic.gesta.model.entities.attached.AttachedFileRepository;
import es.udc.fic.gesta.model.entities.comment.Comment;
import es.udc.fic.gesta.model.entities.comment.CommentRepository;
import es.udc.fic.gesta.model.entities.cycle.DevelopmentCycleRepository;
import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.issue.IssueRepository;
import es.udc.fic.gesta.model.entities.issue.IssueType;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.project.ProjectRepository;
import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.entities.user.UserRepository;
import es.udc.fic.gesta.model.entities.utils.Priority;
import es.udc.fic.gesta.model.entities.utils.Privileges;
import es.udc.fic.gesta.model.service.auth.AccessControlFacade;
import es.udc.fic.gesta.model.service.exceptions.EmptyCommentException;
import es.udc.fic.gesta.model.service.exceptions.IssueNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.ProjectNotAssignedException;
import es.udc.fic.gesta.model.service.exceptions.UserNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.issue.IssueDetails;
import es.udc.fic.gesta.model.service.issue.IssueService;
import es.udc.fic.gesta.model.service.project.exception.ProjectNotFoundException;
import es.udc.fic.gesta.model.util.IssueForm;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalMatchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.Date;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class IssueServiceTest {

    // constants
    private static final String NON_EXISTENT_EMAIL = "random@random.com";
    private static final String PROJECT_ID = "TEST";
    private static final String ISSUE_ID = PROJECT_ID + "-1";
    private static final String OTHER_ISSUE_ID = PROJECT_ID + "-10";
    private static final String ISSUE_SUMMARY = "ISSUE SUMMARY";
    private static final String CREATOR_EMAIL = "creator@email.com";
    private static final String NON_EXISTENT_PROJECT_ID = "NO-EXIST";
    private static final Long NON_EXISTENT_ID = Long.valueOf(-1);
    private static final String NOT_ASSIGNED_USER_EMAIL = "notAssigned@email.com";
    private static final String NON_EXISTENT_ISSUE_ID = "NO_EXIST-1";
    private static final String COMMENT_CONTENT = "I have a pen, I have pineapple";
    private static final String NOT_EXISTENT_USER_EMAIL = "noExist@email.com";
    private static final String WATCHER_EMAIL = "watcher@email.com";

    private static Issue DEFAULT_ISSUE;

    private static User WATCHER = new User(WATCHER_EMAIL,"pass");

    private User CREATOR_USER = null;
    private User OTHER_USER = null;

    // Configuration
    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {
        @Bean
        public IssueService employeeService() {
            return new IssueService("");
        }

        @Bean
        public AccessControlFacade accessControlFacade(){
            return new AccessControlFacade() {
                @Override
                public Authentication getAuthentication() {
                    return null;
                }

                @Override
                public boolean hasAccessToProject(Project project) {
                    return true;
                }

                @Override
                public boolean isAdmin() {
                    return true;
                }

                @Override
                public boolean hasPrivilegesInProject(Project project, Privileges... privileges) {
                    return true;
                }

                @Override
                public Specification<Project> accessToProjectSpecification(Privileges... privileges) {
                    return new Specification<Project>() {
                        @Override
                        public Predicate toPredicate(Root<Project> root, CriteriaQuery<?> criteriaQuery,
                                                     CriteriaBuilder criteriaBuilder) {
                            return null;
                        }
                    };
                }

                @Override
                public Specification<Issue> hasAccessToIssueSpecification(Privileges... privileges) {
                    return new Specification<Issue>() {
                        @Override
                        public Predicate toPredicate(Root<Issue> root, CriteriaQuery<?> criteriaQuery,
                                                     CriteriaBuilder criteriaBuilder) {
                            return null;
                        }
                    };
                }
            };
        }
    }

    @Autowired
    private IssueService issueService;

    @MockBean
    private IssueRepository issueRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private ProjectRepository projectRepository;

    @MockBean
    private CommentRepository commentRepository;

    @MockBean
    private DevelopmentCycleRepository developmentCycleRepository;

    @MockBean
    private AttachedFileRepository attachedFileRepository;

    private static IssueForm createIssueForm(){
        IssueForm issueForm = new IssueForm();
        issueForm.setProjectID(PROJECT_ID);
        issueForm.setAssignedTo(Long.valueOf(1));
        issueForm.setEstimatedHours(5D);
        issueForm.setIssueType(IssueType.TASK);
        issueForm.setPriority(Priority.LOW);
        issueForm.setSummary(ISSUE_SUMMARY);
        return issueForm;
    }

    private void editIssueSetup(IssueForm issueForm, User editedAssignedTo){
        issueForm.setSummary("Edited Summary");
        issueForm.setDescription("EditedDescription");
        issueForm.setAssignedTo(Long.valueOf(111));
        when(userRepository.findById(Long.valueOf(111))).thenReturn(Optional.of(editedAssignedTo));
        issueForm.setEstimatedHours(Double.valueOf(45));
        for(IssueType issueType : IssueType.values()){
            if(!issueType.equals(DEFAULT_ISSUE.getType())){
                issueForm.setIssueType(issueType);
                break;
            }
        }
        for(Priority priority : Priority.values()){
            if(!priority.equals(DEFAULT_ISSUE.getPriority())){
                issueForm.setPriority(priority);
                break;
            }
        }
    }

    private static void setupIssue(IssueRepository issueRepository, Project project, User user){
        DEFAULT_ISSUE = new Issue();
        DEFAULT_ISSUE.setIssueID(OTHER_ISSUE_ID);
        DEFAULT_ISSUE.setProject(project);
        DEFAULT_ISSUE.setCreatedBy(user);
        DEFAULT_ISSUE.setPriority(Priority.LOW);
        DEFAULT_ISSUE.setEstimatedHours(3D);
        DEFAULT_ISSUE.setSummary("Summary");
        DEFAULT_ISSUE.setType(IssueType.UPGRADE);
        DEFAULT_ISSUE.setAssignedTo(user);
        DEFAULT_ISSUE.setDescription("My description");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        DEFAULT_ISSUE.setStartDate(new Date(calendar.getTimeInMillis()));
        calendar.add(Calendar.DAY_OF_MONTH, 5);
        DEFAULT_ISSUE.setEndDate(new Date(calendar.getTimeInMillis()));
        when(issueRepository.findById(DEFAULT_ISSUE.getIssueID())).thenReturn(Optional.of(DEFAULT_ISSUE));
        when(issueRepository.findById(AdditionalMatchers.not(eq(DEFAULT_ISSUE.getIssueID()))))
                .thenReturn(Optional.empty());
        DEFAULT_ISSUE.setWatchers(new HashSet<>(Collections.singletonList(WATCHER)));
    }

    @Before
    public void setup(){
        WATCHER.setId(3L);
        User user = new User();
        user.setEmail(CREATOR_EMAIL);
        user.setId(2L);
        CREATOR_USER = user;
        User assignedTo = new User();
        assignedTo.setId(1L);
        assignedTo.setEmail("other@email.com");
        assignedTo.setFullName("Other Person");
        OTHER_USER = assignedTo;
        Project project = new Project(PROJECT_ID, PROJECT_ID, user);
        project.setAssignedTo(Collections.singletonList(assignedTo));

        setupIssue(issueRepository, project, user);


        when(projectRepository.findByCode(PROJECT_ID)).thenReturn(project);
        when(issueRepository.countByProject(project)).thenReturn(Long.valueOf(0));
        when(userRepository.findById(1L)).thenReturn(Optional.of(assignedTo));
        when(userRepository.findAllById(any())).thenAnswer(new Answer<List<User>>() {
            @Override
            public List<User> answer(InvocationOnMock invocationOnMock) throws Throwable {
                List<User> result = new ArrayList<>();
                List<Long> ids = new ArrayList<Long>(invocationOnMock.getArgument(0));
                for(Long id : ids){
                    if(CREATOR_USER.getId().equals(id)){
                        result.add(CREATOR_USER);
                    }
                    if(OTHER_USER.getId().equals(id)){
                        result.add(OTHER_USER);
                    }
                    if(WATCHER.getId().equals(id)){
                        result.add(WATCHER);
                    }
                }
                return result;
            }
        });
        when(userRepository.findByEmail(CREATOR_EMAIL)).thenReturn(user);
        when(userRepository.findUserByEmail(CREATOR_EMAIL)).thenReturn(Optional.of(user));
        when(userRepository.findUserByEmail(OTHER_USER.getEmail())).thenReturn(Optional.of(OTHER_USER));
        when(userRepository.findUserByEmail(WATCHER.getEmail())).thenReturn(Optional.of(WATCHER));

        when(issueRepository.save(any())).thenAnswer(new Answer<Issue>() {
            @Override
            public Issue answer(InvocationOnMock invocationOnMock) throws Throwable {
                Object[] args = invocationOnMock.getArguments();
                return (Issue) args[0];
            }
        });
        when(projectRepository.findByCode(AdditionalMatchers.not(eq(PROJECT_ID)))).thenReturn(null);
        when(userRepository.findByEmail(NOT_ASSIGNED_USER_EMAIL)).thenReturn(new User(NOT_ASSIGNED_USER_EMAIL,"pass"));

    }

    @Test
    public void shouldCallSaveIssueWithFields() throws Exception {
        IssueForm issueForm = createIssueForm();

        Issue returnedIssue = issueService.createIssue(issueForm,CREATOR_EMAIL);

        // Mock calls verification
        verify(projectRepository, times(1)).findByCode(PROJECT_ID);
        verify(userRepository, times(1)).findById(issueForm.getAssignedTo());
        verify(userRepository, times(1)).findByEmail(CREATOR_EMAIL);
        verify(issueRepository, times(1)).save(any());

        // Fields check
        assertEquals(issueForm.getProjectID(), returnedIssue.getProject().getCode());
        assertEquals(issueForm.getPriority(), returnedIssue.getPriority());
        assertEquals(issueForm.getIssueType(), returnedIssue.getType());
        assertEquals(issueForm.getAssignedTo(), returnedIssue.getAssignedTo().getId());
        assertEquals(issueForm.getEstimatedHours(), returnedIssue.getEstimatedHours());
        assertEquals(issueForm.getSummary(), returnedIssue.getSummary());
        assertEquals(CREATOR_EMAIL, returnedIssue.getCreatedBy().getEmail());
        assertEquals(ISSUE_ID, returnedIssue.getIssueID());
    }

    @Test(expected = ProjectNotFoundException.class)
    public void shouldThrowProjectNotFoundException() throws Exception {
        IssueForm issueForm = createIssueForm();
        issueForm.setProjectID(NON_EXISTENT_PROJECT_ID);
        issueService.createIssue(issueForm,CREATOR_EMAIL);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void shouldThrowUsernameNotFoundException() throws Exception {
        IssueForm issueForm = createIssueForm();
        issueService.createIssue(issueForm, NON_EXISTENT_EMAIL);
    }

    @Test(expected = UserNotFoundException.class)
    public void shouldThrowUserNotFoundExceptionWithEmail() throws Exception {
        IssueForm issueForm = createIssueForm();
        issueForm.setAssignedTo(NON_EXISTENT_ID);
        issueService.createIssue(issueForm, CREATOR_EMAIL);
    }

    @Test(expected = ProjectNotAssignedException.class)
    public void shouldThrowProjectNotAssignedException() throws Exception {
        issueService.createIssue(createIssueForm(), NOT_ASSIGNED_USER_EMAIL);
    }

    @Test
    public void getIssueDetails_CorrectExecutionTest() throws Exception {

        IssueDetails issueDetails = issueService.getIssueDetails(OTHER_ISSUE_ID);

        assertNotNull(issueDetails);

        // We obtain the Issue from the mock repository to check if fields are correct.
        Issue issue = issueRepository.findById(OTHER_ISSUE_ID).get();

        assertEquals(issue.getIssueID(), issueDetails.getIssueID());
        assertEquals(issue.getProject().getCode(), issueDetails.getProjectID());
        assertEquals(issue.getAssignedTo().getId(), issueDetails.getAssignedUser().getId());
        assertEquals(issue.getAssignedTo().getEmail(), issueDetails.getAssignedUser().getEmail());
        assertEquals(issue.getAssignedTo().getFullName(), issueDetails.getAssignedUser().getFullName());
        assertEquals(issue.getCreatedBy().getId(), issueDetails.getCreator().getId());
        assertEquals(issue.getCreatedBy().getEmail(), issueDetails.getCreator().getEmail());
        assertEquals(issue.getCreatedBy().getFullName(), issueDetails.getCreator().getFullName());
        assertEquals(issue.getEstimatedHours(), issueDetails.getEstimatedHours());
        assertEquals(issue.getSummary(), issueDetails.getSummary());
        assertEquals(issue.getDescription(), issueDetails.getDescription());
        assertEquals(issue.getType(), issueDetails.getIssueType());
        assertEquals(issue.getPriority(),issueDetails.getPriority());

    }

    @Test(expected = IssueNotFoundException.class)
    public void getIssueDetails_IssueNotFound_ThrowIssueNotFoundExceptionTest() throws Exception {
        issueService.getIssueDetails(NON_EXISTENT_ISSUE_ID);
    }

    @Test
    public void editIssue_CorrectExecutionTest() throws Exception {
        IssueForm issueForm = createIssueForm();
        User editedAssignedTo = new User();
        editedAssignedTo.setId(Long.valueOf(111));
        editIssueSetup(issueForm, editedAssignedTo);
        IssueDetails issueDetails = issueService.editIssue(issueForm, DEFAULT_ISSUE.getIssueID());
        assertNotNull(issueDetails);
        assertEquals(issueForm.getSummary(), issueDetails.getSummary());
        assertEquals(issueForm.getDescription(), issueDetails.getDescription());
        assertEquals(editedAssignedTo.getId(), issueDetails.getAssignedUser().getId());
        assertEquals(editedAssignedTo.getFullName(), issueDetails.getAssignedUser().getFullName());
        assertEquals(editedAssignedTo.getEmail(), issueDetails.getAssignedUser().getEmail());
        assertEquals(issueForm.getIssueType(), issueForm.getIssueType());
        assertEquals(issueForm.getPriority(), issueDetails.getPriority());
        assertEquals(issueForm.getProjectID(), issueDetails.getProjectID());
    }

    @Test(expected = IssueNotFoundException.class)
    public void editIssue_IssueNotFound_ThrowIssueNotFoundExceptionTest() throws Exception {
        IssueForm issueForm = createIssueForm();
        User editedAssignedTo = new User();
        editedAssignedTo.setId(Long.valueOf(111));
        editIssueSetup(issueForm, editedAssignedTo);
        editIssueSetup(issueForm,editedAssignedTo);
        issueService.editIssue(issueForm, NON_EXISTENT_ISSUE_ID);

    }

    @Test(expected = UserNotFoundException.class)
    public void editIssue_UserNotFound_ThrowUserNotFoundExceptionTest() throws Exception {
        IssueForm issueForm = createIssueForm();
        User editedAssignedTo = new User();
        editedAssignedTo.setId(Long.valueOf(111));
        editIssueSetup(issueForm, editedAssignedTo);
        issueForm.setAssignedTo(NON_EXISTENT_ID);
        issueService.editIssue(issueForm, DEFAULT_ISSUE.getIssueID());
    }

    @Test
    public void commentIssue_CorrectExecutionTest() throws Exception{
        final Comment[] finalComment = {null};

        when(commentRepository.save(any())).thenAnswer(new Answer<Comment>() {
            @Override
            public Comment answer(InvocationOnMock invocationOnMock) throws Throwable {
                Object[] args = invocationOnMock.getArguments();
                finalComment[0] = (Comment) args[0];
                return (Comment) args[0];
            }
        });

        issueService.commentIssue(DEFAULT_ISSUE.getIssueID(), CREATOR_USER.getEmail(), COMMENT_CONTENT);
        // Verify that save method was called one time
        verify(commentRepository, times(1)).save(any());
        // Check the object that was "saved" in DB
        assertNotNull(finalComment[0]);
        // CREATOR_EMAIL is from CREATOR_USER, so the comment has to be associated with it
        assertEquals(CREATOR_USER, finalComment[0].getUser());
        // We used DEFAULT_ISSUE id, so the comment has to be associated with it
        assertEquals(DEFAULT_ISSUE, finalComment[0].getIssue());
        // The content must be the same that we provided.
        assertEquals(COMMENT_CONTENT, finalComment[0].getContent());
    }

    @Test(expected = UserNotFoundException.class)
    public void commentIssue_UserNotFound_ThrowUserNotFoundExceptionTest() throws Exception {
        issueService.commentIssue(DEFAULT_ISSUE.getIssueID(), NOT_EXISTENT_USER_EMAIL, COMMENT_CONTENT);

    }

    @Test(expected = IssueNotFoundException.class)
    public void commentIssue_IssueNotFound_ThrowIssueNotFoundExceptionTest() throws Exception {
        issueService.commentIssue(NON_EXISTENT_ISSUE_ID, CREATOR_USER.getEmail(), COMMENT_CONTENT);
    }

    @Test(expected = EmptyCommentException.class)
    public void commentIssue_EmptyContent_ThrowEmptyCommentExceptionTest() throws Exception {
        issueService.commentIssue(DEFAULT_ISSUE.getIssueID(), CREATOR_USER.getEmail(), "");
    }

    @Test
    public void addWatchers_CorrectExecutionTest() throws Exception{
        // Test Setup
        List<User> addedWatchers = new ArrayList<>();
        when(issueRepository.save(DEFAULT_ISSUE)).thenAnswer(new Answer<Issue>() {
            @Override
            public Issue answer(InvocationOnMock invocationOnMock) throws Throwable {
                Issue issue = invocationOnMock.getArgument(0);
                addedWatchers.addAll(issue.getWatchers());
                return issue;
            }
        });
        DEFAULT_ISSUE.getWatchers().clear();

        // Execution
        Set<Long> watchersId = new HashSet<>(Arrays.asList(CREATOR_USER.getId(), OTHER_USER.getId()));
        issueService.addWatchers(DEFAULT_ISSUE.getIssueID(), watchersId);

        // Verification
        verify(issueRepository, times(1)).save(any());
        assertEquals(watchersId.size(), addedWatchers.size());
        assertTrue(addedWatchers.containsAll(Arrays.asList(CREATOR_USER, OTHER_USER)));
    }

    @Test(expected = IssueNotFoundException.class)
    public void addWatchers_IssueNotFound_ThrowsIssueNotFoundExceptionTest() throws Exception {
        Set<Long> watchersId = new HashSet<>(Arrays.asList(CREATOR_USER.getId(), OTHER_USER.getId()));
        issueService.addWatchers(NON_EXISTENT_ISSUE_ID, watchersId);
    }

    @Test(expected = UserNotFoundException.class)
    public void addWatchers_AtLeastOneUserNotFound_ThrowsUserNotFoundExceptionTest() throws Exception {
        Set<Long> watchersId = new HashSet<>(Arrays.asList(CREATOR_USER.getId(), NON_EXISTENT_ID));
        issueService.addWatchers(DEFAULT_ISSUE.getIssueID(), watchersId);
    }

    @Test(expected = UserNotFoundException.class)
    public void removeFromWatchers_UserNotFound_ThrowsUserNotFoundExceptionTest() throws Exception{
        issueService.removeFromWatchers(NON_EXISTENT_EMAIL, DEFAULT_ISSUE.getIssueID());
    }

    @Test(expected = IssueNotFoundException.class)
    public void removeFromWatchers_IssueNotFound_ThrowsIssueNotFoundExceptionTest() throws Exception{
        issueService.removeFromWatchers(CREATOR_USER.getEmail(), NON_EXISTENT_ISSUE_ID);
    }

    @Test
    public void removeFromWatchers_CorrectExecutionTest() throws Exception{
        // Preparation.
        // We check if the issue watchers aren't setup. If not, we add WATCHER to the DEFAULT_ISSUE set of watchers
        Set<User> watchers = DEFAULT_ISSUE.getWatchers();
        if(watchers == null){
            DEFAULT_ISSUE.setWatchers(new HashSet<>(Collections.singletonList(WATCHER)));
        }else watchers.add(WATCHER);
        // END - Preparation

        // Execution
        issueService.removeFromWatchers(WATCHER.getEmail(), DEFAULT_ISSUE.getIssueID());

        // Verify
        verify(issueRepository,times(1)).save(DEFAULT_ISSUE);
        assertFalse(DEFAULT_ISSUE.getWatchers().contains(WATCHER));

    }

    @Test(expected = UserNotFoundException.class)
    public void removeWatchers_UserNotFound_ThrowsUserNotFoundExceptionTest() throws Exception{
        issueService.removeWatchers(NON_EXISTENT_EMAIL, DEFAULT_ISSUE.getIssueID(),
                Collections.singletonList(WATCHER.getId()));
    }

    @Test(expected = IssueNotFoundException.class)
    public void removeWatchers_IssueNotFound_ThrowsIssueNotFoundExceptionTest() throws Exception{
        issueService.removeWatchers(CREATOR_USER.getEmail(), NON_EXISTENT_ISSUE_ID,
                Collections.singletonList(WATCHER.getId()));
    }

    @Test(expected = OperationForbiddenException.class)
    public void removeWatchers_UserIsNotCreatorOrAssignee_ThrowsOperationForbiddenExceptionTest() throws Exception{
        issueService.removeWatchers(OTHER_USER.getEmail(), DEFAULT_ISSUE.getIssueID(),
                Collections.singletonList(WATCHER.getId()));
    }

    @Test
    public void removeWatchers_CorrectExecutionTest() throws Exception{
        // PreCheck
        assertTrue(DEFAULT_ISSUE.getWatchers().contains(WATCHER));

        issueService.removeWatchers(CREATOR_USER.getEmail(), DEFAULT_ISSUE.getIssueID(),
                Collections.singletonList(WATCHER.getId()));
        // Verify
        verify(issueRepository, times(1)).save(DEFAULT_ISSUE);
        assertFalse(DEFAULT_ISSUE.getWatchers().contains(WATCHER));
    }
}
