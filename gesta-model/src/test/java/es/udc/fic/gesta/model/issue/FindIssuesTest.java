package es.udc.fic.gesta.model.issue;

import es.udc.fic.gesta.model.entities.attached.AttachedFileRepository;
import es.udc.fic.gesta.model.entities.comment.CommentRepository;
import es.udc.fic.gesta.model.entities.cycle.DevelopmentCycleRepository;
import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.issue.IssueRepository;
import es.udc.fic.gesta.model.entities.issue.IssueType;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.project.ProjectRepository;
import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.entities.user.UserRepository;
import es.udc.fic.gesta.model.entities.utils.Priority;
import es.udc.fic.gesta.model.entities.utils.Privileges;
import es.udc.fic.gesta.model.service.auth.AccessControlFacade;
import es.udc.fic.gesta.model.service.issue.IssueDetails;
import es.udc.fic.gesta.model.service.issue.IssueService;
import es.udc.fic.gesta.model.util.form.SearchIssueForm;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class FindIssuesTest {

    private static final User DEFAULT_ADMIN_USER = new User("admin@email.com", "default", "ROLE_ADMIN");
    private static final User DEFAULT_USER = new User("user@email.com", "default", "ROLE_USER");
    private static final Project DEFAULT_PROJECT_1 = new Project("NAME1", "code1", DEFAULT_USER);
    private static final Project DEFAULT_PROJECT_2 = new Project("NAME2", "code2", DEFAULT_ADMIN_USER);
    private static final Issue DEFAULT_ISSUE_1 = new Issue();
    private static final Issue DEFAULT_ISSUE_2 = new Issue();
    private static final String NON_EXISTENT_USER_EMAIL = "noExist@noemail.com";

    private static User USER_EXECUTE = DEFAULT_ADMIN_USER;


    // Configuration
    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {
        @Bean
        public IssueService employeeService() {
            return new IssueService("");
        }

        @Bean
        public AccessControlFacade accessControlFacade(){
            return new AccessControlFacade() {
                @Override
                public Authentication getAuthentication() {
                    return null;
                }

                @Override
                public boolean hasAccessToProject(Project project) {
                    return true;
                }

                @Override
                public boolean isAdmin() {
                    return USER_EXECUTE.equals(DEFAULT_ADMIN_USER);
                }

                @Override
                public boolean hasPrivilegesInProject(Project project, Privileges... privileges) {
                    return true;
                }

                @Override
                public Specification<Project> accessToProjectSpecification(Privileges... privileges) {
                    return new Specification<Project>() {
                        @Override
                        public Predicate toPredicate(Root<Project> root, CriteriaQuery<?> criteriaQuery,
                                                     CriteriaBuilder criteriaBuilder) {
                            return null;
                        }
                    };
                }

                @Override
                public Specification<Issue> hasAccessToIssueSpecification(Privileges... privileges) {
                    return new Specification<Issue>() {
                        @Override
                        public Predicate toPredicate(Root<Issue> root, CriteriaQuery<?> criteriaQuery,
                                                     CriteriaBuilder criteriaBuilder) {
                            return null;
                        }
                    };
                }
            };
        }
    }

    @Autowired
    private IssueService issueService;

    @MockBean
    private IssueRepository issueRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private ProjectRepository projectRepository;

    @MockBean
    private CommentRepository commentRepository;

    @MockBean
    private DevelopmentCycleRepository developmentCycleRepository;

    @MockBean
    private AttachedFileRepository attachedFileRepository;

    @BeforeClass
    public static void setupClass(){
        DEFAULT_ISSUE_1.setSummary("DEFAULT ISSUE 1");
        DEFAULT_ISSUE_1.setPriority(Priority.LOW);
        DEFAULT_ISSUE_1.setEstimatedHours(1d);
        DEFAULT_ISSUE_1.setType(IssueType.TASK);
        DEFAULT_ISSUE_1.setDescription("DEFAULT ISSUE 1 DESCRIPTION");
        DEFAULT_ISSUE_1.setIssueID(DEFAULT_PROJECT_1.getCode() + "-1");
        DEFAULT_ISSUE_1.setProject(DEFAULT_PROJECT_1);
        DEFAULT_ISSUE_1.setCreatedBy(DEFAULT_USER);

        DEFAULT_ISSUE_2.setSummary("DEFAULT ISSUE 2");
        DEFAULT_ISSUE_2.setPriority(Priority.HIGH);
        DEFAULT_ISSUE_2.setEstimatedHours(10d);
        DEFAULT_ISSUE_2.setType(IssueType.UPGRADE);
        DEFAULT_ISSUE_2.setDescription("DEFAULT ISSUE 2 DESCRIPTION");
        DEFAULT_ISSUE_2.setIssueID(DEFAULT_PROJECT_2.getCode() + "-1");
        DEFAULT_ISSUE_2.setProject(DEFAULT_PROJECT_2);
        DEFAULT_ISSUE_2.setCreatedBy(DEFAULT_ADMIN_USER);
    }

    @Test
    public void searchIssues_AdminCorrectExecutionTest() throws Exception{
        USER_EXECUTE = DEFAULT_ADMIN_USER;
        when(issueRepository.findAll(Mockito.<Pageable>any())).thenAnswer(
                (Answer<Page<Issue>>) invocationOnMock -> {
                    Pageable pageable = invocationOnMock.getArgument(0);
                    return new PageImpl<Issue>(Arrays.asList(DEFAULT_ISSUE_1,DEFAULT_ISSUE_2),pageable,2);
                });
        when(issueRepository.findAll(Mockito.<Specification<Issue>>any(), Mockito.<Pageable>any()))
                .thenAnswer((Answer<Page<Issue>>) invocationOnMock -> {
                    throw new Exception();
                });
        when(userRepository.findByEmail(DEFAULT_ADMIN_USER.getEmail())).thenReturn(DEFAULT_ADMIN_USER);

        SearchIssueForm searchIssueForm = new SearchIssueForm();
        Page<IssueDetails> result = issueService.searchIssues(searchIssueForm, null, 0, 10);
        assertNotNull(result);
        assertNotEquals(result.getTotalElements(), 0);
        assertEquals(result.getContent().size(), 2);
        for(IssueDetails issueDetails : result.getContent()){
            Issue issueToCompare = null;
            if(issueDetails.getIssueID().equals(DEFAULT_ISSUE_1.getIssueID())){
                issueToCompare = DEFAULT_ISSUE_1;
            }else if(issueDetails.getIssueID().equals(DEFAULT_ISSUE_2.getIssueID())){
                issueToCompare = DEFAULT_ISSUE_2;
            }else{
                fail("There are only 2 Issues ID possible, but the method call returned an issue with another ID");
            }
            assertEquals(issueToCompare.getPriority(), issueDetails.getPriority());
            assertEquals(issueToCompare.getType(),issueDetails.getIssueType());
            assertEquals(issueToCompare.getDescription(), issueDetails.getDescription());
            assertEquals(issueToCompare.getSummary(), issueDetails.getSummary());
            assertEquals(issueToCompare.getEstimatedHours(), issueDetails.getEstimatedHours());
            assertEquals(issueToCompare.getCreatedBy().getEmail(), issueDetails.getCreator().getEmail());
        }
    }

    @Test
    public void searchIssues_UserCorrectExecutionTest() throws Exception{
        USER_EXECUTE = DEFAULT_USER;
        when(issueRepository.findAll(Mockito.<Pageable>any())).thenAnswer(
                (Answer<Page<Issue>>) invocationOnMock -> {
                    throw new Exception();
                });
        when(issueRepository.findAll(Mockito.<Specification<Issue>>any(), Mockito.<Pageable>any()))
                .thenAnswer((Answer<Page<Issue>>) invocationOnMock -> {
                    Pageable pageable = invocationOnMock.getArgument(1);
                    return new PageImpl<Issue>(Arrays.asList(DEFAULT_ISSUE_1),pageable,1);
                });
        when(userRepository.findByEmail(DEFAULT_USER.getEmail())).thenReturn(DEFAULT_USER);

        SearchIssueForm searchIssueForm = new SearchIssueForm();
        Page<IssueDetails> result = issueService.searchIssues(searchIssueForm, null, 0, 10);
        assertNotNull(result);
        assertNotEquals(result.getTotalElements(), 0);
        assertEquals(result.getContent().size(), 1);
        for(IssueDetails issueDetails : result.getContent()){
            Issue issueToCompare = null;
            if(issueDetails.getIssueID().equals(DEFAULT_ISSUE_1.getIssueID())){
                issueToCompare = DEFAULT_ISSUE_1;
            }else{
                fail("There are only 2 Issues ID possible, but the method call returned an issue with another ID");
            }
            assertEquals(issueToCompare.getPriority(), issueDetails.getPriority());
            assertEquals(issueToCompare.getType(),issueDetails.getIssueType());
            assertEquals(issueToCompare.getDescription(), issueDetails.getDescription());
            assertEquals(issueToCompare.getSummary(), issueDetails.getSummary());
            assertEquals(issueToCompare.getEstimatedHours(), issueDetails.getEstimatedHours());
            assertEquals(issueToCompare.getCreatedBy().getEmail(), issueDetails.getCreator().getEmail());
        }
    }
}
