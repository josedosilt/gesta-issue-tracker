package es.udc.fic.gesta.model.timelog;

import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.issue.IssueRepository;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.project.ProjectRepository;
import es.udc.fic.gesta.model.entities.timelog.TimeLog;
import es.udc.fic.gesta.model.entities.timelog.TimeLogRepository;
import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.entities.user.UserRepository;
import es.udc.fic.gesta.model.service.exceptions.IssueNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.UserNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.InvalidParametersException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.timelog.TimeLogService;
import es.udc.fic.gesta.model.util.UserKeyFilter;
import es.udc.fic.gesta.model.util.form.TimeLogForm;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Optional;

import static org.mockito.AdditionalMatchers.not;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class TimeLogServiceTest {

    // constants
    private static final String NON_EXISTENT_EMAIL = "random@random.com";
    private static final String PROJECT_ID = "TEST";
    private static final String ISSUE_ID = PROJECT_ID + "-1";
    private static final String OTHER_ISSUE_ID = PROJECT_ID + "-10";
    private static final String ISSUE_SUMMARY = "ISSUE SUMMARY";
    private static final String USER_EMAIL = "creator@email.com";
    private static final String OTHER_EMAIL = "other@emai.com";
    private static final String NON_EXISTENT_PROJECT_ID = "NO-EXIST";
    private static final Long NON_EXISTENT_ID = Long.valueOf(-1);
    private static final String NOT_ASSIGNED_USER_EMAIL = "notAssigned@email.com";
    private static final String NON_EXISTENT_ISSUE_ID = "NO_EXIST-1";
    private static final String COMMENT_CONTENT = "I have a pen, I have pineapple";
    private static final String NOT_EXISTENT_USER_EMAIL = "noExist@email.com";

    private static Issue DEFAULT_ISSUE = new Issue();

    private static User DEFAULT_USER = new User(USER_EMAIL, "PASS", "ROLE_ADMIN");
    private static User OTHER_USER = new User(OTHER_EMAIL, "PASS", "ROLE_USER");
    private static Project DEFAULT_PROJECT = new Project(PROJECT_ID, PROJECT_ID, DEFAULT_USER);

    // Configuration
    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {
        @Bean
        public TimeLogService timeLogService () {
            return new TimeLogService();
        }
    }

    @Autowired
    private TimeLogService timeLogService;
    @MockBean
    private IssueRepository issueRepository;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private TimeLogRepository timeLogRepository;
    @MockBean
    private ProjectRepository projectRepository;

    @BeforeClass
    public static void classSetup(){
        DEFAULT_ISSUE.setIssueID(ISSUE_ID);
        DEFAULT_ISSUE.setProject(DEFAULT_PROJECT);
        DEFAULT_PROJECT.setAssignedTo(Arrays.asList(OTHER_USER));
    }

    @Before
    public void testSetup(){
        when(userRepository.findByEmail(DEFAULT_USER.getEmail())).thenReturn(DEFAULT_USER);
        when(userRepository.findByEmail(OTHER_USER.getEmail())).thenReturn(OTHER_USER);
        when(userRepository.findByEmail(not(or(eq(DEFAULT_USER.getEmail()), eq(OTHER_USER.getEmail())))))
                .thenReturn(null);

        when(issueRepository.findById(DEFAULT_ISSUE.getIssueID())).thenReturn(Optional.of(DEFAULT_ISSUE));
        when(issueRepository.findById(not(eq(DEFAULT_ISSUE.getIssueID())))).thenReturn(Optional.empty());

        when(projectRepository.findProjectByCode(DEFAULT_PROJECT.getCode())).thenReturn(Optional.of(DEFAULT_PROJECT));
        when(projectRepository.findProjectByCode(not(eq(DEFAULT_PROJECT.getCode())))).thenReturn(Optional.empty());

        when(projectRepository.findByIssueId(DEFAULT_ISSUE.getIssueID())).thenReturn(Optional.of(DEFAULT_PROJECT));
        when(projectRepository.findByIssueId(not(eq(DEFAULT_ISSUE.getIssueID())))).thenReturn(Optional.empty());
    }

    private TimeLogForm createTimeLogForm(){
        TimeLogForm timeLogForm = new TimeLogForm();
        timeLogForm.setIssueId(ISSUE_ID);
        timeLogForm.setDate(Calendar.getInstance().getTime());
        timeLogForm.setComment("I worked a lot on this!");
        timeLogForm.setHours(8D);
        return timeLogForm;
    }

    @Test
    public void logTime_CorrectExecutionTest() throws Exception{
        // Test Setup
        TimeLogForm timeLogForm = createTimeLogForm();
        final TimeLog[] result = {null};
        when(timeLogRepository.save(any())).then(new Answer<TimeLog>() {
            @Override
            public TimeLog answer(InvocationOnMock invocationOnMock) throws Throwable {
                result[0] = (TimeLog) invocationOnMock.getArgument(0);
                return result[0];
            }
        });


        // Execution
        timeLogService.logTime(DEFAULT_USER.getEmail(), timeLogForm);


        // Validation
        verify(timeLogRepository, times(1)).save(any());
        Assert.assertEquals(DEFAULT_USER, result[0].getUser());
        Assert.assertEquals(DEFAULT_ISSUE, result[0].getIssue());
        Assert.assertEquals(timeLogForm.getComment(), result[0].getComment());
        Assert.assertEquals(timeLogForm.getHours().doubleValue(), result[0].getHours().doubleValue(), 0.01);
        Assert.assertEquals(0, timeLogForm.getDate().compareTo(result[0].getDate()));

    }

    @Test(expected = UserNotFoundException.class)
    public void logTime_UserNotFound_ThrowUserNotFoundExceptionTest() throws Exception{
        // Test Setup
        TimeLogForm timeLogForm = createTimeLogForm();

        // Execution
        timeLogService.logTime(NON_EXISTENT_EMAIL, timeLogForm);
    }

    @Test(expected = IssueNotFoundException.class)
    public void logTime_IssueNotFound_ThrowIssueNotFoundExceptionTest() throws Exception{
        // Test Setup
        TimeLogForm timeLogForm = createTimeLogForm();
        timeLogForm.setIssueId(NON_EXISTENT_ISSUE_ID);

        // Execution
        timeLogService.logTime(DEFAULT_USER.getEmail(), timeLogForm);
    }

    @Test(expected = OperationForbiddenException.class)
    public void getProjectTimeLogs_UserIsNotProjectLeader_ThrowsOperationForbiddenExceptionTest() throws Exception {
        timeLogService
                .getProjectTimeLogs(OTHER_EMAIL, DEFAULT_PROJECT.getCode(), new UserKeyFilter(null, null));
    }

    @Test(expected = InvalidParametersException.class)
    public void getProjectTimeLogs_NoProjectIdProvided_ThrowsOperationForbiddenExceptionTest() throws Exception {
        timeLogService
                .getProjectTimeLogs(DEFAULT_USER.getEmail(), null, new UserKeyFilter(null, null));
    }

    @Test(expected = UserNotFoundException.class)
    public void getProjectTimeLogs_UserNotFound_ThrowsUserNotFoundExceptionTest() throws Exception {
        timeLogService
                .getProjectTimeLogs(NON_EXISTENT_EMAIL, DEFAULT_PROJECT.getCode(), new UserKeyFilter(null, null));
    }

    @Test(expected = OperationForbiddenException.class)
    public void getIssueTimeLogs_UserIsNotProjectLeader_ThrowsOperationForbiddenExceptionTest() throws Exception {
        timeLogService
                .getIssueTimeLogs(OTHER_EMAIL, DEFAULT_ISSUE.getIssueID(), new UserKeyFilter(null, null));
    }

    @Test(expected = InvalidParametersException.class)
    public void getIssueTimeLogs_NoProjectIdProvided_ThrowsOperationForbiddenExceptionTest() throws Exception {
        timeLogService
                .getProjectTimeLogs(DEFAULT_USER.getEmail(), null, new UserKeyFilter(null, null));
    }

    @Test(expected = UserNotFoundException.class)
    public void getIssueTimeLogs_UserNotFound_ThrowsUserNotFoundExceptionTest() throws Exception {
        timeLogService
                .getProjectTimeLogs(NON_EXISTENT_EMAIL, DEFAULT_ISSUE.getIssueID(), new UserKeyFilter(null, null));
    }

    @Test(expected = UserNotFoundException.class)
    public void getUserTimeLogs_UserNotFound_ThrowsUserNotFoundExceptionTest() throws Exception {
        timeLogService.getUserTimeLogs(NON_EXISTENT_EMAIL, null, null);
    }

    @Test
    public void getUserTimeLogs_CorrectExecutionTest() throws Exception {
        timeLogService.getUserTimeLogs(DEFAULT_USER.getEmail(), null, null);
        verify(timeLogRepository, times(1)).findAll(Mockito.<Specification<TimeLog>>any());
    }

    @Test
    public void getProjectOrIssueTimeLogs_CorrectExecutionTest() throws Exception {
        timeLogService.getProjectOrIssueTimeLogs(DEFAULT_USER.getEmail(), DEFAULT_ISSUE.getIssueID(),
                DEFAULT_PROJECT.getCode(), new UserKeyFilter(null, null));
        verify(timeLogRepository, times(1)).findAll(Mockito.<Specification<TimeLog>>any());
    }

}
