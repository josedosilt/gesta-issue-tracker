package es.udc.fic.gesta.model.project;

import es.udc.fic.gesta.model.entities.cycle.DevelopmentCycle;
import es.udc.fic.gesta.model.entities.cycle.DevelopmentCycleRepository;
import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.issue.IssueRepository;
import es.udc.fic.gesta.model.entities.privileges.project.ProjectPrivilegesRepository;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.project.ProjectRepository;
import es.udc.fic.gesta.model.entities.repository.GitLabRepoRepository;
import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.entities.user.UserRepository;
import es.udc.fic.gesta.model.entities.utils.Privileges;
import es.udc.fic.gesta.model.service.auth.AccessControlFacade;
import es.udc.fic.gesta.model.service.dto.DevelopmentCycleDTO;
import es.udc.fic.gesta.model.service.exceptions.ProjectNotAssignedException;
import es.udc.fic.gesta.model.service.exceptions.UserNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.cycle.EmptyOrNullNameException;
import es.udc.fic.gesta.model.service.exceptions.cycle.NameAlreadyExistsForProjectException;
import es.udc.fic.gesta.model.service.project.ProjectService;
import es.udc.fic.gesta.model.service.project.exception.ProjectNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class ProjectServiceTest {

    // Constants
    private static final String DEFAULT_USER_EMAIL = "user@email.com";
    private static final String OTHER_USER_EMAIL = "other@email.com";
    private static final String NON_EXISTENT_EMAIL ="null@null.null";
    private static final String DEFAULT_PROJECT_ID = "TEST";
    private static final String NON_EXISTENT_PROJECT_ID = "NULL";
    private static final User DEFAULT_USER = new User(DEFAULT_USER_EMAIL, "pass");
    private static final User OTHER_USER = new User(OTHER_USER_EMAIL, "pass");
    private static final Long DEFAULT_ID = 1L;
    private static final String DEFAULT_CYCLE_NAME = "CYCLE";
    private static final String ALREADY_EXIST_NAME = "CYCLE THAT EXISTS";

    private static final Project DEFAULT_PROJECT = new Project(DEFAULT_PROJECT_ID, DEFAULT_PROJECT_ID, DEFAULT_USER);



    // Configuration
    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {
        @Bean
        public ProjectService projectService() {
            return new ProjectService();
        }

        @Bean
        public AccessControlFacade accessControlFacade(){
            return new AccessControlFacade() {
                @Override
                public Authentication getAuthentication() {
                    return null;
                }

                @Override
                public boolean hasAccessToProject(Project project) {
                    return true;
                }

                @Override
                public boolean isAdmin() {
                    return true;
                }

                @Override
                public boolean hasPrivilegesInProject(Project project, Privileges... privileges) {
                    return true;
                }

                @Override
                public Specification<Project> accessToProjectSpecification(Privileges... privileges) {
                    return new Specification<Project>() {
                        @Override
                        public Predicate toPredicate(Root<Project> root, CriteriaQuery<?> criteriaQuery,
                                                     CriteriaBuilder criteriaBuilder) {
                            return null;
                        }
                    };
                }

                @Override
                public Specification<Issue> hasAccessToIssueSpecification(Privileges... privileges) {
                    return new Specification<Issue>() {
                        @Override
                        public Predicate toPredicate(Root<Issue> root, CriteriaQuery<?> criteriaQuery,
                                                     CriteriaBuilder criteriaBuilder) {
                            return null;
                        }
                    };
                }
            };
        }
    }

    @Autowired
    private ProjectService projectService;

    @MockBean
    UserRepository userRepository;
    @MockBean
    ProjectRepository projectRepository;
    @MockBean
    private DevelopmentCycleRepository developmentCycleRepository;
    @MockBean
    private ProjectPrivilegesRepository projectPrivilegesRepository;
    @MockBean
    private IssueRepository issueRepository;
    @MockBean
    private GitLabRepoRepository gitLabRepoRepository;

    @Before
    public void setupTest(){

        when(userRepository.findUserByEmail(DEFAULT_USER.getEmail())).thenReturn(Optional.of(DEFAULT_USER));
        when(userRepository.findUserByEmail(OTHER_USER.getEmail())).thenReturn(Optional.of(OTHER_USER));
        when(projectRepository.findProjectByCode(DEFAULT_PROJECT.getCode())).thenReturn(Optional.of(DEFAULT_PROJECT));
        when(projectRepository.hasProjectAssigned(DEFAULT_PROJECT, DEFAULT_USER)).thenReturn(true);
        when(projectRepository.hasProjectAssigned(eq(DEFAULT_PROJECT), not(eq(DEFAULT_USER)))).thenReturn(false);
        when(developmentCycleRepository.save(any()))
                .thenAnswer(
                        (Answer<DevelopmentCycle>) invocationOnMock -> {
                            DevelopmentCycle developmentCycle = invocationOnMock.getArgument(0);
                            developmentCycle.setId(DEFAULT_ID);
                            return developmentCycle;
                        });
        when(developmentCycleRepository.existsByNameAndProject(ALREADY_EXIST_NAME, DEFAULT_PROJECT)).thenReturn(true);
        when(developmentCycleRepository.existsByNameAndProject(not(eq(ALREADY_EXIST_NAME)), eq(DEFAULT_PROJECT)))
                .thenReturn(false);
    }

    @Test(expected = UserNotFoundException.class)
    public void createDevelopmentCycle_UserNotFound_ThrowsUserNotFoundExceptionTest() throws Exception{
        projectService.createDevelopmentCycle(NON_EXISTENT_EMAIL, DEFAULT_PROJECT.getCode(), DEFAULT_CYCLE_NAME);
    }
    @Test(expected = ProjectNotFoundException.class)
    public void createDevelopmentCycle_ProjectNotFound_ThrowsProjectNotFoundExceptionTest() throws Exception{
        projectService.createDevelopmentCycle(DEFAULT_USER.getEmail(), NON_EXISTENT_PROJECT_ID, DEFAULT_CYCLE_NAME);
    }

    @Test(expected = EmptyOrNullNameException.class)
    public void createDevelopmentCycle_NullName_ThrowsEmptyOrNullNameExceptionTest() throws Exception{
        projectService.createDevelopmentCycle(DEFAULT_USER.getEmail(), NON_EXISTENT_PROJECT_ID, null);
    }

    @Test(expected = EmptyOrNullNameException.class)
    public void createDevelopmentCycle_EmptyName_ThrowsEmptyOrNullNameExceptionTest() throws Exception{
        projectService.createDevelopmentCycle(DEFAULT_USER.getEmail(), NON_EXISTENT_PROJECT_ID, "");
    }

    @Test(expected = ProjectNotAssignedException.class)
    public void createDevelopmentCycle_ProjectNotAssigned_ThrowsProjectNotAssignedExceptionTest() throws Exception{
        projectService.createDevelopmentCycle(OTHER_USER.getEmail(), DEFAULT_PROJECT_ID, DEFAULT_CYCLE_NAME);
    }

    @Test(expected = NameAlreadyExistsForProjectException.class)
    public void createDevelopmentCycle_CycleNameAlreadyExists_ThrowsNameAlreadyExistsForProjectExceptionTest() throws Exception{
        projectService.createDevelopmentCycle(DEFAULT_USER.getEmail(), DEFAULT_PROJECT_ID, ALREADY_EXIST_NAME);
    }

    @Test
    public void createDevelopmentCycle_CorrectExecutionTest() throws Exception {
        DevelopmentCycleDTO result =
                projectService.createDevelopmentCycle(DEFAULT_USER.getEmail(), DEFAULT_PROJECT_ID, DEFAULT_CYCLE_NAME);
        verify(developmentCycleRepository, times(1)).save(any());
        assertEquals(DEFAULT_ID, result.getId());
        assertEquals(DEFAULT_CYCLE_NAME, result.getName());
        assertEquals(DEFAULT_PROJECT_ID, result.getProjectId());
    }


}
