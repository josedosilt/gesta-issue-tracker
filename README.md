# GesTa - Issue Tracker
## Abstract
The objetive of this project is the design and development of a Project Managment web
application using MVC architecture and Java technologies.

 The application will allow the management of diferent elements of a project: the tasks that compose it and the human resource that will perform them. To allow this, users will be able to create, modify and remove projects,
tasks and users, as well as assign tasks to users. 

It will also be posible to search projects. Users will be able to link tasks and create development cycles to group tasks, which will help with task organization and management, as well as add estimates and log time, which
can help with project planification and tracking. 

Finally, the application will offer the option to connect to a GitLab account to assign a repository to a project, assign a git branch to a development cycle or search the commits linked to a task.

## Motivation
Issue Trackers and Project/ Team Management tools are used frecuently in software developments for planifications and tracking the actual status of a project. As an Software Engineering student, I learnt about them, their types and ussages in software projects and methodologies.


Making an Issue Tracker requires to explore the diferents alternatives already available out there, like Redmine, and anaylize the needs of developers and project managers, and that's why I decided to design and develop it to obtain more knowledge about project managment.

This is a final degree project in Computer Science (Software Engineering) at the University of A Coruña:
[Aplicación Web Java para la Gestión de Proyectos](https://www.udc.es/es/tfe/traballo/?codigo=17242)

