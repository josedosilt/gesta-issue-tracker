package es.udc.fic.gesta.app.integration.util;

import es.udc.fic.gesta.model.entities.cycle.DevelopmentCycle;
import es.udc.fic.gesta.model.entities.cycle.DevelopmentCycleRepository;
import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.issue.IssueRepository;
import es.udc.fic.gesta.model.entities.issue.IssueType;
import es.udc.fic.gesta.model.entities.privileges.project.ProjectPrivileges;
import es.udc.fic.gesta.model.entities.privileges.project.ProjectPrivilegesRepository;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.project.ProjectRepository;
import es.udc.fic.gesta.model.entities.timelog.TimeLog;
import es.udc.fic.gesta.model.entities.timelog.TimeLogRepository;
import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.entities.user.UserRepository;
import es.udc.fic.gesta.model.entities.utils.Priority;
import es.udc.fic.gesta.model.entities.utils.PrivilegeLevel;
import es.udc.fic.gesta.model.entities.utils.Privileges;
import es.udc.fic.gesta.model.util.IssueStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestComponent;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@TestComponent
public class EntityTestService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    ProjectPrivilegesRepository projectPrivilegesRepository;
    @Autowired
    IssueRepository issueRepository;
    @Autowired
    DevelopmentCycleRepository developmentCycleRepository;
    @Autowired
    TimeLogRepository timeLogRepository;

    public void createProjects(String projectId, Long projectLeaderId) {
        User projectLeader = userRepository.findById(projectLeaderId).get();
        Project project = projectRepository.save(new Project(projectId, projectId, projectLeader));
        // Set defaults privileges
        ProjectPrivileges projectPrivileges = new ProjectPrivileges();
        projectPrivileges.setProject(project);
        projectPrivileges.setUser(projectLeader);
        projectPrivileges.setPrivileges(new ArrayList<>(Privileges.valuesByLevel(PrivilegeLevel.PROJECT)));
        projectPrivilegesRepository.save(projectPrivileges);
    }

    public Project getProject(String projectId){
        return projectRepository.findByCode(projectId);
    }

    public Issue createIssues(String projectId, String creatorEmail)  {
        Project project = projectRepository.findByCode(projectId);
        long issueNumber = issueRepository.countByProject(project) + 1;
        Issue issue = new Issue();
        issue.setIssueID(projectId + "-" + issueNumber);
        issue.setProject(project);
        User creator = userRepository.findByEmail(creatorEmail);
        issue.setCreatedBy(creator);
        issue.setAssignedTo(creator);
        issue.setEstimatedHours(2d);
        issue.setPriority(Priority.LOW);
        issue.setSummary("TASK-" + issueNumber);
        issue.setType(IssueType.TASK);
        issue.setIssueStatus(IssueStatus.OPEN);
        return issueRepository.save(issue);
    }

    public User getUser(String email){
        Optional<User> user = userRepository.findUserByEmail(email);
        return user.orElse(null);
    }

    public boolean existsDevelopmentCycle(String projectCode, String cycleName){
        Optional<Project> project = projectRepository.findProjectByCode(projectCode);
        return project.isPresent()
                && developmentCycleRepository.existsByNameAndProject(cycleName, project.get());
    }

    public void createDevelopmentCycle(String projectCode, String cycleName){
        Project project = projectRepository.findByCode(projectCode);
        developmentCycleRepository.save(new DevelopmentCycle(project, cycleName));
    }

    public TimeLog createTimeLog(String issueId, String userEmail, BigDecimal hours, String comment, Date date) {
        Issue issue = issueRepository.getOne(issueId);
        User user = userRepository.findByEmail(userEmail);
        TimeLog timeLog = new TimeLog();
        timeLog.setComment(comment);
        timeLog.setHours(hours);
        timeLog.setIssue(issue);
        timeLog.setUser(user);
        timeLog.setDate(date);
        return timeLogRepository.save(timeLog);
    }

    public List<TimeLog> getTimeLogs(){
        return timeLogRepository.findAll();
    }
}
