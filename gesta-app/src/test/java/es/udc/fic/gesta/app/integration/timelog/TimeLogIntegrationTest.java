package es.udc.fic.gesta.app.integration.timelog;


import es.udc.fic.gesta.app.integration.BaseIntegrationTest;
import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.entities.timelog.TimeLog;
import es.udc.fic.gesta.model.entities.user.User;
import org.junit.Test;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.junit.Assert.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TimeLogIntegrationTest extends BaseIntegrationTest {

    @Test
    @WithUserDetails
    public void logTimePage_ShouldReturnPage_Status200() throws Exception{
        User user = this.entityTestService.getUser("user");
        this.entityTestService.createProjects("ACOR", user.getId());
        Issue issue = this.entityTestService.createIssues("ACOR", "user");
        String issueId = issue.getIssueID();
        String url = "/issue/" + issue.getIssueID() + "/log-time";
        this.mvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content()
                        .string(containsString(String.format("<title>Log Time in Issue %s</title>", issueId))))
                .andExpect(content()
                        .string(stringContainsInOrder(
                                Arrays.asList("<form", "method=\"post\"", "action=", url, ">", "</form>"))))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<input", "name=\"date\"", "type=\"date\"", "/>"))))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<input", "name=\"hours\"", "type=\"number\"", "/>"))))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<input", "name=\"comment\"", "type=\"text\"", "/>"))));
    }

    @Test
    @WithUserDetails
    public void logTime_ShouldCreateTimeLog() throws Exception{
        User user = this.entityTestService.getUser("user");
        this.entityTestService.createProjects("ACOR", user.getId());
        Issue issue = this.entityTestService.createIssues("ACOR", "user");
        String issueId = issue.getIssueID();
        String url = "/issue/" + issue.getIssueID() + "/log-time";
        List<TimeLog> timeLogs = this.entityTestService.getTimeLogs();
        assertTrue("Test initialization error: Database should not contain any data",
                timeLogs == null || timeLogs.isEmpty());
        BigDecimal hours = new BigDecimal(8);
        String comment = "comment";
        String date = "2019-01-01";
        MockHttpServletRequestBuilder postMethod = post(url)
                .with(csrf())
                .param("issueId", issueId)
                .param("date", date)
                .param("hours", hours.toString())
                .param("comment", comment);
        this.mvc.perform(postMethod);

        timeLogs = this.entityTestService.getTimeLogs();
        assertFalse(timeLogs.isEmpty());
        assertEquals(1,timeLogs.size());
        TimeLog timeLog = timeLogs.get(0);
        assertEquals(issueId, timeLog.getIssue().getIssueID());
        assertEquals(hours.intValue(), timeLog.getHours().intValue());
        assertEquals(comment, timeLog.getComment());
        Date timeLogDate = timeLog.getDate();
        assertEquals(date, new SimpleDateFormat("yyyy-MM-dd").format(timeLogDate));
    }

    @Test
    @WithUserDetails
    public void viewTimeLogs_ShouldReturnTimeLogsView() throws Exception {
        this.entityTestService.createProjects("ACOR", this.adminUser.getId());
        Issue issue = this.entityTestService.createIssues("ACOR", this.adminUser.getEmail());
        BigDecimal hours = new BigDecimal(8);
        String comment = "comment";
        Date date = new Date();
        this.entityTestService.createTimeLog(issue.getIssueID(), "user", hours, comment, date);
        MockHttpServletRequestBuilder getMethod = get("/time-logs").param("issueId", issue.getIssueID());

        this.mvc.perform(getMethod).andExpect(status().isOk())
                .andExpect(content()
                        .string(containsString("<title>Time Logs</title>")))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList(issue.getIssueID(), hours.toString(), comment))));
    }
}
