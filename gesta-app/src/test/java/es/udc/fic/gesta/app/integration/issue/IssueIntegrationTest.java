package es.udc.fic.gesta.app.integration.issue;

import es.udc.fic.gesta.app.integration.BaseIntegrationTest;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class IssueIntegrationTest extends BaseIntegrationTest {


    @WithUserDetails("admin")
    @Test
    public void searchIssuesPage_shouldReturnPageWithoutIssues_Status200() throws Exception {
        mvc.perform(get("/issue/search").contentType(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("No Issues Found")));
    }

    @WithMockUser("user")
    @Test
    public void searchIssuesPage_shouldReturnEmptyPageCauseNotAssigned_Status200() throws Exception {
        entityTestService.createProjects("ACOR", adminUser.getId());
        entityTestService.createIssues("ACOR", adminUser.getEmail());
        mvc.perform(get("/issue/search").contentType(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("No Issues Found")));
    }

    @WithUserDetails("admin")
    @Test
    public void searchIssuesPage_shouldReturnPageWithIssues_Status200() throws Exception {
        entityTestService.createProjects("ACOR", adminUser.getId());
        entityTestService.createIssues("ACOR", adminUser.getEmail());
        entityTestService.createIssues("ACOR", adminUser.getEmail());
        mvc.perform(get("/issue/search").contentType(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("ACOR-1")))
                .andExpect(content().string(containsString("ACOR-2")));
    }

    @WithUserDetails("admin")
    @Test
    public void searchProjectIssuesPage_shouldReturnPageWithProjectIssues_Status200() throws Exception {
        entityTestService.createProjects("ACOR", adminUser.getId());
        entityTestService.createProjects("OTHER", adminUser.getId());
        entityTestService.createIssues("ACOR", adminUser.getEmail());
        entityTestService.createIssues("OTHER", adminUser.getEmail());
        mvc.perform(get("/project/ACOR/issues").contentType(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("ACOR-1")))
                .andExpect(content().string(Matchers.not(containsString("OTHER"))));
    }

    @WithUserDetails("admin")
    @Test
    public void selectIssuePage_shouldReturnPageWithSelectableIssues_Status200() throws Exception {
        entityTestService.createProjects("ACOR", adminUser.getId());
        entityTestService.createIssues("ACOR", adminUser.getEmail());
        mvc.perform(get("/issue/select").contentType(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("ACOR-1")))
                .andExpect(content().string(containsString("Select")));
    }

}
