package es.udc.fic.gesta.app.integration.project;

import es.udc.fic.gesta.app.integration.BaseIntegrationTest;
import es.udc.fic.gesta.model.entities.project.Project;
import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.service.exceptions.EntityNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.util.ProjectState;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.util.NestedServletException;

import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


public class ProjectIntegrationTest extends BaseIntegrationTest {

    @WithMockUser
    @Test
    public void createProjectPage_ShouldReturnErrorUserIsNotAdmin() throws Exception {
        this.mvc.perform(get("/project/create").contentType(MediaType.TEXT_HTML))
                .andExpect(status().is(403));
    }

    @WithUserDetails("admin")
    @Test
    public void createProjectPage_shouldReturnThePageWithoutError() throws Exception {
        this.mvc.perform(get("/project/create").contentType(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<form id=\"projectForm\"")))
                .andExpect(content().string(containsString("id=\"projectName\"")))
                .andExpect(content().string(containsString("id=\"projectID\"")))
                .andExpect(content().string(containsString("id=\"projectLeader\"")));
    }

    @WithMockUser
    @Test
    public void createProjectPost_ShouldReturnErrorUserIsNotAdmin() throws Exception {
        this.mvc.perform(post("/project/create?projectName=ACOR&projectID=ACOR&projectLeader=admin").with(csrf()))
                .andExpect(status().is(403));
    }

    @WithUserDetails("admin")
    @Test
    public void createProject_shouldRedirectToProjectDetails() throws Exception {
        String projectCode = "ACOR";
        MockHttpServletRequestBuilder postMethod = post("/project/create")
                .with(csrf())
                .param("projectName", projectCode)
                .param("projectID", projectCode)
                .param("projectLeader", adminUser.getEmail());
        this.mvc.perform(postMethod)
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/project/details/" + projectCode))
                .andDo((mvcResult) -> {
                    Project resultProject = entityTestService.getProject("ACOR");
                    assertNotNull(resultProject);
                    assertEquals("ACOR", resultProject.getCode());
                    assertEquals("ACOR", resultProject.getName());
                    assertEquals("admin", resultProject.getProjectLeader().getEmail());
                });
    }

    @WithUserDetails("admin")
    @Test
    public void projectDetailsPage_shouldReturnProjectDetailsPage_Status200() throws Exception {
        entityTestService.createProjects("ACOR", adminUser.getId());
        this.mvc.perform(get("/project/details/ACOR"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Project Details")))
                .andExpect(content().string(containsString("ACOR - ACOR")));
    }

    @WithUserDetails("user")
    @Test
    public void projectDetailsPage_shouldReturnOperationForbiddenException() throws Exception {
        entityTestService.createProjects("ACOR", adminUser.getId());
        try {
            this.mvc.perform(get("/project/details/ACOR"));
            fail("Should launch and exception");
        } catch (NestedServletException e) {
            assertTrue("Exception Should be caused by an OperationForbiddenException",
                    e.getCause() instanceof OperationForbiddenException);
        }
    }

    @WithUserDetails("admin")
    @Test
    public void assignProjectPage_shouldReturnAssignProjectPage_200() throws Exception {
        entityTestService.createProjects("ACOR", adminUser.getId());
        this.mvc.perform(get("/project/assign/ACOR"))
                .andExpect(content().string(containsString("<title>Assign Project</title>")))
                .andExpect(content().string(containsString("<form id=\"assignProjectForm\"")))
                .andExpect(content().string(containsString("name=\"projectID\"")))
                .andExpect(content().string(containsString("<select id=\"select-assigned-user\"")));
    }

    @WithUserDetails("admin")
    @Test
    public void assignProject_shouldAddUserToProjectMembers() throws Exception {
        String projectCode = "ACOR";
        entityTestService.createProjects(projectCode, adminUser.getId());
        User user = entityTestService.getUser("user");

        MockHttpServletRequestBuilder postMethod = post("/project/assign")
                .with(csrf())
                .param("projectID", projectCode)
                .param("assignTo", user.getId().toString());
        this.mvc.perform(postMethod)
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/project/details/" + projectCode))
                .andDo((mvcResult) -> {
                    Project project = entityTestService.getProject(projectCode);
                    assertNotNull(project.getAssignedTo());
                    assertNotEquals(0, project.getAssignedTo().size());
                    assertTrue(project.getAssignedTo().contains(user));
                });
    }

    @WithUserDetails("admin")
    @Test
    public void removeProjectPage_shouldReturnAPageToConfirmRemove() throws Exception {
        String projectCode = "ACOR";
        entityTestService.createProjects(projectCode, adminUser.getId());
        // Check that the project was created
        assertNotNull("Test Initialization Error: Project wasn't created correctly. Maybe there is a DB error",
                entityTestService.getProject(projectCode));

        // TEST
        String
                url = "/project/remove/" + projectCode;
        this.mvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<title>Remove Project</title>")))
                .andExpect(content().string(containsString("<form method=\"post\" action=\"" + url + "\">")));
    }

    @WithUserDetails("admin")
    @Test
    public void removeProject_shouldRemoveProjectFromDatabase() throws Exception {
        String projectCode = "ACOR";
        entityTestService.createProjects(projectCode, adminUser.getId());
        // Check that the project was created
        assertNotNull("Test Initialization Error: Project wasn't created correctly. Maybe there is a DB error",
                entityTestService.getProject(projectCode));

        // TEST
        MockHttpServletRequestBuilder postMethod = post("/project/remove/" + projectCode).with(csrf());
        this.mvc.perform(postMethod).andExpect(status().is3xxRedirection());
        assertNull(entityTestService.getProject(projectCode));
    }

    @WithUserDetails
    @Test
    public void removeProjectPage_shouldReturnForbiddenError() throws Exception {
        String projectCode = "ACOR";
        entityTestService.createProjects(projectCode, adminUser.getId());
        assertNotNull("Test Initialization Error: Project wasn't created correctly. Maybe there is a DB error",
                entityTestService.getProject(projectCode));

        //TEST
        this.mvc.perform(get("/project/remove/" + projectCode)).andExpect(status().is(403));

    }

    @WithUserDetails
    @Test
    public void removeProject_shouldReturnForbiddenError() throws Exception {
        String projectCode = "ACOR";
        entityTestService.createProjects(projectCode, adminUser.getId());
        assertNotNull("Test Initialization Error: Project wasn't created correctly. Maybe there is a DB error",
                entityTestService.getProject(projectCode));

        //TEST
        this.mvc.perform(post("/project/remove/" + projectCode).with(csrf())).andExpect(status().is(403));
    }

    @WithUserDetails("admin")
    @Test
    public void createCyclePage_shouldReturnPage() throws Exception {
        String projectCode = "ACOR";
        entityTestService.createProjects(projectCode, adminUser.getId());
        assertNotNull("Test Initialization Error: Project wasn't created correctly. Maybe there is a DB error",
                entityTestService.getProject(projectCode));

        // TEST
        this.mvc.perform(get("/project/" + projectCode + "/create-cycle"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<title>New Development Cycle</title>")))
                .andExpect(content().string(containsString("<form id=\"new-cycle-form\"")))
                .andExpect(content().string(containsString(
                        "<input class=\"form-control col\" id=\"cycle-name\" name=\"cycle-name\"")))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("\n", "<button type=\"submit\"", "Confirm</button>", "\n"))));
    }

    @WithUserDetails("admin")
    @Test
    public void createCycle_shouldCreateCycle() throws Exception {
        String projectCode = "ACOR";
        String cycleName = "TEST-CYCLE";
        entityTestService.createProjects(projectCode, adminUser.getId());
        assertNotNull("Test Initialization Error: Project wasn't created correctly. Maybe there is a DB error",
                entityTestService.getProject(projectCode));

        // TEST
        MockHttpServletRequestBuilder postMethod = post("/project/" + projectCode + "/create-cycle")
                .with(csrf())
                .param("cycle-name", cycleName);
        this.mvc.perform(postMethod).andExpect(status().is3xxRedirection());
        assertTrue(entityTestService.existsDevelopmentCycle(projectCode, cycleName));
    }

    @WithUserDetails("admin")
    @Test
    public void createCycle_shouldReturnNullNameException() throws Exception {
        String projectCode = "ACOR";
        String cycleName = "TEST-CYCLE";
        entityTestService.createProjects(projectCode, adminUser.getId());
        assertNotNull("Test Initialization Error: Project wasn't created correctly. Maybe there is a DB error",
                entityTestService.getProject(projectCode));

        // TEST
        MockHttpServletRequestBuilder postMethod = post("/project/" + projectCode + "/create-cycle")
                .with(csrf());
        this.mvc.perform(postMethod)
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("Required String parameter 'cycle-name' is not present"));
    }

    @WithUserDetails("admin")
    @Test
    public void createCycle_shouldReturnEmptyNameException() throws Exception {
        String projectCode = "ACOR";
        entityTestService.createProjects(projectCode, adminUser.getId());
        assertNotNull("Test Initialization Error: Project wasn't created correctly. Maybe there is a DB error",
                entityTestService.getProject(projectCode));

        // TEST
        MockHttpServletRequestBuilder postMethod = post("/project/" + projectCode + "/create-cycle")
                .with(csrf())
                .param("cycle-name", "");
        this.mvc.perform(postMethod)
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("Name of the development Cycle can't be empty"));
    }

    @WithUserDetails("admin")
    @Test
    public void createCycle_shouldReturnCycleNameInUseException() throws Exception {
        String projectCode = "ACOR";
        String cycleName = "TEST-CYCLE";
        entityTestService.createProjects(projectCode, adminUser.getId());
        entityTestService.createDevelopmentCycle(projectCode, cycleName);
        assertTrue("Test Initialization Error: Development Cycle wasn't created correctly. Maybe there is a DB error",
                entityTestService.existsDevelopmentCycle(projectCode, cycleName));

        // TEST
        MockHttpServletRequestBuilder postMethod = post("/project/" + projectCode + "/create-cycle")
                .with(csrf())
                .param("cycle-name", cycleName);
        this.mvc.perform(postMethod)
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("The selected name already exists for this project."));
    }

    @WithUserDetails("admin")
    @Test
    public void editProjectPage_shouldLaunchEntityNotFoundException() throws Exception {
        try {
            this.mvc.perform(get("/project/edit/ACOR"));
        } catch (NestedServletException e) {
            assertTrue(e.getRootCause() instanceof EntityNotFoundException);
        }
    }

    @WithUserDetails("admin")
    @Test
    public void editProjectPage_shouldReturnEditPage() throws Exception {
        String projectCode = "ACOR";
        entityTestService.createProjects(projectCode, adminUser.getId());
        assertNotNull("Test initialization Error: project wasn't created correctly. Maybe there is a DB problem",
                entityTestService.getProject(projectCode));

        this.mvc.perform(get("/project/edit/ACOR"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<title>Edit Project</title>")))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<form", "id=\"edit-project-form\"", "method=\"post\"", "action=",
                                "/project/edit/ACOR", ">"))))
                .andExpect(content().string(containsString("id=\"projectName\"")))
                .andExpect(content().string(containsString("id=\"projectState\"")))
                .andExpect(content().string(containsString("id=\"projectLeaderID\"")));

    }

    @WithUserDetails("user")
    @Test
    public void editProjectPage_shouldReturnForbiddenStatus() throws Exception {
        this.mvc.perform(get("/project/edit/ACOR")).andExpect(status().is(HttpStatus.FORBIDDEN.value()));
    }

    @WithUserDetails("user")
    @Test
    public void editProject_shouldReturnForbiddenStatus() throws Exception {
        this.mvc.perform(post("/post/edit/ACOR")).andExpect(status().is(HttpStatus.FORBIDDEN.value()));
    }

    @WithUserDetails("admin")
    @Test
    public void editProject_shouldEditProject() throws Exception {
        String projectCode = "ACOR";
        entityTestService.createProjects(projectCode, adminUser.getId());
        assertNotNull("Test initialization Error: project wasn't created correctly. Maybe there is a DB problem",
                entityTestService.getProject(projectCode));
        User user = entityTestService.getUser("user");
        // TEST
        String projectNewName = "NEW-NAME";
        MockHttpServletRequestBuilder postMethod = post("/project/edit/" + projectCode)
                .with(csrf())
                .param("projectName", projectNewName)
                .param("projectState", ProjectState.CLOSE.name())
                .param("projectLeaderID", user.getId().toString());
        this.mvc.perform(postMethod);

        Project project = entityTestService.getProject("ACOR");
        assertNotNull(project);
        assertEquals(projectNewName, project.getName());
        assertEquals(ProjectState.CLOSE, project.getProjectState());
        assertEquals(user, project.getProjectLeader());
    }

    @WithUserDetails("admin")
    @Test
    public void searchProjectPage_ShouldReturnAllProjects() throws Exception {
        String projectCode1 = "ACOR";
        String projectCode2 = "OTHER";
        entityTestService.createProjects(projectCode1, adminUser.getId());
        entityTestService.createProjects(projectCode2, adminUser.getId());

        this.mvc.perform(get("/project/search"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<title>Search Project</title>")))
                .andExpect(content().string(containsString(
                        "<input class=\"form-control\" id=\"projectLeaderName\" name=\"projectLeaderName\"")))
                .andExpect(content().string(containsString(
                        "<input class=\"form-control\" id=\"projectNameOrID\" name=\"projectNameOrID\"")))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<tr>", "<th>Project ID</th>", "<th>Project Name</th>",
                                "<th>Project Leader Name</th>", "</tr>"))))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<a", "href=", "/project/details/ACOR",">","ACOR","</a>"))))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<a", "href=", "/project/details/OTHER", ">", "OTHER", "</a>"))))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<a", "href=", "/profile/" + adminUser.getId(), ">", adminUser.getFullName(),
                                "</a>"))));
    }

    @WithUserDetails("user")
    @Test
    public void searchProjectPage_ShouldReturnNoProjects() throws Exception {
        String projectCode1 = "ACOR";
        String projectCode2 = "OTHER";
        entityTestService.createProjects(projectCode1, adminUser.getId());
        entityTestService.createProjects(projectCode2, adminUser.getId());

        this.mvc.perform(get("/project/search"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<title>Search Project</title>")))
                .andExpect(content().string(containsString(
                        "<input class=\"form-control\" id=\"projectLeaderName\" name=\"projectLeaderName\"")))
                .andExpect(content().string(containsString(
                        "<input class=\"form-control\" id=\"projectNameOrID\" name=\"projectNameOrID\"")))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<tr>", "<th>Project ID</th>", "<th>Project Name</th>",
                                "<th>Project Leader Name</th>", "</tr>"))))
                .andExpect(content().string(containsString("No projects found")));
    }

    @WithUserDetails("user")
    @Test
    public void searchProjectPage_ShouldReturnOnlyAssignedProjects() throws Exception {
        String projectCode1 = "ACOR";
        String projectCode2 = "OTHER";
        entityTestService.createProjects(projectCode1, adminUser.getId());
        Long userId = entityTestService.getUser("user").getId();
        entityTestService.createProjects(projectCode2, userId);

        this.mvc.perform(get("/project/search"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<title>Search Project</title>")))
                .andExpect(content().string(containsString(
                        "<input class=\"form-control\" id=\"projectLeaderName\" name=\"projectLeaderName\"")))
                .andExpect(content().string(containsString(
                        "<input class=\"form-control\" id=\"projectNameOrID\" name=\"projectNameOrID\"")))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<tr>", "<th>Project ID</th>", "<th>Project Name</th>",
                                "<th>Project Leader Name</th>", "</tr>"))))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<a", "href=", "/project/details/OTHER", ">", "OTHER", "</a>"))))
                .andExpect(content().string(not(stringContainsInOrder(
                        Arrays.asList("<a", "href=", "/project/details/ACOR",">","ACOR","</a>")))));
    }

    @WithUserDetails("admin")
    @Test
    public void searchProjectPage_ShouldReturnFilteredProjects() throws Exception {
        String projectCode1 = "ACOR";
        String projectCode2 = "OTHER";
        entityTestService.createProjects(projectCode1, adminUser.getId());
        User user = entityTestService.getUser("user");
        Long userId = user.getId();
        String userFullName = user.getFullName();
        user = null;
        entityTestService.createProjects(projectCode2, userId);

        this.mvc.perform(get("/project/search").param("projectNameOrID", projectCode2))
                .andExpect(status().isOk())
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<a", "href=", "/project/details/OTHER", ">", "OTHER", "</a>"))))
                .andExpect(content().string(not(stringContainsInOrder(
                        Arrays.asList("<a", "href=", "/project/details/ACOR",">","ACOR","</a>")))));
        this.mvc.perform(get("/project/search").param("projectLeaderName", adminUser.getFullName()))
                .andExpect(status().isOk())
                .andExpect(content().string(not(stringContainsInOrder(
                        Arrays.asList("<a", "href=", "/project/details/OTHER", ">", "OTHER", "</a>")))))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<a", "href=", "/project/details/ACOR",">","ACOR","</a>"))));
        this.mvc.perform(get("/project/search").param("projectNameOrID", projectCode2)
                .param("projectLeaderName", userFullName))
                .andExpect(status().isOk())
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<a", "href=", "/project/details/OTHER", ">", "OTHER", "</a>"))))
                .andExpect(content().string(not(stringContainsInOrder(
                        Arrays.asList("<a", "href=", "/project/details/ACOR",">","ACOR","</a>")))));
        this.mvc.perform(get("/project/search").param("projectNameOrID", projectCode2)
                .param("projectLeaderName", adminUser.getFullName()))
                .andExpect(status().isOk())
                .andExpect(content().string(not(stringContainsInOrder(
                        Arrays.asList("<a", "href=", "/project/details/OTHER", ">", "OTHER", "</a>")))))
                .andExpect(content().string(not(stringContainsInOrder(
                        Arrays.asList("<a", "href=", "/project/details/ACOR",">","ACOR","</a>")))))
                .andExpect(content().string(containsString("No projects found")));
    }
}
