package es.udc.fic.gesta.app.integration.user;

import es.udc.fic.gesta.app.integration.BaseIntegrationTest;
import es.udc.fic.gesta.model.entities.user.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.Arrays;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.junit.Assert.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserIntegrationTest extends BaseIntegrationTest {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void signInPage_shouldReturnSignInPage() throws Exception {
        this.mvc.perform(get("/signin"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<title>Sign In</title>")))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<form", "id=\"signinForm\"", "/authenticate", ">", "</form>"))))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<input", "type=\"text\"", "name=\"username\"", "/>"))))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<input", "type=\"password\"", "name=\"password\"", "/>"))));
    }

    @WithUserDetails("admin")
    @Test
    public void createUserPage_shouldReturnCreateUserPage() throws Exception {
        this.mvc.perform(get("/user/create"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<title>Create User</title>")))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<form", "id=\"userForm\"", "/user/create", ">", "</form>"))))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<input type=\"text\"", "name=\"fullName\"", "/>"))))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<input type=\"text\"", "name=\"email\"", "/>"))))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<input type=\"password\"", "name=\"password\"", "/>"))));
    }

    @WithMockUser
    @Test
    public void createUserPage_shouldReturnForbidden() throws Exception {
        this.mvc.perform(get("/user/create")).andExpect(status().is(403));
    }

    @WithUserDetails("admin")
    @Test
    public void createUser_shouldCreateUser() throws Exception {
        String userEmail = "johndoe@mail.com";
        String fullName = "John Doe";
        String password = "password";
        MockHttpServletRequestBuilder postMethod = post("/user/create")
                .with(csrf())
                .param("fullName", fullName)
                .param("email", userEmail)
                .param("password", password)
                .param("confirmPassword", password);
        this.mvc.perform(postMethod)
                .andExpect(status().is3xxRedirection());
        User user = entityTestService.getUser(userEmail);
        assertNotNull(user);
        assertEquals(fullName, user.getFullName());
        assertEquals(userEmail,user.getEmail());
    }

    @WithUserDetails("admin")
    @Test
    public void createUser_shouldCreateUse() throws Exception {
        String userEmail = "admin";
        String fullName = "John Doe";
        String password = "password";
        String fullNameOriginal = entityTestService.getUser(userEmail).getFullName();

        MockHttpServletRequestBuilder postMethod = post("/user/create")
                .with(csrf())
                .param("fullName", fullName)
                .param("email", userEmail)
                .param("password", password)
                .param("confirmPassword", password)
                .header("X-Requested-With", "XMLHttpRequest");
        this.mvc.perform(postMethod)
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
        User user = entityTestService.getUser(userEmail);
        assertNotNull(user);
        assertNotEquals(fullName, user.getFullName());
        assertEquals(fullNameOriginal,user.getFullName());
        assertEquals(userEmail, user.getEmail());
    }


    @WithMockUser
    @Test
    public void createUser_shouldReturnForbidden() throws Exception {
        MockHttpServletRequestBuilder postMethod = post("/user/create")
            .with(csrf())
            .param("fullName","John Doe")
            .param("email","johndoe@mail.com")
            .param("password", "password")
            .param("confirmPassword", "password");
        this.mvc.perform(postMethod).andExpect(status().is4xxClientError());
    }

    @WithMockUser
    @Test
    public void changePasswordPage_shouldReturnChangePasswordPage() throws Exception {
        this.mvc.perform(get("/user/changepassword"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<title>Change Password</title>")))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<input", "type=\"password\"", "name=\"oldPassword\"","/>"))))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<input", "type=\"password\"", "name=\"newPassword\"","/>"))))
                .andExpect(content().string(stringContainsInOrder(
                        Arrays.asList("<input", "type=\"password\"", "name=\"confirmPassword\"","/>"))));

    }

    @WithUserDetails("user")
    @Test
    public void changePasswordPage_shouldChangePassword() throws Exception {
        String password = entityTestService.getUser("user").getPassword();

        MockHttpServletRequestBuilder postMethod = post("/user/updatePassword")
                .with(csrf())
                .param("oldPassword", "user")
                .param("newPassword", "newPassword")
                .param("confirmPassword", "newPassword");
        this.mvc.perform(postMethod).andExpect(status().is3xxRedirection());

        assertNotEquals(password, entityTestService.getUser("user").getPassword());
    }

}
