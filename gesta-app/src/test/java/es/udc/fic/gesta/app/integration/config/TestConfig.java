package es.udc.fic.gesta.app.integration.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "es.udc.fic.gesta.app.integration.util")
public class TestConfig {
}
