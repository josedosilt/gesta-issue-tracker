package es.udc.fic.gesta.app.integration;

import es.udc.fic.gesta.GesTaIssueTrackerApplication;
import es.udc.fic.gesta.app.config.I18NConfig;
import es.udc.fic.gesta.app.config.JpaConfig;
import es.udc.fic.gesta.app.config.MethodSecurityConfig;
import es.udc.fic.gesta.app.config.WebSecurityConfig;
import es.udc.fic.gesta.app.integration.config.TestConfig;
import es.udc.fic.gesta.app.integration.util.EntityTestService;
import es.udc.fic.gesta.model.entities.user.User;
import es.udc.fic.gesta.model.entities.user.UserRepository;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {GesTaIssueTrackerApplication.class, WebSecurityConfig.class, JpaConfig.class,
        I18NConfig.class, MethodSecurityConfig.class, TestConfig.class})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Transactional
public abstract class BaseIntegrationTest {

    @Autowired
    protected EntityTestService entityTestService;
    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected WebApplicationContext context;

    protected User adminUser = null;


    protected MockMvc mvc;

    @Before
    public void setup() {
        if (adminUser == null) {
            adminUser = userRepository.findByEmail("admin");
        }
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

}
