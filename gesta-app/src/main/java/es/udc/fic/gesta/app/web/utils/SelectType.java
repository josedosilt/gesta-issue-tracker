package es.udc.fic.gesta.app.web.utils;

public enum SelectType {
    SINGLE,
    MULTIPLE;
}
