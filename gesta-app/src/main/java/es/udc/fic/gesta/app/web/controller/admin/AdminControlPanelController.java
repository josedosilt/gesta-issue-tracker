package es.udc.fic.gesta.app.web.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.security.RolesAllowed;

@Controller
public class AdminControlPanelController {

    private static final String CONTROL_PANEL_VIEW = "admin/panel";


    @GetMapping("/admin/panel")
    @RolesAllowed("ROLE_ADMIN")
    public String getControlPanelView(){
        return CONTROL_PANEL_VIEW;
    }

}
