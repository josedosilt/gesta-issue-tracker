package es.udc.fic.gesta.app.web.controller.issue.search;

import es.udc.fic.gesta.model.service.issue.IssueService;
import es.udc.fic.gesta.model.util.form.SearchIssueForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

public abstract class BaseIssueSearchController {

    @Autowired
    protected IssueService issueService;

    protected void addSearchResult(Model model, SearchIssueForm searchIssueForm, Integer page){
        model.addAttribute("issueDetailsPage",
                issueService.searchIssues(searchIssueForm, null, page - 1, 10));
    }

}
