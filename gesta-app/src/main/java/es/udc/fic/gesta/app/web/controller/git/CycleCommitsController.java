package es.udc.fic.gesta.app.web.controller.git;

import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.exceptions.DevelopmentCycleNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.git.GitLabRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

@Controller
public class CycleCommitsController {

    private static final String CYCLE_COMMITS_VIEW = "git/cycle-commits";
    private static final String CYCLE_COMMITS_FRAGMENT = CYCLE_COMMITS_VIEW + " :: cycleCommitsFragment";

    @Autowired
    private GitLabRepoService gitService;


    @GetMapping("/project/{project_id}/cycle/{cycle_name}/commits")
    public String getCycleCommitsView(Model model, @PathVariable("project_id") String projectId,
                                      @PathVariable("cycle_name") String cycleName,
                                      @RequestHeader(name = Ajax.REQUESTED_WITH_HEADER, required = false)
                                              String requestedWith) throws DevelopmentCycleNotFoundException,
            OperationForbiddenException {
        model.addAttribute("issueCommitDTOS", gitService.findCycleCommits(projectId, cycleName));
        if(Ajax.isAjaxRequest(requestedWith)){
            return CYCLE_COMMITS_FRAGMENT;
        }
        return CYCLE_COMMITS_VIEW;
    }

}
