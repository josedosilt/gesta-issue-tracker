package es.udc.fic.gesta.app.web.controller.project;

import es.udc.fic.gesta.model.service.exceptions.EntityNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.issue.IssueService;
import es.udc.fic.gesta.model.service.project.ProjectDetails;
import es.udc.fic.gesta.model.service.project.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class ProjectController {

    private static final String PROJECT_DETAILS_VIEW = "project/details";

    @Autowired
    private ProjectService projectService;
    @Autowired
    private IssueService issueService;

    @GetMapping("/project/details/{projectID}")
    public String getProjectDetails(Model model, @PathVariable String projectID) throws OperationForbiddenException {

        ProjectDetails projectDetails = null;
        try {
            projectDetails = projectService.getProjectDetails(projectID, true);
            model.addAttribute("projectIssues", projectDetails.getIssueDetails());
        } catch (EntityNotFoundException e) {
            return "redirect:/error";
        }
        model.addAttribute("projectDetails", projectDetails);

        return PROJECT_DETAILS_VIEW;
    }

}
