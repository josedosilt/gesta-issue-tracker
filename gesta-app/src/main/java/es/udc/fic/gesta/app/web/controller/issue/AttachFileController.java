package es.udc.fic.gesta.app.web.controller.issue;

import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.exceptions.IssueNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.StorageException;
import es.udc.fic.gesta.model.service.exceptions.StorageFileNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.issue.IssueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class AttachFileController {

    private static final String ATTACH_FILE_VIEW = "issue/attach";
    private static final String ATTACH_FILE_FRAGMENT = ATTACH_FILE_VIEW + " :: attachFileFragment";

    @Autowired
    private IssueService issueService;

    @GetMapping("/issue/{issue_id}/attach")
    public String getAttachFileView(Model model,
                                     @PathVariable("issue_id") String issueId,
                                     @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false) String requestedWith) throws
            IssueNotFoundException, OperationForbiddenException {

        model.addAttribute("issueDetails",issueService.getIssueDetails(issueId));
        if(Ajax.isAjaxRequest(requestedWith)){
            return ATTACH_FILE_FRAGMENT;
        }
        return ATTACH_FILE_VIEW;
    }

    @PostMapping("/issue/{issue_id}/attach")
    public String attachFile(@PathVariable("issue_id") String issueId, @RequestParam("file") MultipartFile file) throws
            IssueNotFoundException, StorageException, OperationForbiddenException {

        issueService.attachFile(issueId, file);
        return "redirect:/issue/details/" + issueId;
    }

    @GetMapping("/issue/{issue_id}/file/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable("issue_id") String issueId , @PathVariable String filename) throws
            IssueNotFoundException, StorageFileNotFoundException, OperationForbiddenException {

        Resource file = issueService.loadAttachedFile(issueId, filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }
}
