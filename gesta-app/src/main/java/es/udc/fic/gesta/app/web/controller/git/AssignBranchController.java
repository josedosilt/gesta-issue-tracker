package es.udc.fic.gesta.app.web.controller.git;

import es.udc.fic.gesta.model.service.exceptions.DevelopmentCycleNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.GitLabRepositoryNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.exceptions.git.NoRepositoryAsignedException;
import es.udc.fic.gesta.model.service.git.GitLabRepoService;
import es.udc.fic.gesta.model.service.project.ProjectService;
import es.udc.fic.gesta.model.service.project.exception.ProjectNotFoundException;
import es.udc.fic.gesta.app.web.i18n.CommonMessages;
import es.udc.fic.gesta.app.web.utils.Ajax;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@Controller
public class AssignBranchController {

    public static final String BRANCHES_VIEW = "git/assign-branch";
    public static final String BRANCHES_FRAGMENT = BRANCHES_VIEW + " :: branchesFragment";

    @Autowired
    private GitLabRepoService gitLabRepoService;
    @Autowired
    private ProjectService projectService;

    @GetMapping("/project/{project_id}/cycle/{cycle_id}/assign-branch")
    public String getBranchesView(Model model, @PathVariable("project_id") String projectId,
                                  @PathVariable("cycle_id") String cycleId,
                                  @RequestHeader(name = Ajax.REQUESTED_WITH_HEADER, required = false)
                                          String requestedWith) throws ProjectNotFoundException,
            GitLabRepositoryNotFoundException, OperationForbiddenException {
        model.addAttribute("branchDTOS", gitLabRepoService.findProjectBranches(projectId));
        model.addAttribute("isSelect", true);
        String selectUrl = "/project/" + projectId + "/cycle/" + cycleId + "/assign-branch";
        model.addAttribute("selectUrl", selectUrl);
        if(Ajax.isAjaxRequest(requestedWith)){
            return BRANCHES_FRAGMENT;
        }
        return BRANCHES_VIEW;
    }

    @PostMapping("/project/{project_id}/cycle/{cycle_id}/assign-branch")
    public String assignBranchToCycle(@PathVariable("project_id") String projectId,
                                      @PathVariable("cycle_id") String cycleId,
                                      @RequestParam("branch") String branchName,
                                      @RequestHeader(name = Ajax.REQUESTED_WITH_HEADER, required = false)
                                                  String requestedWith) {

        try {
            projectService.assignGitBranchToCycle(projectId, cycleId, branchName);
        } catch (DevelopmentCycleNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, CommonMessages.ISSUE_NOT_FOUND_MESSAGE);
        } catch (OperationForbiddenException | NoRepositoryAsignedException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, CommonMessages.ERROR_UNKNOWN);
        }
        return "redirect:/project/details/"+projectId;
    }
}
