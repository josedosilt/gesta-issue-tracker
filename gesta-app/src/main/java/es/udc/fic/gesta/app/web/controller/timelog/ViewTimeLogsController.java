package es.udc.fic.gesta.app.web.controller.timelog;

import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.exceptions.UserNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.BadRequestException;
import es.udc.fic.gesta.model.service.timelog.TimeLogService;
import es.udc.fic.gesta.model.util.UserKeyFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;

@Controller
public class ViewTimeLogsController {

    private static final String TIME_LOGS_VIEW = "timeLog/time-logs";

    private static final String TIME_LOGS_FRAGMENT = TIME_LOGS_VIEW + " :: timeLogsFragment";

    private static final String TIME_LOGS_RESULT_FRAGMENT = TIME_LOGS_VIEW + " :: timeLogsResultFragment";

    @Autowired
    private TimeLogService timeLogService;

    @GetMapping("/time-logs")
    public String getTimeLogsView(Model model, Principal principal,
                                  @RequestParam(value = "issueId", required = false) String issueId,
                                  @RequestParam(value = "projectId", required = false) String projectId,
                                  @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                          String requestedWith) throws UserNotFoundException {

        model.addAttribute("timeLogsDTO", timeLogService.getUserTimeLogs(principal.getName(), issueId, projectId));

        model.addAttribute("projectFilter", timeLogService.getProjectIdsFromTimeLogs(principal.getName()));
        model.addAttribute("issueFilter", timeLogService.getIssueIdsFromTimeLogs(principal.getName()));
        if(Ajax.isAjaxRequest(requestedWith)){
            return TIME_LOGS_FRAGMENT;
        }
        return TIME_LOGS_VIEW;
    }

    @GetMapping("/time-logs-result")
    public String getTimeLogsResult(Model model, Principal principal,
                                    @RequestParam(value = "issueId", required = false) String issueId,
                                    @RequestParam(value = "projectId", required = false) String projectId) throws
            UserNotFoundException {

        model.addAttribute("timeLogsDTO", timeLogService.getUserTimeLogs(principal.getName(), issueId, projectId));
        return TIME_LOGS_RESULT_FRAGMENT;
    }

    @GetMapping("/project/{id}/time-logs")
    public String getProjectTimeLogs(Model model, Principal principal, @PathVariable("id") String projectId,
                                     @RequestParam(value = "issueId", required = false) String issueId,
                                     @RequestParam(value = "user", required = false) List<Long> userId) throws
            UserNotFoundException, BadRequestException {


        UserKeyFilter userFilter = new UserKeyFilter(userId, null);
        model.addAttribute("timeLogsDTO",
                timeLogService.getProjectOrIssueTimeLogs(principal.getName(), issueId, projectId, userFilter));
        model.addAttribute("isProjectTimeLogs", true);
        model.addAttribute("userProfilesDTOs", timeLogService.getUserFromProjectWithTimeLogs(projectId));
        model.addAttribute("selectedUsers", userId);
        return TIME_LOGS_VIEW;
    }
}
