package es.udc.fic.gesta.app.web.utils;

import es.udc.fic.gesta.model.util.IssueStatus;

import java.util.Map;

public final class WebUtils {

    public static final String ERROR_MSG_CODE = "error-msg";

    private WebUtils(){}

    public static String createUrlWithoutPage(String baseUrl, Map<String,String[]> parameters){
        StringBuilder resultBuilder = new StringBuilder(baseUrl);
        if(!parameters.isEmpty()){
            resultBuilder.append("?");
            for(Map.Entry<String,String[]>  entry : parameters.entrySet()){
                if(entry.getKey().equals("page")){
                    continue;
                }
                for(String value : entry.getValue()){
                    resultBuilder.append(entry.getKey()).append("=").append(value).append("&");
                }
            }
        }
        return resultBuilder.toString();
    }

    public static String getIssueStatusClass(IssueStatus issueStatus){
        switch (issueStatus){
            case OPEN: return "bg-success";
            case CLOSED: return "bg-danger";
            case IN_PROGRESS: return "bg-warning";
        }
        return "";
    }
}
