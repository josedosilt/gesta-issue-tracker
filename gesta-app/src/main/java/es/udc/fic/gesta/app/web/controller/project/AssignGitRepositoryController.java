package es.udc.fic.gesta.app.web.controller.project;

import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.app.web.utils.FormInput;
import es.udc.fic.gesta.model.service.exceptions.EntityNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.git.GitLabRepoService;
import es.udc.fic.gesta.model.service.project.ProjectService;
import es.udc.fic.gesta.model.util.form.GitRepositoryForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.Arrays;
import java.util.Locale;

@Controller
public class AssignGitRepositoryController {

    private static final String ASSIGN_GIT_REPOSITORY_VIEW = "project/assign-git";

    private static final String ASSIGN_GIT_REPOSITORY_FRAGMENT = ASSIGN_GIT_REPOSITORY_VIEW + " :: assignFragment";

    private static final String SEARCH_REPOSITORY_VIEW = "git/repositories";
    private static final String SEARCH_REPOSITORY_FRAGMENT = SEARCH_REPOSITORY_VIEW + " :: repositoriesResultFragment";

    @Autowired
    private ProjectService projectService;
    @Autowired
    private GitLabRepoService gitLabRepoService;
    @Autowired
    private MessageSource messageSource;

    @GetMapping("/project/{id}/assign-git")
    @RolesAllowed("ROLE_ADMIN")
    public String getAssignGitRepositoryView(Model model, @PathVariable("id") String projectId,
                                             @ModelAttribute GitRepositoryForm gitRepositoryForm,
                                             @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                                     String requestedWith) throws
            OperationForbiddenException, EntityNotFoundException {

        model.addAttribute("projectDetails", projectService.getProjectDetails(projectId));
        if (Ajax.isAjaxRequest(requestedWith)) {
            return ASSIGN_GIT_REPOSITORY_FRAGMENT;
        }
        return ASSIGN_GIT_REPOSITORY_VIEW;
    }

    @PostMapping("/project/{id}/assign-git")
    @RolesAllowed("ROLE_ADMIN")
    public String assignGitRepository(@PathVariable("id") String projectId,
                                      @ModelAttribute GitRepositoryForm gitRepositoryForm) throws
            OperationForbiddenException, EntityNotFoundException {

        projectService.assignGitRepository(projectId, gitRepositoryForm);
        return "redirect:/project/details/" + projectId;
    }

    @GetMapping("/project/{project_id}/assign-git/search")
    @RolesAllowed("ROLE_ADMIN")
    public String getSearchRepositoryView(Model model, Locale locale,
                                          @PathVariable("project_id") String projectId,
                                          @RequestHeader(name = Ajax.REQUESTED_WITH_HEADER, required = false)
                                                  String requestedWith) {
        FormInput urlInput = new FormInput("url", "repositoryUrl", "hidden");
        FormInput idInput = new FormInput("repositoryId", "repositoryId", "hidden");
        FormInput nameInput = new FormInput("name", "repositoryName", "hidden");
        model.addAttribute("selectFormInputs", Arrays.asList(urlInput, idInput, nameInput));
        model.addAttribute("selectFormAction", "/project/" + projectId + "/assign-git");
        model.addAttribute("isSelect", true);
        model.addAttribute("repositoryDTOS", gitLabRepoService.findRepositories());
        model.addAttribute("pageTitle",
                projectId + " - " + messageSource.getMessage("gitRepository.title.assign", null, locale));
        if (Ajax.isAjaxRequest(requestedWith)) {
            return SEARCH_REPOSITORY_FRAGMENT;
        }
        return SEARCH_REPOSITORY_VIEW;
    }

}
