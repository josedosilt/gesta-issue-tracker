package es.udc.fic.gesta.app.web.controller.user;

import es.udc.fic.gesta.model.entities.utils.PrivilegeLevel;
import es.udc.fic.gesta.model.entities.utils.Privileges;
import es.udc.fic.gesta.model.service.exceptions.UserNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.project.ProjectDetails;
import es.udc.fic.gesta.model.service.project.exception.ProjectNotFoundException;
import es.udc.fic.gesta.model.service.user.UserProfileDTO;
import es.udc.fic.gesta.model.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.security.RolesAllowed;
import java.util.ArrayList;
import java.util.List;

@Controller
public class AssignPrivilegesController {

    private static final String ASSIGN_PRIVILEGES_VIEW = "user/assign-privileges";

    @Autowired
    private UserService userService;

    @GetMapping("/profile/{id}/privileges")
    @RolesAllowed("ROLE_ADMIN")
    public String getAssignPrivilegesView(Model model, @PathVariable("id") Long userId) {
        UserProfileDTO userProfileDTO = userService.getUserProfile(userId);
        model.addAttribute("userProfileDTO", userProfileDTO);
        model.addAttribute("privilegeList", Privileges.valuesByLevel(PrivilegeLevel.PROJECT));
        model.addAttribute("userProjectIds",
                userProfileDTO.getProjects().stream().map(ProjectDetails::getProjectID).toArray());
        return ASSIGN_PRIVILEGES_VIEW;
    }

    @PostMapping("/profile/{id}/privileges")
    @RolesAllowed("ROLE_ADMIN")
    public String assignPrivileges(@PathVariable("id") Long userId, @RequestParam("project") String projectId,
                                   @RequestParam(value= "privilege", defaultValue = "") List<String> privileges) throws ProjectNotFoundException,
            UserNotFoundException, OperationForbiddenException {
        List<Privileges> privilegesList = new ArrayList<>();
        for (String p : privileges) {
            privilegesList.add(Privileges.valueOf(p));
        }
        userService.assignPrivileges(userId, projectId, privilegesList);

        return "redirect:/profile/" + userId;
    }
}
