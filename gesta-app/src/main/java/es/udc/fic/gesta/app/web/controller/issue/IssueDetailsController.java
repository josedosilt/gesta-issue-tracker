package es.udc.fic.gesta.app.web.controller.issue;

import es.udc.fic.gesta.app.web.i18n.CommonMessages;
import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.dto.CommentDTO;
import es.udc.fic.gesta.model.service.exceptions.EmptyCommentException;
import es.udc.fic.gesta.model.service.exceptions.IssueNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.UserNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.issue.IssueDetails;
import es.udc.fic.gesta.model.service.issue.IssueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.Locale;

@Controller
public class IssueDetailsController {

    private static final String ISSUE_DETAILS_VIEW = "issue/details";

    @Autowired
    private IssueService issueService;

    @Autowired
    private MessageSource messageSource;

    @GetMapping("/issue/details/{id}")
    public String getIssueDetailsView(Model model, @PathVariable("id") String issueId) throws IssueNotFoundException,
            OperationForbiddenException {
        IssueDetails issueDetails  = issueService.getIssueDetails(issueId, true);
        Page<CommentDTO> comments = issueService.getComments(issueId, 0, 10);
        model.addAttribute("issueDetails", issueDetails);
        model.addAttribute("commentsPage", comments);
        return ISSUE_DETAILS_VIEW;
    }

    @PostMapping(value = "/issue/comment/{id}", headers = Ajax.SPRING_MVC_HEADER_NO_AJAX)
    public String addCommentToIssue(Principal principal, RedirectAttributes ra, Locale locale,
                                    @PathVariable("id") String issueId,
                                    @RequestParam("commentContent") String commentContent) throws
            IssueNotFoundException, UserNotFoundException, OperationForbiddenException {

        try {
            issueService.commentIssue(issueId, principal.getName(), commentContent);
        } catch (EmptyCommentException e) {
            ra.addFlashAttribute("addCommentError",
                    messageSource.getMessage(CommonMessages.EMPTY_COMMENT_ERROR, null, locale));
        }

        return "redirect:/issue/details/" + issueId;
    }

    @PostMapping(value = "/issue/comment/{id}", headers = Ajax.SPRING_MVC_HEADER_AJAX)
    public String addCommentToIssueAjax(Principal principal, Model model, RedirectAttributes ra, Locale locale,
                                                HttpServletResponse httpServletResponse,
                                                @PathVariable("id") String issueId,
                                                @RequestParam("commentContent") String commentContent) throws
            Exception {

        try {
            issueService.commentIssue(issueId, principal.getName(), commentContent);
            model.addAttribute("commentsPage", issueService.getComments(issueId, 0, 10));
            return "issue/details :: commentsSection";
        } catch (EmptyCommentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    messageSource.getMessage(CommonMessages.EMPTY_COMMENT_ERROR, null, locale));
        }
    }
}
