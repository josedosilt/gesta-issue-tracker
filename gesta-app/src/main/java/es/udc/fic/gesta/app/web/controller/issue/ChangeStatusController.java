package es.udc.fic.gesta.app.web.controller.issue;

import es.udc.fic.gesta.app.web.i18n.CommonMessages;
import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.exceptions.IssueNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.issue.IssueDetails;
import es.udc.fic.gesta.model.service.issue.IssueService;
import es.udc.fic.gesta.model.util.IssueStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Controller
public class ChangeStatusController {

    private static final String CHANGE_STATUS_VIEW = "issue/change-status";

    private static final String CHANGE_STATUS_FRAGMENT = CHANGE_STATUS_VIEW + " :: changeStatusFragment";

    @Autowired
    private IssueService issueService;
    @Autowired
    private MessageSource messageSource;

    @GetMapping("/issue/{issueId}/change-status")
    public String getChangeStatusView(Model model,
                                      @PathVariable("issueId") String issueId,
                                      @RequestHeader(name = Ajax.REQUESTED_WITH_HEADER, required = false)
                                              String requestedWith) throws IssueNotFoundException,
            OperationForbiddenException {
        IssueDetails issueDetails = issueService.getIssueDetails(issueId);
        model.addAttribute("issueDetails", issueDetails);
        List<IssueStatus> statusOptions = new ArrayList<>();
        for(IssueStatus status : IssueStatus.values()){
            if(!status.equals(issueDetails.getIssueStatus())){
                statusOptions.add(status);
            }
        }
        model.addAttribute("statusOptions", statusOptions);
        if(Ajax.isAjaxRequest(requestedWith)){
            return CHANGE_STATUS_FRAGMENT;
        }
        return CHANGE_STATUS_VIEW;
    }

    @PostMapping("/issue/{issueId}/change-status")
    public String changeIssueStatus(Model model, Locale locale,
                                  @PathVariable("issueId") String issueId,
                                  @RequestParam("status") IssueStatus issueStatus) {
        try {
            issueService.changeIssueStatus(issueId, issueStatus);
        } catch (IssueNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    this.messageSource.getMessage(CommonMessages.ISSUE_NOT_FOUND_MESSAGE, null, locale));
        } catch (OperationForbiddenException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    this.messageSource.getMessage(CommonMessages.OPERATION_FORBIDDEN, null, locale));
        }

        return "redirect:/issue/details/" + issueId;
    }

}
