package es.udc.fic.gesta.app.web.controller.issue;

import es.udc.fic.gesta.app.web.i18n.CommonMessages;
import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.exceptions.IssueNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.UserNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.issue.IssueService;
import es.udc.fic.gesta.model.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.Collections;
import java.util.Locale;

@Controller
public class ViewWatchersController {

    public static final String WATCHERS_VIEW = "issue/view-watchers";

    public static final String WATCHERS_FRAGMENT = WATCHERS_VIEW + " :: viewWatchersFragment";

    @Autowired
    private IssueService issueService;
    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;

    @GetMapping("/issue/{id}/watchers")
    public String getWatchersView(Model model, Principal principal, @PathVariable("id") String issueId, Locale locale,
                                  @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                          String requestedWith) throws OperationForbiddenException {
        try {
            model.addAttribute("issueId", issueId);
            model.addAttribute("issueWatchers", issueService.getIssueWatchers(issueId));
            model.addAttribute("connectedUser", userService.getUserProfile(principal.getName()));
            model.addAttribute("canRemoveWatchers", issueService.canRemoveWatchers(principal.getName(), issueId));
        } catch (IssueNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    messageSource.getMessage(CommonMessages.ISSUE_NOT_FOUND_MESSAGE, null, locale));
        } catch (UserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    messageSource.getMessage(CommonMessages.USER_NOT_FOUND_MESSAGE, null, locale));
        }

        if (Ajax.isAjaxRequest(requestedWith)) {
            return WATCHERS_FRAGMENT;
        }
        return WATCHERS_VIEW;
    }

    @PostMapping("/issue/{issueId}/remove-watcher")
    @ResponseStatus(HttpStatus.OK)
    public void removeUserFromWatchers(HttpServletResponse hsp, Principal principal,
                                       @PathVariable("issueId") String issueId,
                                       @RequestParam(value = "user", required = false) Long userId,
                                       @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                                   String requestedWith) throws IOException {

        try {
            if (userId == null) {
                issueService.removeFromWatchers(principal.getName(), issueId);
            }else{
                issueService.removeWatchers(principal.getName(), issueId, Collections.singletonList(userId));
            }
        } catch (UserNotFoundException | IssueNotFoundException | OperationForbiddenException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, null);
        }

        if(!Ajax.isAjaxRequest(requestedWith)){
            hsp.sendRedirect("/issue/details/" + issueId);
        }

    }

}
