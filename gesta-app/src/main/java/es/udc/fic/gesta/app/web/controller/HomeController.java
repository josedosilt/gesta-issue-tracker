package es.udc.fic.gesta.app.web.controller;

import es.udc.fic.gesta.model.service.exceptions.UserNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.issue.IssueService;
import es.udc.fic.gesta.model.service.project.ProjectService;
import es.udc.fic.gesta.model.util.form.SearchIssueForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class HomeController {

    @Autowired
    private IssueService issueService;
    @Autowired
    private ProjectService projectService;


    @GetMapping("/")
    public String index(Model model, Principal principal) throws OperationForbiddenException, UserNotFoundException {
        SearchIssueForm searchIssueForm = new SearchIssueForm();
        searchIssueForm.setAssignedToNameOrEmail(principal.getName());
        model.addAttribute("assignedIssues", issueService.searchIssues(searchIssueForm, null, 0,5));
        model.addAttribute("assignedProjects", projectService.findAssignedProjects(0,5));
        model.addAttribute("watchedIssues", issueService.findWatchedIssues(0, 5));

        return "index";
    }
}
