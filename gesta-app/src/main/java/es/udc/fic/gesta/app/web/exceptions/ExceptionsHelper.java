package es.udc.fic.gesta.app.web.exceptions;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

public final class ExceptionsHelper {

    private ExceptionsHelper(){}

    public static String getMsgFromConstraintViolationException(ConstraintViolationException e){
        String msg = "";
        for(ConstraintViolation cv : e.getConstraintViolations()){
            msg = cv.getMessage();
        }
        return msg;
    }

}
