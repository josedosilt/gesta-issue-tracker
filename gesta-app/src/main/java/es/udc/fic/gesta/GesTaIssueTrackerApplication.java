package es.udc.fic.gesta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GesTaIssueTrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GesTaIssueTrackerApplication.class, args);
	}

}
