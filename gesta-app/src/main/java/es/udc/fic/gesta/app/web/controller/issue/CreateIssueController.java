package es.udc.fic.gesta.app.web.controller.issue;

import es.udc.fic.gesta.app.web.controller.utils.ControllerHelper;
import es.udc.fic.gesta.app.web.exceptions.ExceptionsHelper;
import es.udc.fic.gesta.app.web.i18n.CommonMessages;
import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.entities.issue.Issue;
import es.udc.fic.gesta.model.service.exceptions.EntityNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.ProjectNotAssignedException;
import es.udc.fic.gesta.model.service.exceptions.UserNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.issue.IssueService;
import es.udc.fic.gesta.model.service.project.ProjectDetails;
import es.udc.fic.gesta.model.service.project.ProjectService;
import es.udc.fic.gesta.model.service.project.exception.ProjectNotFoundException;
import es.udc.fic.gesta.model.service.utils.ValidatingService;
import es.udc.fic.gesta.model.util.IssueForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.security.Principal;

@Controller
public class CreateIssueController {

    private static final String CREATE_ISSUE_VIEW = "issue/create";

    @Autowired
    private IssueService issueService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ValidatingService validatingService;

    @GetMapping("/issue/create")
    private String getCreateIssueView(Model model, @RequestParam(value = "projectID", required = false) String projectID) throws
            OperationForbiddenException {
        IssueForm issueForm = new IssueForm();
        issueForm.setProjectID(projectID);
        model.addAttribute("issueForm", issueForm);
        if(projectID !=null) {
            try {
                ProjectDetails project = projectService.getProjectDetails(projectID);
            } catch (EntityNotFoundException e) {
                model.addAttribute("projectNotFound", true);
            }
        }

        return CREATE_ISSUE_VIEW;
    }

    @PostMapping("/issue/create")
    private void createIssue(HttpServletResponse httpServletResponse, @ModelAttribute IssueForm issueForm,
                                       RedirectAttributes ra, Principal principal,
                                       @RequestHeader(value = "X-Requested-With", required = false)
                                       String requestedWith)
            throws IOException, ProjectNotAssignedException, OperationForbiddenException {

        String onErrorRedirectUrl = "/issue/create?projectID="+issueForm.getProjectID();
        try {
            Issue issue =issueService.createIssue(issueForm, principal.getName());
            httpServletResponse.setStatus(200);
            httpServletResponse.addHeader("redirectTo", "/issue/details/"+ issue.getIssueID());
        } catch (UserNotFoundException e) {
            ControllerHelper.manageErrorRedirectAttributes(null, CommonMessages.USER_NOT_FOUND_MESSAGE, ra, null);
            httpServletResponse.setStatus(404);
        } catch (ProjectNotFoundException e) {
            ControllerHelper.manageErrorRedirectAttributes(null, CommonMessages.PROJECT_NOT_FOUND_MESSAGE, ra, null);
            httpServletResponse.setStatus(404);
        } catch (ConstraintViolationException e) {
            httpServletResponse.sendError(400, ExceptionsHelper.getMsgFromConstraintViolationException(e));
            return;
        }


        if(!Ajax.isAjaxRequest(requestedWith)) {
            httpServletResponse.sendRedirect(onErrorRedirectUrl);
        }
    }
}
