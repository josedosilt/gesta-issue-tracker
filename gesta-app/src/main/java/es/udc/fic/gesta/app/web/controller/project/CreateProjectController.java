package es.udc.fic.gesta.app.web.controller.project;

import es.udc.fic.gesta.app.web.i18n.CommonMessages;
import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.exceptions.EntityNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.project.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.security.RolesAllowed;
import java.util.Locale;

@Controller
public class CreateProjectController {

    private static final String CREATE_PROJECT_VIEW = "project/create";
    private static final String CREATE_PROJECT_FRAGMENT = CREATE_PROJECT_VIEW + " :: createProjectFragment";

    @Autowired
    private ProjectService projectService;
    @Qualifier("messageSource")
    @Autowired
    private MessageSource messageSource;


    @GetMapping("/project/create")
    @RolesAllowed("ROLE_ADMIN")
    public String getCreateProjectPage(@RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                                   String requestedWith){
        if(Ajax.isAjaxRequest(requestedWith)){
            return CREATE_PROJECT_FRAGMENT;
        }
        return CREATE_PROJECT_VIEW;
    }

    @PostMapping("/project/create")
    @RolesAllowed("ROLE_ADMIN")
    public String createProject(Locale locale, RedirectAttributes ra,
                                @RequestParam(value="projectName", required = false) String projectName,
                                @RequestParam(value="projectID",required = false) String projectID,
                                @RequestParam(value="projectLeader",required = false) String projectLeaderEmail)
            throws OperationForbiddenException {

        try {
            projectService.createProject(projectID, projectName, projectLeaderEmail);
        } catch (EntityNotFoundException e) {
            ra.addFlashAttribute("error_msg", CommonMessages.USER_NOT_FOUND_MESSAGE);
            return "redirect:/project/create";
        }

        return "redirect:/project/details/"+ projectID;
    }
}
