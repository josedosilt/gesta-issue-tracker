package es.udc.fic.gesta.app.web.controller.timelog;

import es.udc.fic.gesta.app.web.exceptions.ExceptionsHelper;
import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.timelog.TimeLogService;
import es.udc.fic.gesta.model.service.exceptions.IssueNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.UserNotFoundException;
import es.udc.fic.gesta.model.util.form.TimeLogForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.security.Principal;

@Controller
public class LogTimeController {

    public static final String LOG_TIME_VIEW = "issue/log-time";

    public static final String LOG_TIME_FRAGMENT = LOG_TIME_VIEW + " :: logTimeFragment";

    @Autowired
    private TimeLogService timeLogService;

    @GetMapping("/issue/{id}/log-time")
    public String getLogTimeView(Model model, @PathVariable("id") String issueId,
                                 @ModelAttribute TimeLogForm timeLogForm,
                                 @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                             String requestedWith) {
        model.addAttribute("issueId", issueId);

        if(Ajax.isAjaxRequest(requestedWith)){
            return LOG_TIME_FRAGMENT;
        }
        return LOG_TIME_VIEW;
    }

    @PostMapping(value = "/issue/{id}/log-time", headers = Ajax.SPRING_MVC_HEADER_NO_AJAX)
    public String logTime(Principal principal, RedirectAttributes ra, @PathVariable("id") String issueId,
                          @ModelAttribute TimeLogForm timeLogForm) throws UserNotFoundException,
            IssueNotFoundException {

        try {
            timeLogService.logTime(principal.getName(), timeLogForm);
        }catch (ConstraintViolationException e){
            ra.addFlashAttribute("timeLogFormError", ExceptionsHelper.getMsgFromConstraintViolationException(e));
            return "redirect:/issue/"+issueId + "/log-time";
        }
        return "redirect:/issue/details/" + issueId;
    }

    @PostMapping(value = "/issue/{id}/log-time", headers = Ajax.SPRING_MVC_HEADER_AJAX)
    @ResponseStatus(HttpStatus.CREATED)
    public void logTimeAjax(HttpServletResponse hsr, Principal principal, @PathVariable("id") String issueId,
                            @ModelAttribute TimeLogForm timeLogForm) throws UserNotFoundException,
            IssueNotFoundException {

        try {
            timeLogService.logTime(principal.getName(), timeLogForm);
        } catch (ConstraintViolationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    ExceptionsHelper.getMsgFromConstraintViolationException(e));
        }
        hsr.setHeader("Location", "/issue/details/" + issueId);
    }
}
