package es.udc.fic.gesta.app.web.controller.issue;

import es.udc.fic.gesta.app.web.controller.issue.search.BaseIssueSearchController;
import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.exceptions.UserNotFoundException;
import es.udc.fic.gesta.model.util.form.SearchIssueForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Optional;

@Controller
public class SearchIssuesController extends BaseIssueSearchController {

    private static final String SEARCH_ISSUES_VIEW = "issue/search";

    public static final String SEARCH_RESULT_FRAGMENT = SEARCH_ISSUES_VIEW + " :: issueSearchResultFragment";

    @GetMapping(value = {"/issue/search", "/project/{id}/issues"})
    public String getSearchIssuesView(Model model, Principal principal, @ModelAttribute SearchIssueForm searchIssueForm,
                                      @RequestParam(value = "page", defaultValue = "1") Integer page,
                                      @PathVariable(value = "id", required = false) Optional<String> projectId,
                                      @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                              String requestedWith) throws UserNotFoundException {

        searchIssueForm.setProjectId(projectId.orElse(null));
        this.addSearchResult(model,searchIssueForm,page);
        if (Ajax.isAjaxRequest(requestedWith)) {
            return SEARCH_RESULT_FRAGMENT;
        }
        model.addAttribute("searchIssueForm", searchIssueForm);
        return SEARCH_ISSUES_VIEW;
    }

    @GetMapping(value = {"/issue/select", "/project/{id}/select-issue"})
    public String getSelectIssuesView(Model model, Principal principal, @ModelAttribute SearchIssueForm searchIssueForm,
                                      @RequestParam(value = "page", defaultValue = "1") Integer page,
                                      @PathVariable(value = "id", required = false) Optional<String> projectId,
                                      @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                              String requestedWith) throws UserNotFoundException {
        model.addAttribute("selectIssue", true);
        return this.getSearchIssuesView(model, principal, searchIssueForm, page, projectId, requestedWith);
    }

}
