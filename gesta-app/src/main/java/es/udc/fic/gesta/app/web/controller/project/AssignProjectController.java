package es.udc.fic.gesta.app.web.controller.project;

import es.udc.fic.gesta.app.web.controller.project.form.AssignProjectForm;
import es.udc.fic.gesta.app.web.i18n.CommonMessages;
import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.exceptions.EntityNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.project.ProjectService;
import es.udc.fic.gesta.model.service.project.exception.ProjectAlreadyAssignedException;
import es.udc.fic.gesta.model.service.project.exception.ProjectNotFoundException;
import es.udc.fic.gesta.model.service.user.UserService;
import es.udc.fic.gesta.app.web.controller.utils.ControllerHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Controller
public class AssignProjectController {

    private static final String ASSIGN_PROJECT_VIEW = "project/assign";
    private static final String ASSIGN_PROJECT_FRAGMENT = ASSIGN_PROJECT_VIEW + " :: assignProjectFragment";

    private static final String ALREADY_ASSIGNED_MESSAGE = "project.error.alreadyAssigned";

    @Autowired
    MessageSource messageSource;

    @Autowired
    private ProjectService projectService;
    @Autowired
    private UserService userService;

    @GetMapping(value = {"/project/assign/{projectId}", "/project/assign"})
    public String getAssignProjectView(Model model, @ModelAttribute AssignProjectForm assignProjectForm,
                                       @PathVariable("projectId") Optional<String> projectId,
                                       RedirectAttributes ra,
                                       @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                                   String requestedWith) throws OperationForbiddenException {
        if(projectId.isPresent()){
            try {
                projectService.getProjectDetails(projectId.get());
            } catch (EntityNotFoundException e) {
                ra.addAttribute("returnedError", CommonMessages.PROJECT_NOT_FOUND_MESSAGE);
                return "redirect:/error";
            }
        }
        model.addAttribute("readOnlyProjectID", projectId.isPresent());
        assignProjectForm.setProjectID((projectId.isPresent()) ? projectId.get() : assignProjectForm.getProjectID());
        model.addAttribute("assignProjectForm", assignProjectForm);

        if (!(assignProjectForm.getAssignTo() == null || assignProjectForm.getAssignTo().isEmpty())) {
            model.addAttribute("selectedUsers", this.userService.findUsersById(assignProjectForm.getAssignTo()));
        }
        return Ajax.isAjaxRequest(requestedWith) ? ASSIGN_PROJECT_FRAGMENT : ASSIGN_PROJECT_VIEW;
    }

    @PostMapping(value = {"/project/assign/{projectId}", "/project/assign"})
    public String assignProject(@ModelAttribute AssignProjectForm assignProjectForm,
                                @PathVariable Optional<String> projectId, RedirectAttributes ra) throws
            OperationForbiddenException {
        String onError = "redirect:/project/assign";
        Map<String, Object> onErrorAttributes = new HashMap<>();
        onErrorAttributes.put("assignTo", assignProjectForm.getAssignTo());
        onErrorAttributes.put("projectID", assignProjectForm.getProjectID());
        if(projectId.isPresent()){
            onError += "/" + projectId.get();
        }
        try {
            projectService.assignProject(assignProjectForm.getProjectID(), assignProjectForm.getAssignTo());
        } catch (ProjectAlreadyAssignedException e) {
            return  ControllerHelper.manageErrorRedirectAttributes(onError, ALREADY_ASSIGNED_MESSAGE, ra, onErrorAttributes);
        } catch (ProjectNotFoundException e) {
            return  ControllerHelper
                    .manageErrorRedirectAttributes(onError, CommonMessages.PROJECT_NOT_FOUND_MESSAGE, ra, onErrorAttributes);
        } catch(EntityNotFoundException e){
            return ControllerHelper.manageErrorRedirectAttributes(onError, CommonMessages.ERROR_UNKNOWN, ra, onErrorAttributes);
        }

        return "redirect:/project/details/" + assignProjectForm.getProjectID();
    }

}
