package es.udc.fic.gesta.app.web.controller.issue;

import es.udc.fic.gesta.app.web.exceptions.ExceptionsHelper;
import es.udc.fic.gesta.app.web.i18n.CommonMessages;
import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.exceptions.IssueNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.UserNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.issue.IssueDetails;
import es.udc.fic.gesta.model.service.issue.IssueService;
import es.udc.fic.gesta.model.util.IssueForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Locale;

@Controller
public class EditIssueController {

    private static final String EDIT_ISSUE_VIEW = "issue/edit";

    @Autowired
    private IssueService issueService;

    @Autowired
    private MessageSource messageSource;

    private static final void adaptIssueForm(IssueForm issueForm, IssueDetails issueDetails){
        issueForm.setSummary(issueDetails.getSummary());
        issueForm.setAssignedTo(issueDetails.getAssignedUser().getId());
        issueForm.setIssueType(issueDetails.getIssueType());
        issueForm.setPriority(issueDetails.getPriority());
        issueForm.setDescription(issueDetails.getDescription());
        if (issueDetails.getDevelopmentCycleDTO() != null) {
            issueForm.setDevelopmentCycle(issueDetails.getDevelopmentCycleDTO().name);
        }
        issueForm.setProjectID(issueDetails.getProjectID());
        issueForm.setAssignedTo(issueDetails.getAssignedUser().getId());
    }

    @GetMapping("/issue/edit/{id}")
    public String getEditIssueView(Model model, @PathVariable("id") String issueId,
                                   @ModelAttribute IssueForm issueForm,
                                   @ModelAttribute("returnedError") String returnedError,
                                   @RequestHeader(value = "X-Requested-With", required = false)
                                           String requestedWith) throws IssueNotFoundException,
            OperationForbiddenException {

        IssueDetails issueDetails = issueService.getIssueDetails(issueId);
        adaptIssueForm(issueForm, issueDetails);

        model.addAttribute("assignedUser", issueDetails.getAssignedUser());
        model.addAttribute("issueId", issueId);
        model.addAttribute("issueForm", issueForm);

        if (Ajax.isAjaxRequest(requestedWith)) {
            model.addAttribute("hideFormLegend", true);
            return EDIT_ISSUE_VIEW + " :: editIssueForm";
        }
        return EDIT_ISSUE_VIEW;
    }

    @PostMapping("/issue/edit/{id}")
    public String getEditIssueView(HttpServletResponse hsr, RedirectAttributes ra, Locale locale,
                                   @PathVariable("id") String issueId, @ModelAttribute IssueForm issueForm,
                                   @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                             String requestedWidth) throws IOException, IssueNotFoundException,
            URISyntaxException, OperationForbiddenException {
        String redirectTo = "redirect:/issue/details/" + issueId;
        String errorMsg = null;
        try {
            issueService.editIssue(issueForm, issueId);
        } catch (UserNotFoundException e) {
            errorMsg = messageSource.getMessage(CommonMessages.USER_NOT_FOUND_MESSAGE, null, locale);
        } catch (ConstraintViolationException e){
            errorMsg = ExceptionsHelper.getMsgFromConstraintViolationException(e);
        }

;
        if(errorMsg != null){
            if(Ajax.isAjaxRequest(requestedWidth)){
                hsr.sendError(400, errorMsg);
                return null;
            }
            ra.addFlashAttribute("returnedError", errorMsg);
            redirectTo = "redirect:/issue/edit/" + issueId;
        }
        return redirectTo;
    }

}
