package es.udc.fic.gesta.app.web.controller.error;

import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;



public class CustomErrorController {

    @RequestMapping(value = "/error")
    public String handleError(HttpServletRequest request) {
        //do something like logging
        return "error";
    }
}
