package es.udc.fic.gesta.app.web.exceptions;

/**
 *  Exceptions to use when a user has to input the password but doesn't match with the current user password
 */
public class InvalidOldPasswordException extends Exception {
}
