package es.udc.fic.gesta.app.web.controller.project;

import es.udc.fic.gesta.app.web.i18n.CommonMessages;
import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.exceptions.EntityNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.ProjectNotAssignedException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.exceptions.cycle.EmptyOrNullNameException;
import es.udc.fic.gesta.model.service.exceptions.cycle.NameAlreadyExistsForProjectException;
import es.udc.fic.gesta.model.service.project.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.Locale;

@Controller
public class CreateCycleController {

    public static final String CREATE_CYCLE_VIEW = "project/create-cycle";
    public static final String CREATE_CYCLE_FRAGMENT = CREATE_CYCLE_VIEW + " :: createCycleFragment";

    @Autowired
    private ProjectService projectService;
    @Autowired
    private MessageSource messageSource;

    @GetMapping("/project/{id}/create-cycle")
    public String getCreateCycleView(Model model,
                                     @RequestParam(value = "cycle-name", required = false) String cycleName,
                                     @PathVariable("id") String projectId,
                                     @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                             String requestedWith) throws EntityNotFoundException {
        if(cycleName != null){
            model.addAttribute("cycleName", cycleName);
        }
        model.addAttribute("projectId", projectId);
        if(Ajax.isAjaxRequest(requestedWith)){
            return CREATE_CYCLE_FRAGMENT;
        }
        return CREATE_CYCLE_VIEW;
    }

    @PostMapping("/project/{id}/create-cycle")
    @ResponseStatus(HttpStatus.OK)
    public void createCycle(HttpServletResponse hsr, Principal principal, Locale locale,
                            @RequestParam(value = "cycle-name") String cycleName,
                            @PathVariable("id") String projectId,
                            @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                    String requestedWith) throws EntityNotFoundException, IOException,
            ProjectNotAssignedException, OperationForbiddenException {

        try {
            projectService.createDevelopmentCycle(principal.getName(), projectId, cycleName);
        } catch (EmptyOrNullNameException | NameAlreadyExistsForProjectException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    getMessageForException(this.messageSource, e, locale));
        }

        if (!Ajax.isAjaxRequest(requestedWith)) {
            hsr.sendRedirect("/project/details/" + projectId);
        }else{
            hsr.setHeader("Location", "/project/details/" + projectId);
        }

    }

    private static String getMessageForException(MessageSource messageSource, Exception e, Locale locale){
        String msg;
        if(e instanceof EmptyOrNullNameException){
            msg= CommonMessages.NULL_OR_EMPTY_CYCLE_NAME;
        }else if(e instanceof NameAlreadyExistsForProjectException){
            msg = CommonMessages.CYCLE_NAME_ALREADY_EXIST;
        }else{
            msg = CommonMessages.ERROR_UNKNOWN;
        }


        return messageSource.getMessage(msg, null, locale);
    }
}
