package es.udc.fic.gesta.app.web.controller.user;

import es.udc.fic.gesta.app.web.controller.user.form.UserSearchForm;
import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.user.UserProfileDTO;
import es.udc.fic.gesta.model.service.user.UserService;
import es.udc.fic.gesta.model.service.utils.SearchExclusions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class SearchUserController {

    private static final String SEARCH_USER_VIEW = "user/search.html";
    private static final int MAX_USER_PER_PAGE = 10;

    @Autowired
    private UserService userService;

    @GetMapping("user/search")
    public String getSearchUserPage(Principal principal, Model model,
                                    @RequestParam(defaultValue = "false", name = "selectUser") boolean selectUser,
                                    @RequestHeader(value = "X-Requested-With", required = false) String requestedWith,
                                    @RequestParam(required = false, name = "page", defaultValue = "1") Integer page,
                                    @RequestParam(required = false, name = "email") String email,
                                    @RequestParam(required = false, name = "fullName") String fullName,
                                    @RequestParam(required = false, name = "projectId") String projectId) {
        String view = SEARCH_USER_VIEW;
        model.addAttribute("selectUser", selectUser);
        if (Ajax.isAjaxRequest(requestedWith)) {
            view = view.concat(" :: userSearchForm");
        }

        // Even if we enter the page of the search, we still return the results of a search with no filters.
        this.findUsers(model, email, fullName, projectId, null, page.intValue() - 1, MAX_USER_PER_PAGE, true);

        // Set the values of the last search on the form;
        UserSearchForm searchForm = new UserSearchForm();
        searchForm.setEmail(email);
        searchForm.setName(fullName);
        searchForm.setProjectId(projectId);
        model.addAttribute("userSearchForm", searchForm);
        return view;
    }

    @GetMapping("/project/{projectId}/users")
    public String getSearchUserViewByProject(Model model,
                                             @RequestParam(defaultValue = "false", name = "selectUser")
                                                     boolean selectUser,
                                             @RequestHeader(value = "X-Requested-With", required = false)
                                                     String requestedWith,
                                             @RequestParam(required = false, name = "page", defaultValue = "1")
                                                     Integer page,
                                             @RequestParam(required = false, name = "email") String email,
                                             @RequestParam(required = false, name = "fullName") String fullName,
                                             @PathVariable("projectId") String projectId) {
        model.addAttribute("blockProjectFilter", true);
        return this.getSearchUserPage(null, model, selectUser, requestedWith, page, email, fullName, projectId);
    }

    @GetMapping("user/searchResult")
    public String getSearchResult(Model model,
                                  @RequestParam(defaultValue = "false", name = "selectUser") boolean selectUser,
                                  @RequestParam(required = false, name = "page", defaultValue = "1") Integer page,
                                  @RequestParam(required = false, name = "email") String email,
                                  @RequestParam(required = false, name = "fullName") String fullName,
                                  @RequestParam(required = false, name = "projectId") String projectId) {

        model.addAttribute("selectUser", selectUser);
        this.findUsers(model, email, fullName, projectId, null, page - 1, MAX_USER_PER_PAGE, true);
        return SEARCH_USER_VIEW + " :: userSearchResult";
    }

    /**
     * Searchs users and sets the result in a Model.
     *
     * @param model            in which the result will be set
     * @param email
     * @param fullName
     * @param projectID
     * @param searchExclusions
     * @param pageNumber
     * @param maxResultNumber
     * @param isAnd
     */
    private void findUsers(Model model, String email, String fullName, String projectID,
                           SearchExclusions searchExclusions, int pageNumber, int maxResultNumber, boolean isAnd) {
        Page<UserProfileDTO> result =
                userService.findUsers(email, fullName, projectID, searchExclusions, pageNumber, maxResultNumber, isAnd);
        model.addAttribute("resultPage", result);
        if (result.getTotalPages() > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, result.getTotalPages())
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
    }

    @GetMapping("/user/getUsers")
    @ResponseBody
    public List<UserProfileDTO> getUsers(@RequestParam(name = "user", required = false) String user,
                                         @RequestParam(name = "project", required = false) String projectId,
                                         @RequestParam(required = false, name = "page", defaultValue = "1")
                                                 Integer page) {
        return userService.findUsers(user, user, projectId, null, page - 1, MAX_USER_PER_PAGE, false).getContent();
    }
}
