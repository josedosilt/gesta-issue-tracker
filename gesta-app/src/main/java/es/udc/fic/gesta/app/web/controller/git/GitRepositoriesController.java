package es.udc.fic.gesta.app.web.controller.git;

import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.git.GitLabRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.security.RolesAllowed;

@Controller
public class GitRepositoriesController {

    private static final String REPOSITORIES_VIEW = "git/repositories";

    private static final String REPOSITORIES_FRAGMENT = REPOSITORIES_VIEW + " :: repositoriesFragment";

    @Autowired
    private GitLabRepoService gitLabRepoService;

    @GetMapping("/git/repositories")
    @RolesAllowed("ROLE_ADMIN")
    public String getRepositoriesView(Model model,
                                      @RequestParam(value = "select", defaultValue = "false") boolean select,
                                      @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                              String requestedWith) {
        model.addAttribute("isSelect", select);
        model.addAttribute("repositoryDTOS", gitLabRepoService.findRepositories());
        if(Ajax.isAjaxRequest(requestedWith)){
            return REPOSITORIES_FRAGMENT;
        }
        return REPOSITORIES_VIEW;
    }

}
