package es.udc.fic.gesta.app.web.controller.project;

import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.dto.DevelopmentCycleDTO;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.project.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class SearchCycleController {

    private static final String SEARCH_CYCLE_VIEW = "project/search-cycle";

    private static final String SEARCH_CYCLE_FRAGMENT = SEARCH_CYCLE_VIEW + " :: searchCycleFragment";

    private static final String SEARCH_CYCLE_RESULT_FRAGMENT = SEARCH_CYCLE_VIEW + " :: searchCycleResultFragment";

    @Autowired
    private ProjectService projectService;

    @GetMapping("project/{id}/cycles")
    public String getSearchCycleView(Model model, @PathVariable("id") String projectId,
                                     @RequestParam(value = "cycle", required = false) String cycleName,
                                     @RequestParam(value = "page", defaultValue = "1") Integer page,
                                     @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                             String requestedWith) throws OperationForbiddenException {

        model.addAttribute("searchCycleResult",
                projectService.searchDevelopmentCycles(projectId, cycleName, page - 1, 10));
        model.addAttribute("projectId", projectId);
        if (Ajax.isAjaxRequest(requestedWith)) {
            return SEARCH_CYCLE_FRAGMENT;
        }
        return SEARCH_CYCLE_VIEW;
    }

    @GetMapping("project/{id}/searchCycleResult")
    public String getSearchCycleResult(Model model, @PathVariable("id") String projectId,
                                       @RequestParam(value = "cycle", required = false) String cycleName,
                                       @RequestParam(value = "page", defaultValue = "1") Integer page) throws
            OperationForbiddenException {
        model.addAttribute("searchCycleResult",
                projectService.searchDevelopmentCycles(projectId, cycleName, page - 1, 10));
        model.addAttribute("projectId", projectId);
        return SEARCH_CYCLE_RESULT_FRAGMENT;
    }

    @GetMapping("project/{id}/cyclesResult")
    @ResponseBody
    public List<DevelopmentCycleDTO> getSearchCycleResultNames(@PathVariable("id") String projectId,
                                                               @RequestParam(value = "cycle", required = false)
                                                                       String cycleName,
                                                               @RequestParam(value = "page", defaultValue = "1")
                                                                       Integer page) throws
            OperationForbiddenException {
        return projectService.searchDevelopmentCycles(projectId, cycleName, page - 1, 10).getContent();
    }
}
