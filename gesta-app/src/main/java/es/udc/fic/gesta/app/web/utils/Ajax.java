package es.udc.fic.gesta.app.web.utils;

public class Ajax {

    public static final String REQUESTED_WITH_HEADER = "X-Requested-With";

    public static final String SPRING_MVC_HEADER_AJAX = "X-Requested-With=XMLHttpRequest";

    public static final String SPRING_MVC_HEADER_NO_AJAX = "X-Requested-With!=XMLHttpRequest";

    private Ajax() {
    }

    public static boolean isAjaxRequest(String requestedWith) {
        return requestedWith != null ? "XMLHttpRequest".equals(requestedWith) : false;
    }
}

