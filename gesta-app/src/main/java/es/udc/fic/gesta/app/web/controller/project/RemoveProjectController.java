package es.udc.fic.gesta.app.web.controller.project;

import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.exceptions.EntityNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.project.ProjectDetails;
import es.udc.fic.gesta.model.service.project.ProjectService;
import es.udc.fic.gesta.model.service.project.exception.ProjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class RemoveProjectController {

    private static final String REMOVE_PROJECT_VIEW = "project/remove";

    @Autowired
    private ProjectService projectService;

    @GetMapping("/project/remove/{projectId}")
    @RolesAllowed("ROLE_ADMIN")
    public String getRemoveProjectView(Model model, @PathVariable("projectId") String projectId,
                                       @RequestHeader(value = "X-Requested-With", required = false)
                                               String requestedWith)
            throws EntityNotFoundException, OperationForbiddenException {

        ProjectDetails project = projectService.getProjectDetails(projectId);
        model.addAttribute("projectDetails", project);

        if(Ajax.isAjaxRequest(requestedWith)){
            return REMOVE_PROJECT_VIEW + " :: removeProjectFragment";
        }
        return REMOVE_PROJECT_VIEW;
    }

    @PostMapping("/project/remove/{projectId}")
    @RolesAllowed("ROLE_ADMIN")
    public void removeProject(HttpServletResponse httpServletResponse, @PathVariable("projectId") String projectId,
                              @RequestHeader(value = "X-Requested-With", required = false) String requestedWith)
            throws IOException, ProjectNotFoundException, OperationForbiddenException {
        try {
            projectService.removeProject(projectId);
            httpServletResponse.sendRedirect("/");
        } catch (ProjectNotFoundException e) {
            if (Ajax.isAjaxRequest(requestedWith)) {
                httpServletResponse.setStatus(404);
            } else {
                throw e;
            }
        }
    }

}
