package es.udc.fic.gesta.app.web.controller.git;

import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.exceptions.IssueNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.git.GitLabRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

@Controller
public class IssueCommitsController {

    private static final String ISSUE_COMMITS_VIEW = "git/issue-commits";

    private static final String ISSUE_COMMITS_FRAGMENT = ISSUE_COMMITS_VIEW + " :: issueCommitsFragment";

    @Autowired
    private GitLabRepoService gitService;

    @GetMapping("/issue/{issue}/commits")
    public String getIssueCommitsView(Model model, @PathVariable("issue") String issueId,
                                      @RequestHeader(name = Ajax.REQUESTED_WITH_HEADER, required = false)
                                              String requestedWith) throws IssueNotFoundException,
            OperationForbiddenException {
        model.addAttribute("issueCommitDTOS", gitService.findIssueCommits(issueId));
        if (Ajax.isAjaxRequest(requestedWith)) {
            return ISSUE_COMMITS_FRAGMENT;
        }
        return ISSUE_COMMITS_VIEW;
    }
}
