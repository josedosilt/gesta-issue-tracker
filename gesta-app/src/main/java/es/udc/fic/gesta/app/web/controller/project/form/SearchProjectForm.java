package es.udc.fic.gesta.app.web.controller.project.form;

/**
 * A form to use for searching projects
 */
public class SearchProjectForm {

    private String projectNameOrID;

    private String projectLeaderName;

    /**
     * Gets the project Name Or ID included in this form
     *
     * @return project name or id
     */
    public String getProjectNameOrID() {
        return projectNameOrID;
    }

    /**
     * Sets project name or id.
     *
     * @param projectNameOrID the project name or id
     */
    public void setProjectNameOrID(String projectNameOrID) {
        this.projectNameOrID = projectNameOrID;
    }

    /**
     * Gets project leader name.
     *
     * @return the project leader name
     */
    public String getProjectLeaderName() {
        return projectLeaderName;
    }

    /**
     * Sets project leader name.
     *
     * @param projectLeaderName the project leader name
     */
    public void setProjectLeaderName(String projectLeaderName) {
        this.projectLeaderName = projectLeaderName;
    }
}
