package es.udc.fic.gesta.app.web.controller.project;

import es.udc.fic.gesta.app.web.i18n.CommonMessages;
import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.exceptions.EntityNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.UserNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.project.ProjectDetails;
import es.udc.fic.gesta.model.service.project.ProjectService;
import es.udc.fic.gesta.model.service.project.exception.ProjectNotFoundException;
import es.udc.fic.gesta.model.service.utils.ValidatingService;
import es.udc.fic.gesta.model.util.form.ProjectForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.util.Locale;

@Controller
public class EditProjectController {

    public static final String EDIT_PROJECT_VIEW = "project/edit";

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ValidatingService validatingService;

    @Autowired
    private MessageSource messageSource;


    @GetMapping("/project/edit/{id}")
    @RolesAllowed("ROLE_ADMIN")
    public String getEditProjectView(Model model, @ModelAttribute ProjectForm projectForm,
                                     @PathVariable("id") String projectId,
                                     @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                             String requestedWith)
            throws EntityNotFoundException, OperationForbiddenException {


        ProjectDetails projectDetails = projectService.getProjectDetails(projectId);
        projectForm.setProjectLeaderID(projectDetails.getProjectLeader().getId());
        projectForm.setProjectName(projectDetails.getProjectName());
        projectForm.setProjectState(projectDetails.getProjectState());

        model.addAttribute("projectDetails", projectDetails);
        model.addAttribute("projectForm", projectForm);
        if(Ajax.isAjaxRequest(requestedWith)){
            model.addAttribute("dontShowFormLegend", true);
            return EDIT_PROJECT_VIEW + " :: editProjectForm";
        }
        return EDIT_PROJECT_VIEW;
    }

    @PostMapping("/project/edit/{id}")
    @RolesAllowed("ROLE_ADMIN")
    public void editProject(HttpServletResponse httpServletResponse, Locale locale,
                            @ModelAttribute ProjectForm projectForm, @PathVariable("id") String projectId,
                            @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                    String requestedWitdh) throws IOException, ProjectNotFoundException,
            OperationForbiddenException {
        try {
            validatingService.validateProjectForm(projectForm);
            projectService.editProject(projectForm, projectId);
            if (!Ajax.isAjaxRequest(requestedWitdh)) {
                httpServletResponse.sendRedirect("/project/details/" + projectId);
            } else {
                httpServletResponse.setStatus(200);
            }
        } catch (ConstraintViolationException e) {
            String msg = "";
            for(ConstraintViolation cv : e.getConstraintViolations()){
                msg = cv.getMessage();
            }
            httpServletResponse.sendError(400, msg);
        } catch (UserNotFoundException e) {

            httpServletResponse
                    .sendError(400, messageSource.getMessage(CommonMessages.USER_NOT_FOUND_MESSAGE, null, locale));
        }

    }
}
