package es.udc.fic.gesta.app.web.controller.user;

import es.udc.fic.gesta.app.web.i18n.CommonMessages;
import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.exceptions.EmailAlreadyInUseException;
import es.udc.fic.gesta.model.service.user.UserProfileDTO;
import es.udc.fic.gesta.model.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.security.RolesAllowed;
import java.util.Locale;

@Controller
public class CreateUserController {

    private static final String CREATE_USER_VIEW = "user/create";
    private static final String CREATE_USER_FRAGMENT = CREATE_USER_VIEW + " :: createUserFragment";

    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;

    @GetMapping("/user/create")
    @RolesAllowed("ROLE_ADMIN")
    public String getCreateUserView(@RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                                String requestedWith) {
        if(Ajax.isAjaxRequest(requestedWith)){
            return CREATE_USER_FRAGMENT;
        }
        return CREATE_USER_VIEW;
    }

    @PostMapping(value = "/user/create", headers = Ajax.SPRING_MVC_HEADER_NO_AJAX)
    @RolesAllowed("ROLE_ADMIN")
    public String createUser(RedirectAttributes ra, Locale locale,
                           @RequestParam("email") String email, @RequestParam("password") String password,
                           @RequestParam("confirmPassword") String confirmPassword,
                           @RequestParam("fullName") String fullName) {
        UserProfileDTO user;
        try {
            user = commonCreateUser(email, password, confirmPassword, fullName, locale);
        }catch (ResponseStatusException e){
            ra.addFlashAttribute("returnedError", e.getReason());
            return "redirect:/user/create";
        }
        return "redirect:/profile/" + user.getId();
    }

    @PostMapping(value = "/user/create", headers = Ajax.SPRING_MVC_HEADER_AJAX)
    @RolesAllowed("ROLE_ADMIN")
    public void createUserAjax(Locale locale, @RequestParam("email") String email,
                               @RequestParam("password") String password,
                               @RequestParam("confirmPassword") String confirmPassword,
                               @RequestParam("fullName") String fullName) {
            commonCreateUser(email, password, confirmPassword, fullName, locale);
    }

    private UserProfileDTO commonCreateUser(String email, String password, String confirmPassword, String fullName,
                                            Locale locale) {
        if (!password.equals(confirmPassword)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    messageSource.getMessage(CommonMessages.ERROR_PASSWORD_NOT_MATCH, null, locale));
        }
        UserProfileDTO user = null;
        try {
            user = userService.createUser(email, password, fullName);
        } catch (EmailAlreadyInUseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    messageSource.getMessage(CommonMessages.ERROR_EMAIL_ALREADY_IN_USE, null, locale));
        }
        return user;
    }
}
