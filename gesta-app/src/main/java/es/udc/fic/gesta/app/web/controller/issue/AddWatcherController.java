package es.udc.fic.gesta.app.web.controller.issue;

import es.udc.fic.gesta.app.web.i18n.CommonMessages;
import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.exceptions.IssueNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.UserNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.issue.IssueService;
import es.udc.fic.gesta.model.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Locale;
import java.util.Set;

@Controller
public class AddWatcherController {

    private static final String ADD_WATCHER_VIEW = "issue/add-watcher";

    private static final String ADD_WATCHER_FRAGMENT = ADD_WATCHER_VIEW + " :: addWatcherFragment";

    @Autowired
    private UserService userService;

    @Autowired
    private IssueService issueService;

    @Autowired
    private MessageSource messageSource;

    @GetMapping("/issue/{id}/add-watcher")
    public String getAddWatcherView(Model model, @PathVariable("id") String issueId,
                                    @RequestParam(value = "watchers", required = false) List<Long> watchers,
                                    @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                                String requestedWith) throws IssueNotFoundException,
            OperationForbiddenException {

        model.addAttribute("issueDetails", issueService.getIssueDetails(issueId));
        if(!(watchers == null || watchers.isEmpty())){
            model.addAttribute("addedWatchers", userService.findUsersById(watchers));
        }
        if(Ajax.isAjaxRequest(requestedWith)){
            model.addAttribute("isAjaxCall", true);
            return ADD_WATCHER_FRAGMENT;
        }
        return ADD_WATCHER_VIEW;
    }

    @PostMapping(value = "/issue/{id}/add-watcher", headers = Ajax.SPRING_MVC_HEADER_NO_AJAX)
    public String addWatcher(RedirectAttributes ra, Locale locale, @PathVariable("id") String issueId,
                             @RequestParam(value = "watchers", required = false) Set<Long> watchers) throws
            IssueNotFoundException, OperationForbiddenException {
        String onErrorRedirect = "redirect:/issue/" + issueId + "/add-watcher";
        if(watchers == null || watchers.isEmpty()){
            ra.addFlashAttribute("returnedError",
                    messageSource.getMessage(CommonMessages.EMPTY_USER_LIST_ERROR, null, locale));
            return onErrorRedirect;
        }

        try {
            issueService.addWatchers(issueId, watchers);
        } catch (UserNotFoundException e) {
            ra.addFlashAttribute("returnedError",
                    messageSource.getMessage(CommonMessages.USER_NOT_FOUND_MESSAGE, null, locale));
            ra.addAttribute("watchers", watchers);
            return onErrorRedirect;
        }

        return "redirect:/issue/details/" + issueId;
    }

    @PostMapping(value = "/issue/{id}/add-watcher", headers = Ajax.SPRING_MVC_HEADER_AJAX)
    @ResponseStatus(value = HttpStatus.OK)
    public void addWatcherAjax(Locale locale, @PathVariable("id") String issueId,
                             @RequestParam(value = "watchers", required = false) Set<Long> watchers) throws
            IssueNotFoundException, OperationForbiddenException {

        if(watchers == null || watchers.isEmpty()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    messageSource.getMessage(CommonMessages.EMPTY_USER_LIST_ERROR, null, locale));
        }

        try {
            issueService.addWatchers(issueId, watchers);
        } catch (UserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    messageSource.getMessage(CommonMessages.USER_NOT_FOUND_MESSAGE, null, locale));
        }
    }
}
