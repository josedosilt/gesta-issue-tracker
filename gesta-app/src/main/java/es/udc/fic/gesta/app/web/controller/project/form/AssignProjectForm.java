package es.udc.fic.gesta.app.web.controller.project.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that represent the form to assign projects.
 */
public class AssignProjectForm {

    @NotNull
    private String projectID;

    @NotNull
    @NotEmpty
    private List<Long> assignTo;

    public AssignProjectForm(){
        assignTo = new ArrayList<>();
    }

    /**
     * Gets project id.
     *
     * @return the project id
     */
    public String getProjectID() {
        return projectID;
    }

    /**
     * Sets project id.
     *
     * @param projectID the project id
     */
    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    /**
     * Gets user i ds.
     *
     * @return the user i ds
     */
    public List<Long> getAssignTo() {
        return assignTo;
    }

    /**
     * Sets user i ds.
     *
     * @param assignTo the user i ds
     */
    public void setAssignTo(List<Long> assignTo) {
        this.assignTo = assignTo;
    }
}
