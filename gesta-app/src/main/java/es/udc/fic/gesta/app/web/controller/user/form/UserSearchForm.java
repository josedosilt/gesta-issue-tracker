package es.udc.fic.gesta.app.web.controller.user.form;

/**
 * Class that represents a basic Search Form for Users
 */
public class UserSearchForm {

    String email = "";

    String name = "";

    String projectId = "";

    /**
     * Gets the email
     *
     * @return The value of the email that it's set to this form
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the name
     *
     * @return The value of the name of the users that is set in this form
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the project id
     *
     * @return the value of the project ID that is set in this form
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * Sets the value of the project ID
     *
     * @param projectId
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }
}
