package es.udc.fic.gesta.app.web.controller.issue;

import es.udc.fic.gesta.app.web.controller.issue.search.BaseIssueSearchController;
import es.udc.fic.gesta.app.web.i18n.CommonMessages;
import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.exceptions.IssueNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.issue.IssueDetails;
import es.udc.fic.gesta.model.util.form.SearchIssueForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Locale;

@Controller
public class LinkIssuesController extends BaseIssueSearchController {

    private static final String LINK_ISSUES_VIEW = "issue/link-issues";

    private static final String LINK_ISSUES_FRAGMENT = LINK_ISSUES_VIEW + " :: linkIssuesFragment";

    private static final String LINK_ISSUES_RESULT_FRAGMENT = LINK_ISSUES_VIEW + " :: linkIssuesResultFragment";

    private static final String RELATED_ISSUES_VIEW = "issue/related-issues";

    private static final String RELATED_ISSUES_FRAGMENT = RELATED_ISSUES_VIEW + " :: relatedIssuesFragment";

    @Autowired
    private MessageSource messageSource;

    @GetMapping("/issue/{issue}/link")
    public String getLinkIssuesView(Model model,
                                    @PathVariable("issue") String issueId,
                                    @ModelAttribute SearchIssueForm searchIssueForm,
                                    @RequestParam(value = "page", defaultValue = "1") Integer page,
                                    @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                                      String requestedWith) throws IssueNotFoundException,
            OperationForbiddenException {
        IssueDetails issueDetails = this.issueService.getIssueDetails(issueId);
        searchIssueForm.setProjectId(issueDetails.getProjectID());
        model.addAttribute("issueDetails", issueDetails);
        model.addAttribute("disableIssueSearchAjax", true);
        this.addSearchResult(model,searchIssueForm,page);
        if (Ajax.isAjaxRequest(requestedWith)) {
            return LINK_ISSUES_FRAGMENT;
        }
        model.addAttribute("searchIssueForm", searchIssueForm);
        return LINK_ISSUES_VIEW;
    }

    @GetMapping("/issue/{issueId}/link/issueResult")
    public String getLinkIssuesResultFragment(Model model,
                                              @PathVariable("issueId") String issueId,
                                              @ModelAttribute SearchIssueForm searchIssueForm,
                                              @RequestParam(value = "page", defaultValue = "1") Integer page) throws
            IssueNotFoundException, OperationForbiddenException {
        model.addAttribute("issueDetails", this.issueService.getIssueDetails(issueId));
        this.addSearchResult(model,searchIssueForm,page);
        return LINK_ISSUES_RESULT_FRAGMENT;
    }

    @PostMapping("/issue/{issueId}/link")
    public String linkIssues(Locale locale,
                             @PathVariable("issueId") String issueId,
                             @RequestParam("issue") String issueToLink){
        try {
            this.issueService.addRelatedIssue(issueId,issueToLink);
        } catch (IssueNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    this.messageSource.getMessage(CommonMessages.ISSUE_NOT_FOUND_MESSAGE, null, locale));
        } catch (OperationForbiddenException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    this.messageSource.getMessage(CommonMessages.OPERATION_FORBIDDEN, null, locale));
        }

        return "redirect:/issue/details/" + issueId;
    }

    @GetMapping("/issue/{issue}/linked-issues")
    public String getLinkedIssuesView(Model model,
                                      @PathVariable("issue") String issueId,
                                      @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                                  String requestedWith) throws IssueNotFoundException,
            OperationForbiddenException {
        model.addAttribute("relatedIssuesDTO", this.issueService.getRelatedIssues(issueId));
        if(Ajax.isAjaxRequest(requestedWith)){
            return RELATED_ISSUES_FRAGMENT;
        }
        return RELATED_ISSUES_VIEW;
    }

}
