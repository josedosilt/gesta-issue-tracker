package es.udc.fic.gesta.app.web.controller.signin;

import es.udc.fic.gesta.model.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SignInController {

    @Autowired
    private UserService userService;

    @GetMapping("/signin")
    public String signin(){

        return "signin/signin.html";
    }

}
