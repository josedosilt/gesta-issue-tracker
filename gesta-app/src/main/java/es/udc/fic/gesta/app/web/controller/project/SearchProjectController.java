package es.udc.fic.gesta.app.web.controller.project;

import es.udc.fic.gesta.app.web.controller.project.form.SearchProjectForm;
import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.app.web.utils.SelectType;
import es.udc.fic.gesta.model.service.project.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SearchProjectController {

    private static final String SEARCH_PROJECTS_VIEW = "project/search";

    @Autowired
    private ProjectService projectService;

    @GetMapping("/project/search")
    public String getSearchProjectsView(Model model,
                                        @RequestHeader(value = "X-Requested-With", required = false)
                                                String requestedWith,
                                        @ModelAttribute("searchProjectForm") SearchProjectForm searchProjectForm,
                                        @RequestParam(name = "page", defaultValue = "1") Integer page) {
        String view = SEARCH_PROJECTS_VIEW;

        if(Ajax.isAjaxRequest(requestedWith)){
            view = view + " :: searchProjectsFragment";
        }
        model.addAttribute("searchProjectForm", searchProjectForm);
        model.addAttribute("searchProjectPage", projectService
                .findProjects(searchProjectForm.getProjectNameOrID(), searchProjectForm.getProjectLeaderName(),
                        null, page - 1, 10));
        return view;
    }

    @GetMapping("/project/searchResult")
    public String getSearchProjectsResultsView(Model model,
                                               @ModelAttribute("searchProjectForm") SearchProjectForm searchProjectForm,
                                               @RequestParam(name = "page", defaultValue = "1") Integer page){
        model.addAttribute("searchProjectPage", projectService
                .findProjects(searchProjectForm.getProjectNameOrID(), searchProjectForm.getProjectLeaderName(),
                        null, page - 1, 10));
        return SEARCH_PROJECTS_VIEW + " :: searchProjectResultFragment";
    }


    @GetMapping("/project/select")
    public String getSelectProjectView(Model model){
        model.addAttribute("selectType", SelectType.SINGLE);
        return this.getSearchProjectsView(model, "XMLHttpRequest", new SearchProjectForm(), 1);
    }
}
