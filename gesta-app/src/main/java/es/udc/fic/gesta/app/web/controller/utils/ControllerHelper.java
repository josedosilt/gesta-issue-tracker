package es.udc.fic.gesta.app.web.controller.utils;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Map;

public final class ControllerHelper {

    private ControllerHelper(){}

    public static String manageErrorRedirectAttributes(String redirectTo, String errorMessage, RedirectAttributes ra,
                                                       Map<String, Object> otherAttributes) {
        ra.addAttribute("returnedError",errorMessage);
        if (otherAttributes != null) {
            ra.addAllAttributes(otherAttributes);
        }
        return redirectTo;
    }
}
