package es.udc.fic.gesta.app.web.controller.git;

import es.udc.fic.gesta.model.service.dto.BranchDTO;
import es.udc.fic.gesta.model.service.exceptions.GitLabRepositoryNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.git.GitLabRepoService;
import es.udc.fic.gesta.model.service.project.exception.ProjectNotFoundException;
import es.udc.fic.gesta.app.web.utils.Ajax;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class BranchesController {

    public static final String BRANCHES_VIEW = "git/branches";

    public static final String BRANCHES_FRAGMENT = BRANCHES_VIEW + " :: branchesFragment";

    @Autowired
    private GitLabRepoService gitLabRepoService;

    @GetMapping("/project/{id}/branches")
    public String getBranchesView(Model model, @PathVariable("id") String projectId,
                                  @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                          String requestedWith) throws GitLabRepositoryNotFoundException,
            ProjectNotFoundException, OperationForbiddenException {
        model.addAttribute("projectId", projectId);
        model.addAttribute("branchDTOS", gitLabRepoService.findProjectBranches(projectId));
        if (Ajax.isAjaxRequest(requestedWith)) {
            return BRANCHES_FRAGMENT;
        }
        return BRANCHES_VIEW;
    }

    @GetMapping("/project/{id}/getBranches")
    @ResponseBody
    public List<BranchDTO> getBranches(@PathVariable("id") String projectId) throws GitLabRepositoryNotFoundException,
            ProjectNotFoundException, OperationForbiddenException {
        return gitLabRepoService.findProjectBranches(projectId);
    }
}
