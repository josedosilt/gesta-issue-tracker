package es.udc.fic.gesta.app.web.i18n;

public final class CommonMessages {

    public static final String PROJECT_NOT_FOUND_MESSAGE = "project.error.notFound";

    public static final String USER_NOT_FOUND_MESSAGE = "user.error.notFound";

    public static final String ERROR_UNKNOWN = "error.unknown";
    public static final String EMPTY_COMMENT_ERROR = "error.emptyComment";
    public static final String EMPTY_USER_LIST_ERROR = "error.emptyUserList";
    public static final String ISSUE_NOT_FOUND_MESSAGE = "error.issueNotFound";
    public static final String NULL_OR_EMPTY_CYCLE_NAME = "developmentCycle.error.nullOrEmptyName";
    public static final String CYCLE_NAME_ALREADY_EXIST = "developmentCycle.error.nameAlreadyExist";
    public static final String ERROR_PASSWORD_NOT_MATCH = "error.createUser.passwordNotMatch";
    public static final String ERROR_EMAIL_ALREADY_IN_USE = "error.createUser.emailAlreadyInUse";
    public static final String OPERATION_FORBIDDEN = "error.operationForbidden";
    public static final String CYCLE_NOT_FOUND_MESSAGE = "error.cycleNotFound";

    private CommonMessages(){}


}
