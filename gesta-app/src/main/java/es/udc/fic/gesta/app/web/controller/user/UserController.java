package es.udc.fic.gesta.app.web.controller.user;


import es.udc.fic.gesta.app.web.utils.Ajax;
import es.udc.fic.gesta.model.service.user.UserProfileDTO;
import es.udc.fic.gesta.model.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import java.security.Principal;
import java.util.Optional;

@Controller
public class UserController {

    @Autowired
    private UserService userService;


    @GetMapping(value = {"/profile", "/profile/{id}"})
    public String getUserProfile(Model model, @PathVariable Optional<Long> id, Principal principal,
                                 @RequestHeader(value = Ajax.REQUESTED_WITH_HEADER, required = false)
                                         String requestedWith) {
        UserProfileDTO userProfile;

        if (id.isPresent()) {
            userProfile = userService.getUserProfile(id.get());
        } else {
            userProfile = userService.getUserProfile(principal.getName());
        }

        model.addAttribute("userProfile", userProfile);

        return Ajax.isAjaxRequest(requestedWith) ? "user/user_profile :: userProfileFragment" : "user/user_profile";
    }
}
