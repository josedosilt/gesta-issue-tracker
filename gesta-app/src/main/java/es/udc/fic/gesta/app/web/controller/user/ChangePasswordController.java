package es.udc.fic.gesta.app.web.controller.user;

import es.udc.fic.gesta.app.web.exceptions.InvalidOldPasswordException;
import es.udc.fic.gesta.model.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;

@Controller
public class ChangePasswordController {

    @Autowired
    private UserService userService;

    @GetMapping("/user/changepassword")
    public String getChangePassword(Principal principal, Model model) {
        return "user/change_password.html";
    }

    @PostMapping("/user/updatePassword")
    public String changeUserPassWord(Principal principal, Model model, RedirectAttributes ra,
                                     @RequestParam("newPassword") String newPassword,
                                     @RequestParam("confirmPassword") String confirmPassword,
                                     @RequestParam("oldPassword") String oldPassword)
            throws InvalidOldPasswordException {

        if (newPassword == null || !newPassword.equals(confirmPassword)) {
            model.addAttribute("showError", true);
            model.addAttribute("errorMsg", "Passwords doesn't match");
            return "user/change_password.html";
        }
        UserDetails user = userService.loadUserByUsername(principal.getName());
        if (!userService.checkPassword(user, oldPassword)) {
            model.addAttribute("showError", true);
            model.addAttribute("errorMsg", "Old password doesn't match");
            return "user/change_password.html";
        }
        userService.updatePassword(user, newPassword);
        return "redirect:/profile";
    }
}
