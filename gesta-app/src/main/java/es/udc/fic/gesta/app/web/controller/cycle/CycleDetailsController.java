package es.udc.fic.gesta.app.web.controller.cycle;

import es.udc.fic.gesta.model.service.exceptions.DevelopmentCycleNotFoundException;
import es.udc.fic.gesta.model.service.exceptions.badrequest.OperationForbiddenException;
import es.udc.fic.gesta.model.service.project.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class CycleDetailsController {

    private static final String CYCLE_DETAILS_VIEW = "cycle/details";

    @Autowired
    ProjectService projectService;

    @GetMapping("/project/{project_id}/cycle/{name}")
    public String getCycleDetailsView(Model model, @PathVariable("project_id") String projectId,
                                      @PathVariable("name") String name) throws
            DevelopmentCycleNotFoundException, OperationForbiddenException {
        model.addAttribute("developmentCycle", projectService.getDevelopmentCycle(projectId, name));
        model.addAttribute("projectHasGitRepository", projectService.projectHasGitRepository(projectId));
        return CYCLE_DETAILS_VIEW;
    }
}
