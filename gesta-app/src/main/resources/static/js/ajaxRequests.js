

function openModalWindowOnAction(param) {
    param.component.on(param.action, function (e) {
        e.preventDefault();
        if(param.modalTitle){
            $(param.modalId + " .modal-title").text(param.modalTitle);
        }
        $(param.modalId).modal('show');
        var modalBodyId = param.modalId + "-body";
        var redirectUrl = $(this).attr("href") || $(this).data("action");
        if(param.urlFunction){
            redirectUrl = param.urlFunction($(this))
        } else if(param.url){
            redirectUrl = param.url;
        }
        if(param.modalSize){
            $(param.modalId + " .modal-dialog")
                .removeClass("modal-sm modal-md modal-lg modal-xl")
                .addClass(param.modalSize);
        }
        if ($(modalBodyId).is(':empty') || param.override || $(modalBodyId).data("error")) {
            var loaderTemplate = $("#loader-template");
            if(loaderTemplate.length){
                $(modalBodyId).html(loaderTemplate.html());
            }else{
                $(modalBodyId).empty();
            }
            $.ajax({
                url: redirectUrl,
                cache: false,
                success: function (ta) {
                    $(modalBodyId).html(ta);
                    $(modalBodyId).data("error", false);
                    if(param.success){
                        param.success();
                    }
                },
                error: function (xhr, status, error) {
                    $(modalBodyId).html(/*[[#{error.unknown}]]*/ "Unknown Error. Try Again.")
                    $(modalBodyId).data("error", true);
                }

            });
        }
        return false;
    });
}

function ajaxSubmitFormPost(param){
    param.component.on("submit", function (e) {
        e.preventDefault();
        if(param.preCheck && param.preCheck()){
            return false;
        }
        if(param.errorComponent) {
            param.errorComponent.removeClass("show");
        }
        var url = $(this).attr("action");
        var data = $(this).serialize()
        $.ajax({
            url: url,
            cache: false,
            method: "POST",
            data: data,
            success: function (data,textStatus, request) {
                if (param.errorComponent.is(":visible")) {
                    param.errorComponent.hide();
                }
                if(param.success){
                    param.success(data);
                } else {
                    var newUrl = param.redirectTo || request.getResponseHeader('Location');
                    if (newUrl && newUrl !== "") {
                        $(location).attr('href', newUrl);
                    } else {
                        location.reload();
                    }}

            },
            error: function (xhr, status, error) {
                    if (param.errorComponent) {
                        param.errorComponent.addClass("show");
                    if (!param.errorComponent.is(":visible")) {
                        param.errorComponent.show();
                    }
                    param.errorComponent.text(xhr.responseJSON.message);
                }
            }
        });
        return false;
    })
}

function ajaxSubmitFormGet(param) {
    param.component.on("submit", function (e) {
        e.preventDefault();
        $.ajax({
            url: param.url || param.component.attr("action"),
            cache: false,
            data: param.component.serialize(),
            success: function (ta) {
                param.resultComponent.html(ta);
            },
            error: function (xhr, status, error) {
                param.resultComponent.html(/*[[#{error.unknown}]]*/ "Unknown Error. Try Again.")
            }

        });
        return false;
    });
}

function dynamicAjaxSelect(param) {
    param.component.on("loaded.bs.select", function () {
        param.component.parent().find("input").on("keyup", function (e) {
            if ((e.which > 90 || e.which < 48) && !(e.which == 8 || e.which == 32)){
                return;
            }
            var data = {};
            data[param.requestParamName] = $(this).val();
            $.ajax({
                url: param.component.data("search-url"),
                data: data,
                cache: false,
                dataType: 'json',
                success: function (result) {
                    param.component.empty();
                    param.component.append("<option></option>");
                    if(param.currentValue){
                        var optionText = param.currentText || param.currentValue;
                        param.component.append("<option class='font-italic font-weight-bold'"
                            + "value='" + param.currentValue
                            +"'>"+ optionText +"</option>");
                    }
                    var fieldValue = param.propertyValue || param.propertyName;
                    $.each(result, function (i, element) {
                        if(!(param.currentValue) || (param.currentValue !== element[fieldValue])) {
                            param.component.append("<option value='" + element[fieldValue] + "'>" + element[param.propertyName] + "</option>");
                        }
                    });
                    param.component.selectpicker('refresh');
                }
            });
        });
    });
}

function ajaxNavigation(navComponentId, resultComponentId, url){
    var navComponents = navComponentId + " .page-link";
    $(navComponents).on("click", function (e) {
        e.preventDefault();
        $.ajax({
            url: url,
            cache: false,
            data: $(this).attr("href").split('?')[1],
            success: function (ta) {
                $(resultComponentId).html(ta);
            },
            error: function (xhr, status, error) {
                $(resultComponentId).html(/*[[#{error.unknown}]]*/ "Unknown Error. Try Again.")
            }

        });
        return false;
    });
}

